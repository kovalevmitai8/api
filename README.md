# Procédure d'installation

### Logiciels utilisés :
À installer avant de commencer la procédure.
- Un IDE https://www.jetbrains.com/fr-fr/webstorm/download/#section=windows
- Git https://git-scm.com/downloads
- Nodejs https://nodejs.org/en/download/current/

# Scripts utile pour dev

### restorer un snapshot de la BD mongo sur un container MongoDB frais :
- Télécharger un snapshot de la bdd
- Dans un invite de commande, se déplacer à la racine de ce snapshot
- Renommer la bdd au besoin (ex: finances_prod -> finances_dev)
- Restorer la bdd
```
mongorestore --uri mongodb://localhost:27018 ./
```

### setup minimal pour lancer l'API en debug
- installer les packages npm
```
yarn
```
- lancer les conteneurs de bdd
```
docker-compose up --build database es
```
- lancer l'API
```
NODE_ENV=DEV;HTTPS_CERT_PATH=./certificate/server.crt;HTTPS_PRIVK_PATH=./certificate/server.key;API_CONFIG_PATH=secrets.json;CRAWLER_HOSTNAME=localhost:9095;MAILER_HOSTNAME=localhost:9096;PDF_EXTRACT_HOSTNAME=localhost:9097
```

# Notes sur le déploiement

### Setup de la VM de production
En production, seul Git et docker sont requis pour lancer l'API.

Le MongoDB command line toolbox peux aussi être utile pour restaurer une sauvegarde.

### Certificats TLS
Certbot est utilisé pour obtenir le certificat TLS et le garder à jour. Le cron de certbot doit être modifié pour
appeler un script après le renouvellement du certificat pour redémarrer les services. 

Le cron se trouve normalement à /etc/cron.d/certbot et le flag `--post-hook "path/to/script"` doit être ajouté à la fin
de la commande `certbot renew` (à la toute fin de la commande dans le cron).

Exemple:
```shell script
#!/bin/bash

# restart the API
cd /home/finances-admin/finances_api
./start.sh

# re-create Graylog's cert/key pair from the new ones
mkdir -p ./infrastructure/graylog/cert
openssl pkcs8 -in /etc/letsencrypt/live/api.finances-app.com/privkey.pem -topk8 -nocrypt -out ./infrastructure/graylog/cert/graylog-plain.pem
cp /etc/letsencrypt/live/api.finances-app.com/fullchain.pem ./infrastructure/graylog/cert/fullchain.pem

# restart the Graylog container
cd ./infrastructure/graylog
docker-compose up --build -d
```
