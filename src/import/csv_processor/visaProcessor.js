/**
 * Created by hugo on 2018-05-18.
 */
const mongoose = require('mongoose');

const dateUtil = require('../../modules/dateUtil');
const TransactionSchema = require('../../daos/transaction');
const Transaction = mongoose.model('transaction', TransactionSchema);
const config = require('../../../config/import').visa;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Public functions
///
module.exports = {
    process: function (lines) {
        validate(lines);

        let records = getRecords(lines);
        return records.map(
            (r) =>
                new Transaction({
                    description: r.description,
                    date: r.date,
                    amount: r.amount,
                    account: `${r.cardNumber.substr(12, 4)}-CC`,
                    currency: 'CAD',
                })
        );
    },

    /**
     * @param {string[]} lines
     * @returns {{ accountName: string, date: Date, balance: number }[]}
     */
    getBalance: function (lines) {
        return [];
    },
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Private classes
///
class VisaRecord {
    constructor({ cardNumber, date, transactionNumber, description, amount }) {
        this.cardNumber = cardNumber;
        this.date = date;
        this.transactionNumber = transactionNumber;
        this.description = description;
        this.amount = amount;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Private functions
///
function validate(lines) {
    if (lines.length < config.minimum_line_count) {
        throw new Error('Empty transaction file.');
    }

    let line;
    for (let i = 0; i < lines.length; i++) {
        line = lines[i];

        if (line.length !== config.column_count) {
            throw new Error(`Error at line ${i}. Expected ${config.column_count} columns, got ${line.length}.`);
        }

        for (let key in config.columns_type) {
            if (typeof line[key] !== config.columns_type[key]) {
                throw new Error(
                    `Error at line ${i}. Invalid data type at column ${key}. Expected ${
                        config.columns_type[key]
                    }, got ${typeof line[key]}.`
                );
            }
        }

        let dateSplit = line[3].split(config.date_separator);
        if (dateSplit.length !== 3) {
            throw new Error(`Error at line ${i}. Unexpected date format.`);
        }
    }
}

function getRecords(lines) {
    return lines.map(
        (l) =>
            new VisaRecord({
                cardNumber: l[0].substring(5),
                date: dateUtil.formatDateTime(l[3], {
                    format: config.date_format,
                    separator: config.date_separator,
                }),
                transactionNumber: parseInt(l[4]),
                description: l[5],
                amount: l[11]
                    ? -parseFloat(l[11].toString().replace(',', '.'))
                    : parseFloat(l[12].toString().replace(',', '.')),
            })
    );
}
