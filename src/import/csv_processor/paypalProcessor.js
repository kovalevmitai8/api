/**
 * Created by hugo on 2018-05-18.
 */
const mongoose = require('mongoose');

const dateUtil = require('../../modules/dateUtil');
const TransactionSchema = require('../../daos/transaction');
const Transaction = mongoose.model('transaction', TransactionSchema);
const config = require('../../../config/import').paypal;
const transacStatus = config.transaction_status;
const transacType = config.transaction_type;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Public functions
///
module.exports = {
    process: function (lines) {
        validate(lines);

        let records = getRecords(lines);
        return getTransactions(records);
    },

    /**
     * @param {string[]} lines
     * @returns {{ accountName: string, date: Date, balance: number }[]}
     */
    getBalance: function (lines) {
        return [];
    },
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Private classes
///
class PaypalRecord {
    constructor({ dateTime, name, type, status, currency, amount, balance }) {
        this.dateTime = dateTime;
        this.name = name;
        this.type = type;
        this.status = status;
        this.currency = currency;
        this.amount = amount;
        this.balance = balance;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Private functions
///
function validate(lines) {
    if (lines.length < config.minimum_line_count) {
        throw new Error(`Unexpected number of lines. Expected ${config.minimum_line_count}, got ${lines.length}`);
    }

    let line;
    for (let i = 1; i < lines.length - 1; i++) {
        line = lines[i];

        if (line.length !== config.column_count) {
            throw new Error(`Error at line ${i}. Expected ${config.column_count} columns, got ${line.length}.`);
        }

        for (let key in config.columns_type) {
            if (typeof line[key] !== config.columns_type[key]) {
                throw new Error(`Error at line ${i}. Invalid data type at column ${key}. 
                    Expected ${config.columns_type[key]}, got ${typeof line[key]}.`);
            }
        }

        let dateSplit = line[0].split(config.date_separator);
        if (dateSplit.length !== 3) {
            throw new Error(`Error at line ${i}. Unexpected date format.`);
        }

        let timeSplit = line[1].split(':');
        if (timeSplit.length !== 3) {
            throw new Error(`Error at line ${i}. Unexpected time format.`);
        }
    }
}

function getRecords(lines) {
    return lines
        .filter((l, i) => l.length === config.column_count && i !== 0)
        .map(
            (l) =>
                new PaypalRecord({
                    dateTime: dateUtil.formatDateTime(l[0], l[1], {
                        format: config.date_format,
                        separator: config.date_separator,
                    }),
                    name: l[3] ? l[3] : l[4],
                    type: l[4],
                    status: l[5],
                    currency: l[6],
                    amount:
                        config.decimal_char === '.' && config.thousand_separator === ','
                            ? parseFloat(l[7].toString().replace(',', ''))
                            : parseFloat(l[7].toString().replace(',', '.')),
                    balance:
                        config.decimal_char === '.' && config.thousand_separator === ','
                            ? parseFloat(l[9].toString().replace(',', ''))
                            : parseFloat(l[9].toString().replace(',', '.')),
                })
        );
}

function getTransactions(records) {
    let transactions = [];

    // group records by datetime
    let groupReducer = function (groups, record) {
        let group = groups.filter((g) => g[0] && g[0].dateTime.getTime() === record.dateTime.getTime())[0];
        if (group) {
            group.push(record);
        } else groups.push([record]);
        return groups;
    };
    let recordGroups = records.reduce(groupReducer, []);

    recordGroups.forEach((group) => {
        group
            .filter((r) => transacType.bankAccount.indexOf(r.type) !== -1)
            .forEach((r) =>
                transactions.push(
                    new Transaction({
                        type: 'Virement',
                        description: r.name,
                        date: r.dateTime,
                        amount: r.amount,
                        currency: r.currency,
                        account: 'Paypal',
                    })
                )
            );

        // If any transaction in the group is cancelled or reimbursed, stop parsing the group.
        let cancelledOrReimbursedRecords = group.reduce(
            (r) =>
                transacStatus.completed.indexOf(r.status) === -1 &&
                transacStatus.pending.indexOf(r.status) === -1 &&
                transacType.fundsRelease.indexOf(r.type) !== -1
        );
        if (cancelledOrReimbursedRecords.length > 0) return;

        let conversions = group.filter((r) => transacType.currencyConversion.indexOf(r.type) !== -1);
        let purchase = group.filter(
            (r) => transacType.purchase.indexOf(r.type) !== -1 || transacType.paymentReceived.indexOf(r.type) !== -1
        )[0];
        let refund = group.filter((r) => transacType.refund.indexOf(r.type) !== -1)[0];
        let reversal = group.filter((r) => transacType.reversal.indexOf(r.type) !== -1)[0];

        let conversionRate = 1;
        if (conversions.filter((r) => r.currency === 'CAD').length > 0) {
            let localAmount = conversions.filter((r) => r.currency === 'CAD')[0].amount;
            let foreignAmount = conversions.filter((r) => r.currency !== 'CAD')[0].amount;
            conversionRate = Math.abs(localAmount) / Math.abs(foreignAmount);
        }

        if (purchase) {
            transactions.push(
                new Transaction({
                    type: purchase.amount <= 0 ? 'Achat' : 'Vente',
                    description: purchase.name,
                    date: purchase.dateTime,
                    amount: purchase.amount * conversionRate,
                    currency: purchase.currency,
                    conversionRate: conversionRate,
                    account: 'Paypal',
                })
            );
        } else if (refund) {
            transactions.push(
                new Transaction({
                    type: 'Remboursement',
                    description: refund.name,
                    date: refund.dateTime,
                    amount: refund.amount * conversionRate,
                    currency: refund.currency,
                    conversionRate: conversionRate,
                    account: 'Paypal',
                })
            );
        } else if (reversal) {
            transactions.push(
                new Transaction({
                    type: 'Annulation',
                    description: reversal.name,
                    date: reversal.dateTime,
                    amount: reversal.amount * conversionRate,
                    currency: reversal.currency,
                    conversionRate: conversionRate,
                    account: 'Paypal',
                })
            );
        } else {
            group
                .filter(
                    (r) =>
                        transacType.bankAccount.indexOf(r.type) === -1 &&
                        transacType.accountHold.indexOf(r.type) === -1 &&
                        transacType.currencyConversion.indexOf(r.type) === -1 &&
                        transacType.authorization.indexOf(r.type) === -1
                )
                .forEach((r) => {
                    transactions.push(
                        new Transaction({
                            type: r.type,
                            description: r.name,
                            date: r.dateTime,
                            amount: r.amount,
                            currency: r.currency,
                            conversionRate: 1,
                            account: 'Paypal',
                        })
                    );
                });
        }
    });

    return transactions;
}
