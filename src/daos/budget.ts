/**
 * Created by hugo on 2020-03-04
 */
import * as dayjs from 'dayjs';
import { extend, isEmpty } from 'lodash';
import { Schema, Model, model, Document, FilterQuery, Types, LeanDocument } from 'mongoose';
import { forUser, IGenericSchema, GenericQueryHelpers, GenericSchema } from './genericModel';
import { addOnePeriod } from '../modules/dateUtil';
import { budgetEntrySchema, BudgetEntryDocument } from './budgetEntry';
import { CategoryDocument } from './category';
import { Budget as BudgetDto, BudgetCategory } from '../models/entities';

export interface IBudget extends IGenericSchema {
    year: number;
    entries: Array<BudgetEntryDocument>;
    categoriesSnapshot: Array<CategoryDocument>;
    plannedBudgetSumHistory: Array<{ month: number; value: number }>;
    isHidden: boolean;
}

interface IBudgetMethods {
    findEntryByCategory(categoryId: string): BudgetEntryDocument;
    /**
     * Copy the budget entries, filtering the ones ending before the `newYear` and changing the start dates for the
     * other ones.
     */
    cloneEntriesForYear(newYear: number): Array<BudgetEntryDocument>;
    entries: Types.DocumentArray<BudgetEntryDocument>;
}

export type BudgetDocument = IBudget & Document & IBudgetMethods & {
    isClosed: boolean;
};

interface BudgetModel extends Model<BudgetDocument, GenericQueryHelpers<BudgetModel>, IBudgetMethods> {
    get(userId: string, year: number, ignoreHidden?: boolean): Promise<BudgetDocument>;
    getClosest(userId: string, year: number, ignoreHidden?: boolean): Promise<BudgetDocument>;
    toDto(budget: LeanDocument<BudgetDocument>, rootCategories: BudgetCategory[]): BudgetDto;
}

/**
 * @class Budget
 */
let budgetSchema = new Schema<BudgetDocument, BudgetModel, IBudgetMethods, GenericQueryHelpers<BudgetDocument>>(
    extend(
        {
            year: { type: Number, required: true },
            /**
             * *** WHEN THIS VALUE IS SET, THE BUDGET BECOMES READ-ONLY ***
             * Only needed after the budget is closed (next one is created), to display the yearly stats as the categories
             * were when the budget was first created.
             * For example, if a category is renamed and does not reference the same shildren categories anymore, we still
             * want to display the budget stats as the category was before, otherwise the data loses context and doesn't
             * make sence anymore.
             */
            categoriesSnapshot: [Schema.Types.Mixed],
            entries: [budgetEntrySchema],
            /**
             * History of the planned yearly amount for cimple budget entries.
             */
            plannedBudgetSumHistory: [{ month: Number, value: Number }],
            isHidden: Boolean,
        },
        GenericSchema
    )
);

budgetSchema.query.forUser = forUser<BudgetDocument, GenericQueryHelpers<BudgetDocument>>;

budgetSchema.virtual('isClosed').get(function isClosed(this: BudgetDocument) {
    return !isEmpty(this.categoriesSnapshot);
});

budgetSchema.method('findEntryByCategory', function findEntryByCategory(this: BudgetDocument, categoryId: string) {
    return this.entries.find((entry) => entry.categoryId.equals(categoryId));
});

budgetSchema.method('cloneEntriesForYear', function cloneEntriesForYear(this: BudgetDocument, newYear: number) {
    const oldEntries = this.entries.map((entry) => entry.toObject());
    const updatedEntries = oldEntries.filter((entry) => !entry.isComplex);

    for (let entry of oldEntries.filter((entry) => entry.isComplex)) {
        let updatedRules = entry.rules
            // filter rules where the end date is before the start of newYear, or rules for single
            // transactions (no period and no end date) where the start date is before the start of newYear.
            .filter((rule: any) => {
                const isEndingBeforeNewYear = rule.endDate && rule.endDate.getFullYear() < newYear;
                const isSingleTransactionRule = !rule.endDate && !rule.period;
                return !(
                    isEndingBeforeNewYear ||
                    (isSingleTransactionRule && rule.startDate.getFullYear() < newYear)
                );
            })
            .map((rule: any) => {
                if (rule.startDate.getFullYear() < newYear) {
                    // we have to adjust the startDate to be within the new year
                    const target = dayjs().year(newYear).startOf('year');
                    let start = dayjs(rule.startDate);

                    while (start.isBefore(target)) {
                        start = addOnePeriod(start, rule.period);
                    }

                    rule.startDate = start.toDate();
                }
                return rule;
            });
        if (!isEmpty(updatedRules)) {
            updatedEntries.push({
                ...entry,
                rules: updatedRules,
            });
        }
    }

    return updatedEntries;
});

budgetSchema.static('get', async function get(userId: string, year: number, ignoreHidden = false) {
    let query: FilterQuery<BudgetDocument> = {
        userId,
        year,
    };
    if (ignoreHidden) {
        query['isHidden'] = { $ne: true };
    }
    return this.findOne(query);
});

budgetSchema.static('getClosest', async function getClosest(userId: string, year: number, ignoreHidden = false) {
    let budgets;
    if (year >= dayjs().year()) {
        let query: FilterQuery<BudgetDocument> = {
            userId,
            year: {
                $lte: year,
            },
        };
        if (ignoreHidden) {
            query['isHidden'] = { $ne: true };
        }
        budgets = await this.find(query).sort('-year');
    } else {
        let query: FilterQuery<BudgetDocument> = {
            userId,
            year: {
                $gte: year,
            },
        };
        if (ignoreHidden) {
            query['isHidden'] = { $ne: true };
        }
        budgets = await this.find(query).sort('year');
    }
    return isEmpty(budgets) ? null : budgets[0];
});

budgetSchema.static('toDto', function toDto(
    budget: LeanDocument<BudgetDocument>,
    rootCategories: BudgetCategory[]
): BudgetDto {
    return {
        id: budget?._id?.toString() || budget.id,
        isClosed: budget?.isClosed,
        year: budget?.year,
        hasBudget: !!budget,
        budgetEntries: budget?.entries?.map(entry => entry.toDto()) || [],
        rootCategories,
    };
});

export { budgetSchema }
export const Budget = model<BudgetDocument, BudgetModel>('budget', budgetSchema);
