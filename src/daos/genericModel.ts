import { QueryWithHelpers, HydratedDocument } from 'mongoose';

export interface GenericQueryHelpers<SchemaInterface> {
    forUser(
        userId: string
    ): QueryWithHelpers<
        HydratedDocument<SchemaInterface>[],
        HydratedDocument<SchemaInterface>,
        GenericQueryHelpers<SchemaInterface>
    >;
}

export interface IGenericSchema {
    userId: string;
}

export const GenericSchema = {
    userId: { type: String, required: true },
};

export function forUser<SchemaInterface, QueryHelperInterface>(
    this: QueryWithHelpers<any, HydratedDocument<SchemaInterface>, QueryHelperInterface>,
    userId: string
) {
    return this.where({ userId });
}
