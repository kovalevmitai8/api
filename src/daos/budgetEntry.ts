import * as dayjs from 'dayjs';
import { Schema, Model, Document, Types, LeanDocument } from 'mongoose';
import { addOnePeriod, DateInterval, SimpleDateInterval, periodsInYear } from '../modules/dateUtil';
import { CategoryDocument } from './category';
import { SystemCategory } from '../models/enums';
import { BudgetEntry as BudgetEntryDto } from '../models/entities';

export type PlannedTransaction = {
    amount: number;
    date: number;
};

export interface IBudgetEntry {
    categoryId: Types.ObjectId;
    isComplex: boolean;
    // fields for simple budget
    amount: number;
    period: SimpleDateInterval;
    // fields for complex budget
    rules: [
        {
            period: DateInterval;
            amount: number;
            startDate: Date;
            endDate: Date;
        }
    ];
}

interface IBudgetEntryMethods {
    getPlannedTransactions(minDate: dayjs.Dayjs, maxDate: dayjs.Dayjs): Array<PlannedTransaction>;
    calculatePlannedBudgetSumPerDay(
        categories: Array<CategoryDocument | LeanDocument<CategoryDocument>>
    ): number | null;
    getMaximumSloppinessInDays(): number;
    toDto(): BudgetEntryDto;
}

export type BudgetEntryDocument = IBudgetEntry & Document & IBudgetEntryMethods;

interface BudgetEntryModel extends Model<BudgetEntryDocument, {}, IBudgetEntryMethods> {}

let budgetEntrySchema = new Schema<BudgetEntryDocument, BudgetEntryModel, IBudgetEntryMethods>({
    // @ts-ignore
    categoryId: { type: Types.ObjectId, required: true },
    isComplex: Boolean,
    // fields for simple budget
    amount: Number,
    period: { type: String, enum: ['w', 'M', 't', 'y'] },
    // fields for complex budget
    rules: [
        {
            period: { type: String, enum: ['w', '2w', 'M', 't', 'q', 'y'] },
            amount: Number,
            startDate: Schema.Types.Date,
            endDate: Schema.Types.Date,
        },
    ],
});

budgetEntrySchema.method('toDto', function toDto(this: BudgetEntryDocument): BudgetEntryDto {
    return {
        _id: this._id?.toString() || this.id,
        amount: this.amount,
        categoryId: this.categoryId,
        isComplex: this.isComplex,
        period: this.period,
        rules: this.rules,
    };
});

budgetEntrySchema.method('getPlannedTransactions', function getPlannedTransactions(
    minDate: dayjs.Dayjs,
    maxDate: dayjs.Dayjs
): Array<PlannedTransaction> {
    let plannedTransactions: Array<PlannedTransaction> = [];

    if (!this.isComplex) {
        return plannedTransactions;
    }

    for (let rule of this.rules.values()) {
        if (!rule.period) {
            let startDateTimestamp = dayjs(rule.startDate).valueOf();
            if (minDate.valueOf() <= startDateTimestamp && startDateTimestamp <= maxDate.valueOf()) {
                plannedTransactions.push({ amount: rule.amount, date: startDateTimestamp });
            }
        } else {
            let startDate = dayjs(rule.startDate);
            while (startDate.isBefore(minDate)) {
                startDate = addOnePeriod(startDate, rule.period);
            }

            let endDate = rule.endDate && dayjs(rule.endDate);
            endDate = !endDate || endDate.isAfter(maxDate) ? maxDate.clone() : endDate;

            // Move the startDate forward by the specified period until it goes past the endDate
            while (startDate.isBefore(endDate)) {
                plannedTransactions.push({
                    amount: rule.amount,
                    date: startDate.valueOf(),
                });

                startDate = addOnePeriod(startDate, rule.period);
            }
        }
    }

    return plannedTransactions;
});

budgetEntrySchema.method('calculatePlannedBudgetSumPerDay', function calculatePlannedBudgetSumPerDay(
    categories: Array<CategoryDocument | LeanDocument<CategoryDocument>>
): number | null {
    if (this.isComplex) {
        return null;
    }

    const budget = this.parent();

    const category = categories.find((_cat) => _cat._id.equals(this.categoryId));
    if (!category) {
        throw new Error(
            `The budget ${budget.year} for user ${budget.userId} has a budget entry referencing the category ` +
                `${this.categoryId.toString()}, but the category does not exist either in the ` +
                `categoriesSnapshot or the category collection.`
        );
    }

    const timesInYear = periodsInYear(this.period);
    if (timesInYear === null) {
        throw new Error(`Budget entry with invalid period '${this.period}'.`);
    }
    let sumPerDay = (this.amount * timesInYear) / 365;

    return sumPerDay * (category.systemType === SystemCategory.revenu ? 1 : -1);
});

budgetEntrySchema.method('getMaximumSloppinessInDays', function getMaximumSloppinessInDays(
    this: BudgetEntryDocument
): number {
    // There can be some slop in the date matching on the actual transaction side.
    // This is because bank typicaly dont record transactions on week-ends and some holidays.
    // Adding to the complexity, transactions occuring on off-days will randomly be assigned the first
    // business day before OR after the off-day period.

    // Actually this all goes out of the window because we may want to track some recurring transactions
    // that are handled by a human ! Paychecks is a good example. They will occur on average every 2 weeks
    // lets say, but one could forget to send one for a while. This could result in two pay checks being
    // sent nearly on the same day.
    // We should therefore accept as much sloppiness as possible.

    // There is no rhyme or reason behind these numbers, they are basically magic numbers.
    return Math.max(
        ...this.rules.map((rule) => {
            // prettier-ignore
            switch (rule.period) {
                case 'w':   return 3;
                case '2w':  return 5;
                case 'M':   return 10;
                case 'q':   return 25;
                case 't':   return 35;
                case 'y':   return 40;
                default:    return 5;
            }
        })
    );
});

export { budgetEntrySchema };
