/**
 * Created by hugo on 2018-05-13.
 */

const _ = require('lodash');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const dayjs = require('dayjs');
const mongooseLeanVirtuals = require('mongoose-lean-virtuals');
const FileDao = require('./file');
const { AttachmentType, CategoryType } = require('../models/enums');
const { splitDesjardinsDescription } = require('../import/importHelper');

/**
 * @class Transaction
 */
let transactionSchema = new Schema({
    // fundamental fields
    /** @type {string} */
    description: { type: String, required: true },
    /** @type {Date} */
    date: { type: Schema.Types.Date, required: true },
    /** @type {number} */
    amount: { type: Number, required: true },

    // infered from the context
    /** @type {string} */
    account: String,
    /** @type {string} */
    currency: String,
    /** @type {number} */
    conversionRate: Number,

    // stuff that is tacked on post-import
    /** @type {Date} */
    creationDate: { type: Schema.Types.Date, default: Date.now },
    /** @type {ObjectId} */
    statementId: mongoose.Types.ObjectId,
    /** @type {string} */
    category: { type: String, default: '' },
    /** @type {*[]} */
    attachments: [
        {
            fileId: { type: String, required: true },
            type: {
                type: String,
                required: true,
                enum: [AttachmentType.file, AttachmentType.eml, AttachmentType.json, AttachmentType.pdf],
            },
            filename: String,
        },
    ],
    /** @type string */
    userId: String,

    // a bunch of flags. bad design ?
    /** @type boolean */
    manuallyApproved: Boolean,
    /** @type boolean */
    ignoreForAutoCategoryAssignment: Boolean,
    /** @type boolean */
    recurring: Boolean,
    /** @type boolean */
    missingDetails: Boolean,
    /** @type boolean */
    isPending: Boolean,
});
transactionSchema.index({ category: 1, userId: 1 });

transactionSchema.query.forUser = function (userId) {
    return this.where({ userId });
};

transactionSchema.virtual('descriptionInfo').get(function descriptionInfo() {
    const split = splitDesjardinsDescription(this.description);
    if (this.description?.length === 41 && this.account.includes('CC')) {
        // visa transactions always have exactly 41 characters if un-processed
        return {
            main: this.description.substring(0, 25),
            location: this.description.substring(25),
        };
    } else if (!this.account.includes('CC') && split[0] && split[1]) {
        return {
            type: split[0],
            main: split[1],
        };
    } else {
        return {};
    }
});

transactionSchema.plugin(mongooseLeanVirtuals);

class Transaction {
    /**
     * @returns {{date: Date, amount: number, manuallyApproved: boolean, statement: ObjectId, description: (string|null), currency: string, creationDate: Date, type: (string|null), category: (string|null), ignoreForAutoCategoryAssignment: boolean, conversionRate: number, userId: string}}
     */
    toEsEntity() {
        let esEntity = {
            id: this.id,
            creationDate: this.creationDate,
            conversionRate: this.conversionRate ? this.conversionRate : 1,
            amount: this.amount,
            currency: this.currency,
            description: this.description?.toLowerCase() || null,
            type: this.descriptionInfo?.type?.toLowerCase() || null,
            location: this.descriptionInfo?.location?.toLowerCase() || null,
            descriptionMain: this.descriptionInfo?.main?.toLowerCase() || null,
            date: this.date,
            category: this.category ? this.category.toLowerCase() : null,
            statement: this.statementId,
            userId: this.userId,
            manuallyApproved: this.manuallyApproved,
            ignoreForAutoCategoryAssignment: this.ignoreForAutoCategoryAssignment,
        };
        // remove empty fields
        for (let key in esEntity)
            if (esEntity.hasOwnProperty(key))
                if (esEntity[key] === null || esEntity[key] === undefined || esEntity[key] === '') delete esEntity[key];
        return esEntity;
    }

    /**
     * Upload a file and set it as an attachment to the transaction
     * @param {ReadableStream} stream
     * @param {string} type
     * @param {string} filename
     * @returns {Promise<{ fileId: string, type: string, filename: String }>}
     */
    async attachFile(stream, type, filename) {
        let saveRes;
        try {
            saveRes = await FileDao.save(stream, filename);
            const attachment = {
                fileId: saveRes._id,
                type,
                filename,
            };
            this.attachments.push(attachment);
            this.markModified('attachments');
            await this.save();

            return attachment;
        } catch (err) {
            if (saveRes) {
                // failled saving the file, cleanup db
                await FileDao.remove(saveRes._id);
            }
            throw err;
        }
    }

    /**
     * @param userId {string}
     * @param transactionId {ObjectId}
     * @returns {Promise<Transaction[]>}
     */
    static async getForSubCategoryAssignment(userId, transactionId) {
        let transaction = await this.model('transaction').findOne({ _id: transactionId, userId: userId }).lean();
        if (!transaction) {
            throw new Error(`No transaction with id ${transactionId}`);
        }

        return this.model('transaction')
            .find({
                description: transaction.description,
                statementId: transaction.statementId,
            })
            .sort('date');
    }

    static async assignCategoryToSimilarTransactions(userId, transactionId, categoryName) {
        let transaction = await this.model('transaction').findOne({ _id: transactionId }).forUser(userId).lean();
        const date = dayjs(transaction.date);
        return await this.model('transaction').updateMany(
            {
                userId,
                description: transaction.description,
                ignoreForAutoCategoryAssignment: { $ne: true },
                date: {
                    $gte: date.startOf('year').toDate(),
                    $lt: date.endOf('year').toDate(),
                },
            },
            {
                $set: { category: categoryName },
            }
        );
    }

    /**
     * @param userId {string}
     * @param statementId {ObjectId}
     * @param [errorsOnly] {boolean}
     * @returns {Promise<Transaction[]>}
     */
    static async getDistinctForSubCategoryAssignment(userId, statementId, errorsOnly = false) {
        let transactions = await this.model('transaction')
            .find({
                userId: userId,
                statementId: statementId,
            })
            .lean();

        return transactions
            .reduce((distinctTransactions, transaction) => {
                if (!distinctTransactions.some((t) => t.description === transaction.description)) {
                    distinctTransactions.push(transaction);
                }
                return distinctTransactions;
            }, [])
            .filter((_transaction) => !errorsOnly || !_transaction.category)
            .sort((a, b) => {
                let aString = a.description.toLowerCase();
                let bString = b.description.toLowerCase();
                if (aString < bString) return -1;
                if (aString > bString) return 1;
                return 0;
            });
    }

    /**
     * @param query {object}
     * @returns {Promise< Transaction[] >}
     */
    static getWithCategoryHierarchy(query) {
        return this.model('transaction').aggregate([
            {
                $match: query,
            },
            {
                $graphLookup: {
                    from: 'categories',
                    startWith: '$category',
                    connectFromField: 'parent',
                    connectToField: 'name',
                    as: 'category_hierarchy',
                    depthField: 'depth',
                },
            },
        ]);
    }

    /**
     * @param userId {string}
     * @param from {Date}
     * @param excludedCategories {?string[]}
     * @param balanceOffset {?number}
     * @returns {Promise< { date: string, relativeBalance: number }[] >}
     */
    static async getBalanceHistory(userId, from, excludedCategories = [], balanceOffset = 0) {
        let transactionAmounts = await this.model('transaction').aggregate([
            {
                $match: {
                    userId: userId,
                    date: {
                        $gte: from,
                    },
                    category: {
                        $nin: excludedCategories,
                    },
                },
            },
            {
                $group: {
                    _id: {
                        year: { $year: '$date' },
                        month: { $month: '$date' },
                        day: { $dayOfMonth: '$date' },
                    },
                    amount: { $sum: '$amount' },
                },
            },
            {
                $sort: {
                    '_id.year': 1,
                    '_id.month': 1,
                    '_id.day': 1,
                },
            },
        ]);
        return transactionAmounts
            .reverse()
            .reduce((acc, val, index) => {
                let date = new Date(val._id.year, val._id.month - 1, val._id.day).getTime();
                if (index === 0) {
                    acc.push([date, parseFloat((balanceOffset - val.amount).toFixed(2))]);
                } else {
                    acc.push([date, parseFloat((acc[index - 1][1] - val.amount).toFixed(2))]);
                }
                return acc;
            }, [])
            .reverse();
    }

    /**
     * @param userId {string}
     * @param from {Date}
     * @param to {Date}
     * @returns {Promise<{ _id: string, name: string, creationDate: Date, type: number, systemType: number, hue: number, parent: string, total: number, count: number, average: number }[]>}
     */
    static aggregateAmountByCategories(userId, from, to) {
        return this.model('transaction').aggregate([
            {
                $match: {
                    userId: userId,
                    date: {
                        $gte: from,
                        $lt: to,
                    },
                },
            },
            {
                $group: {
                    _id: '$category',
                    total: { $sum: '$amount' },
                },
            },
        ]);
    }

    /**
     * @param {string} userId
     * @param {Date} from
     * @param {Date} to
     * @param {string} [period]
     * @returns {Promise<[]>}
     */
    static aggregateAmountByCategoryAndPeriod(userId, from, to, period) {
        // default value is for year grouping.
        let groupByExpression = {
            category: '$category',
        };
        let sortExpression = {
            '_id.category': 1,
        };

        if (period) {
            groupByExpression['year'] = { $year: '$date' };

            if (period === 't') {
                groupByExpression[period] = {
                    $floor: {
                        $divide: [
                            {
                                $subtract: [
                                    {
                                        $month: '$date',
                                    },
                                    1,
                                ],
                            },
                            4,
                        ],
                    },
                };
            } else if (period === 'M') {
                groupByExpression[period] = {
                    $subtract: [
                        {
                            $month: '$date',
                        },
                        1,
                    ],
                };
            }

            sortExpression['_id.year'] = 1;
            if (period !== 'y') {
                sortExpression[`_id.${period}`] = 1;
            }
        }

        return this.model('transaction').aggregate([
            {
                $match: {
                    userId,
                    date: {
                        $gte: from,
                        $lt: to,
                    },
                    category: {
                        $exists: true,
                        $nin: [null, ''],
                    },
                },
            },
            {
                $group: {
                    _id: groupByExpression,
                    amount: { $sum: '$amount' },
                },
            },
            {
                $sort: sortExpression,
            },
        ]);
    }

    /**
     * Aggregate all transactions within the range of dates and with the specified sub-categories and return the sum of
     * the amounts.
     * From and to dates are inclusive.
     * @param {string} userId
     * @param {string[]} subCategoryNames
     * @param {Date} from
     * @param {Date} to
     * @return {Promise<number>}
     */
    static async sumAmountsForSubCategories(userId, subCategoryNames, from, to) {
        const res = await this.model('transaction').aggregate([
            {
                $match: {
                    userId,
                    category: { $in: subCategoryNames },
                    date: {
                        $gte: from,
                        $lte: to,
                    },
                },
            },
            {
                $group: {
                    _id: null,
                    sum: { $sum: '$amount' },
                },
            },
        ]);
        return (res[0] && res[0].sum) || 0;
    }

    /**
     * Count the number of transactions occuring for each categories within a specific time period.
     * @param userId
     * @param subCategoryNames
     * @param from
     * @param to
     * @returns {any}
     */
    static async countByCategories(userId, subCategoryNames, from, to) {
        return this.model('transaction').aggregate([
            {
                $match: {
                    userId,
                    date: {
                        $gte: from,
                        $lte: to,
                    },
                    category: { $in: subCategoryNames },
                },
            },
            {
                $group: {
                    _id: '$category',
                    count: { $sum: 1 },
                },
            },
        ]);
    }

    /**
     * Validate a list of transactions
     * @param userId
     * @param transactions
     * @returns {Promise<boolean|*>}
     */
    static async validate(userId, transactions) {
        // check that no group category was assigned
        const categoryNames = _.chain(transactions)
            .map((_t) => _t.category)
            .uniq()
            .filter(Boolean)
            .value();
        const categoryCount = await this.model('category').countDocuments({
            userId,
            type: CategoryType.group,
            name: {
                $in: categoryNames,
            },
        });
        if (categoryCount > 0) {
            return false;
        }

        return transactions.every((_transactions) => {
            if (typeof _transactions.description !== 'string' || !_transactions.description) {
                return false;
            } else if (typeof _transactions.amount !== 'number') {
                return false;
            } else if (typeof _transactions.account !== 'string' || !_transactions.account) {
                return false;
            } else if (typeof _transactions.currency !== 'string' || !_transactions.currency) {
                return false;
            } else if (!_transactions.date || isNaN(new Date(_transactions.date))) {
                return false;
            }
            return true;
        });
    }
}

transactionSchema.loadClass(Transaction);
module.exports = transactionSchema;
