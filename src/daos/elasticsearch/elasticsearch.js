/**
 * Created by hugo on 2018-05-13.
 */
const mongoose = require('mongoose');
const _ = require('lodash');
const { Client } = require('es7');

const config = require('../../../mainConfig');
const requestBuilder = require('./esRequestBuilder');
const logger = require('../../modules/simpleLogger');

const _client = new Client({
    node: config.es_host,
});

class elasticsearchCtrl {
    static async waitForConnection() {
        try {
            await _client.ping();
        } catch (err) {
            logger.log('Could not connect to the Elasticsearch service. Will retry in 5 seconds.');

            let retries;
            for (retries = 0; retries < 10; retries++) {
                try {
                    logger.log(`Elasticsearch connection retry #${retries}...`);

                    await _client.ping();
                    return;
                } catch (_) {
                    logger.log('Could not connect to the Elasticsearch service. Will retry in 5 seconds.');
                }

                await new Promise((resolve) => setTimeout(resolve, 5000));
            }

            if (retries === 10) {
                throw err;
            }
        }
    }

    static get client() {
        return _client;
    }

    static async exists() {
        return _client.indices.exists({
            index: config.instance,
        });
    }

    static async initialize() {
        return _client.indices.create(requestBuilder.initialize());
    }

    static async deleteIndex() {
        return _client.indices.delete({
            index: config.instance,
        });
    }

    /**
     * @param userId {string}
     * @param transactions {{ descriptionInfo?: { type: string, main: string, location: string }, description: string }[]}
     * @param [fileType] {number}
     */
    static async createCategoryGuesstimator(userId, transactions, fileType) {
        const uniqueDescriptions = _.chain(transactions)
            .map((_record) => _record.descriptionInfo || _record.description)
            .uniqBy((_info) => JSON.stringify(_info))
            .value();
        if (_.isEmpty(uniqueDescriptions)) {
            return [];
        }

        let body = [];

        uniqueDescriptions.forEach((info) => {
            body.push({}, requestBuilder.mostRelevantCategory(userId, info, fileType));
        });

        let res = await _client.msearch({
            index: config.instance,
            body: body,
        });
        let results = [];
        res.body.responses.forEach((r, i) => {
            if (r?.hits?.hits?.length > 0) {
                let firstHit = r.hits.hits[0];
                results.push({
                    transactionId: firstHit._id,
                    score: firstHit._score,
                    descriptionInfo: uniqueDescriptions[i],
                    category: firstHit._source && firstHit._source.category,
                });
            }
        });

        return {
            find(descriptionInfo) {
                return results.find((result) => _.isEqual(descriptionInfo, result.descriptionInfo));
            },
        };
    }

    /**
     * @param userId {string}
     * @param descriptionInfo {string | { type: string, main: string, location: string }}
     * @param [fileType] {number}
     */
    static async getRelevantCategories(userId, descriptionInfo, fileType) {
        const response = await _client.search({
            index: config.instance,
            size: 200,
            body: requestBuilder.mostRelevantCategory(userId, descriptionInfo, fileType),
        });
        const hits =
            response?.body?.hits?.hits?.map((item) => ({ score: item._score, value: item._source.category })) || [];
        return _.uniqBy(hits, 'value');
    }

    /**
     * @param userId {string}
     * @param query {string}
     * @param size {number}
     * @param skip {number}
     * @returns {Promise<number[]>}
     */
    static async search(userId, query, size, skip, from, to) {
        let transactionIds = [];

        try {
            let response = await _client.search({
                index: config.instance,
                size: size,
                from: skip,
                body: requestBuilder.userSearch(userId, query, from, to),
            });

            if (response?.body?.hits?.hits?.length > 0) {
                for (let hit of response.body.hits.hits) {
                    transactionIds.push(hit._id);
                }
            }
        } catch (err) {
            logger.warning(`user search error: ${err.message}`);
        }

        return transactionIds;
    }

    /**
     * Bulk index transactions
     */
    static async indexTransactions(transactions, fileType) {
        let body = [];
        transactions.forEach((t) => {
            body.push({
                index: { _id: t.id },
            });
            let esTransaction = t.toEsEntity();
            esTransaction.accountType = fileType;
            body.push(esTransaction);
        });
        await _client.bulk({
            _source: false,
            index: config.instance,
            body: body,
        });
    }

    static async upsertTransactions(transactions) {
        let body = [];
        transactions.forEach((t) => {
            body.push({
                update: { _id: t.id },
            });
            body.push({
                doc: t,
                doc_as_upsert: true,
            });
        });
        if (_.isEmpty(body)) {
            return;
        }
        await _client.bulk({
            _source: false,
            index: config.instance,
            body,
        });
    }

    /**
     * Delete all transactions with statementId
     * @param statementId {string}
     */
    static async deleteTransactionsByStatement(statementId) {
        await _client.deleteByQuery({
            index: config.instance,
            body: {
                query: {
                    term: { statement: statementId },
                },
            },
        });
    }

    static async deleteTransaction(transactionId) {
        await _client.delete({
            id: transactionId,
            index: config.instance,
        });
    }

    static async deleteTransactionsById(transactionIds) {
        const body = transactionIds.map((id) => ({ delete: { _id: id } }));
        if (_.isEmpty(body)) {
            return;
        }
        await _client.bulk({
            _source: false,
            index: config.instance,
            body,
        });
    }

    /**
     * Delete and recreate the elasticsearch index from the database
     * @returns {Promise<void>}
     */
    static async reindex() {
        logger.log(`Reindexing the Elasticsearch database...`);

        // local require to prevent circular references
        const { Statement } = require('../statement');
        const TransactionSchema = require('../transaction');
        const Transaction = mongoose.model('transaction', TransactionSchema);

        const batchSize = 1000;

        await this.deleteIndex();
        await this.initialize();

        let approvedStatements = await Statement.find({ isApproved: true });

        for (let fileType = 0; fileType < 4; fileType++) {
            let statementIds = approvedStatements.filter((s) => s.fileType === fileType).map((s) => s._id);
            if (statementIds.length < 1) continue;

            let transactions = await Transaction.getWithCategoryHierarchy({
                isPending: { $ne: true },
                statementId: {
                    $in: statementIds,
                },
            });
            for (let _transaction of transactions) {
                let sortedCategories = _.sortBy(_transaction.category_hierarchy, 'depth').reverse();
                _transaction.categories = sortedCategories.map((_cat) => _cat.name.toLowerCase());
            }

            let batchCount = Math.ceil(transactions.length / batchSize);
            for (let i = 0; i < batchCount; i++) {
                let transactionBatch = transactions.slice(i * batchSize, (i + 1) * batchSize);

                let body = [];
                transactionBatch.forEach((_transaction) => {
                    body.push({
                        index: { _id: _transaction._id.toString() },
                    });

                    _transaction._id = null;
                    _transaction.category_hierarchy = null;

                    _transaction.description = _transaction.description?.toLowerCase() || null;
                    _transaction.type = _transaction.descriptionInfo?.type?.toLowerCase() || null;
                    _transaction.location = _transaction.descriptionInfo?.location?.toLowerCase() || null;
                    _transaction.descriptionMain = _transaction.descriptionInfo?.main?.toLowerCase() || null;
                    _transaction.descriptionInfo = null;

                    _transaction.category = _transaction.category && _transaction.category.toLowerCase();
                    _transaction.accountType = fileType;

                    // remove empty fields
                    for (let key in _transaction) {
                        if (_transaction.hasOwnProperty(key)) {
                            if (
                                _transaction[key] === null ||
                                _transaction[key] === undefined ||
                                _transaction[key] === ''
                            ) {
                                delete _transaction[key];
                            }
                        }
                    }

                    body.push(_transaction);
                });
                await _client.bulk({
                    _source: false,
                    index: config.instance,
                    body: body,
                });
            }
        }

        logger.log(`Reindex done !`);
    }
}

module.exports = elasticsearchCtrl;
