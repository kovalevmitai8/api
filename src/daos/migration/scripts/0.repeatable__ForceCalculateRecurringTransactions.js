const mongoose = require('mongoose');
const AbstractMigrationScript = require('../AbstractMigrationScript');
const { findRelatedTransactions } = require('../../../controllers/budget');
const Transaction = mongoose.model('transaction', require('../../transaction'));
const { Budget } = require('../../budget');

class ForceCalculateRecurringTransactions extends AbstractMigrationScript {
    static get executeOnce() {
        return false;
    }

    static async migrate() {
        await Transaction.updateMany(
            {},
            {
                $set: { recurring: false },
            }
        );
        for (const budget of await Budget.find()) {
            for (const budgetEntry of budget.entries.filter((e) => e.isComplex)) {
                const matchedTransactionIds = await findRelatedTransactions(budgetEntry);
                await Transaction.updateMany({ _id: { $in: matchedTransactionIds } }, { recurring: true });
            }
        }
    }
}
module.exports = ForceCalculateRecurringTransactions;
