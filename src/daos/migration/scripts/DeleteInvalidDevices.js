const AbstractMigrationScript = require('../AbstractMigrationScript');
const { User } = require('../../user');

class DeleteInvalidDevices extends AbstractMigrationScript {
    static async migrate() {
        const users = await User.find();
        for (const user of users) {
            user.devices = user.devices.filter((d) => d.deviceToken);
            user.markModified('devices');
            await user.save();
        }
    }
}
module.exports = DeleteInvalidDevices;
