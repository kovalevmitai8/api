const AbstractMigrationScript = require('../AbstractMigrationScript');
const { User } = require('../../user');
const { AuthenticationType } = require('../../../models/enums');

class NewAuthenticatedTypeField extends AbstractMigrationScript {
    static async migrate() {
        const users = await User.find();
        for (let user of users) {
            if (user.googleUserId === 'demo_user') {
                user.authenticationType = AuthenticationType.demo;
            } else {
                user.authenticationType = AuthenticationType.google;
            }
            await user.save();
        }
    }
}
module.exports = NewAuthenticatedTypeField;
