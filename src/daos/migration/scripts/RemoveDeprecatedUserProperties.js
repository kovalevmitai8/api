const AbstractMigrationScript = require('../AbstractMigrationScript');
const { User } = require('../../user');

class RemoveDeprecatedUserProperties extends AbstractMigrationScript {
    static async migrate() {
        for (let user of await User.find()) {
            user.set('lastRefreshToken', undefined, { strict: false });
            user.set('pendingCacheEvents', undefined, { strict: false });
            user.set('pendingCacheExpirationEvents', undefined, { strict: false });
            user.set('deviceHistory', undefined, { strict: false });
            await user.save();
        }
    }
}
module.exports = RemoveDeprecatedUserProperties;
