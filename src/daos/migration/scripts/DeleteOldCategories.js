const AbstractMigrationScript = require('../AbstractMigrationScript');
const mongoose = require('mongoose');
const { Category } = require('../../category');
const { User } = require('../../user');
const { Budget } = require('../../budget');
const { CategoryType } = require('../../../models/enums');
const _ = require('lodash');
const dayjs = require('dayjs');
const transactionCtrl = require('../../../controllers/transaction');
const { getCategories } = require('../../../controllers/budget');

class DeleteOldCategories extends AbstractMigrationScript {
    static async migrate() {
        for (let user of await User.find({})) {
            let budget = await Budget.get(user.googleUserId, dayjs().year() - 1);
            if (!budget) {
                continue;
            }
            let categories = (await getCategories(budget)).filter((category) => category.type === CategoryType.base);
            const previousYearActivityMap = await transactionCtrl.getLastActivity(
                user.googleUserId,
                dayjs().year(budget.year).startOf('year'),
                dayjs().year(budget.year).endOf('year'),
                categories
            );
            const namesToKeep = _.keys(previousYearActivityMap).map(
                (categoryId) => categories.find((c) => c._id.equals(categoryId)).name
            );
            categories = await Category.find({ userId: user.googleUserId, type: CategoryType.base });
            const currentYearActivityMap = await transactionCtrl.getLastActivity(
                user.googleUserId,
                dayjs().startOf('year'),
                dayjs().endOf('year'),
                categories
            );
            namesToKeep.push(
                ..._.keys(currentYearActivityMap).map(
                    (categoryId) => categories.find((c) => c._id.equals(categoryId)).name
                )
            );
            budget = await Budget.get(user.googleUserId, dayjs().year());
            namesToKeep.push(
                ...budget.entries
                    .map((entry) => categories.find((c) => c._id.equals(entry.categoryId))?.name)
                    .filter(Boolean)
            );
            await Category.deleteMany({
                name: {
                    $nin: namesToKeep,
                },
                type: CategoryType.base,
                creationDate: {
                    $lt: dayjs().subtract(1, 'year').toDate(),
                },
            });
        }
    }
}
module.exports = DeleteOldCategories;
