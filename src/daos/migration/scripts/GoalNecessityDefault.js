const AbstractMigrationScript = require('../AbstractMigrationScript');
const { Goal } = require('../../goal');

class GoalNecessityDefault extends AbstractMigrationScript {
    static async migrate() {
        await Goal.updateMany(
            {
                necessity: { $exists: false },
            },
            {
                $set: { necessity: 3 },
            }
        );
    }
}
module.exports = GoalNecessityDefault;
