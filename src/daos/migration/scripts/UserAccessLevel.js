const AbstractMigrationScript = require('../AbstractMigrationScript');
const { User } = require('../../user');
const { UserLevel } = require('../../../models/enums');

class UserAccessLevel extends AbstractMigrationScript {
    static async migrate() {
        let users = await User.find({});
        for (let user of users) {
            switch (user.level) {
                case '0': {
                    user.level = UserLevel.user;
                    break;
                }
                case '1': {
                    user.level = UserLevel.admin;
                    break;
                }
            }
            await user.save();
        }
    }
}
module.exports = UserAccessLevel;
