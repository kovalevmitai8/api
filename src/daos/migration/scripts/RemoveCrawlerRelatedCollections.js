const mongoose = require('mongoose');
const mongodb = mongoose.connection.db;
const AbstractMigrationScript = require('../AbstractMigrationScript');

class RemoveCrawlerRelatedCollections extends AbstractMigrationScript {
    static async migrate() {
        await mongodb.dropCollection('scrapingDataSet');
        await mongodb.dropCollection('events');
    }
}
module.exports = RemoveCrawlerRelatedCollections;
