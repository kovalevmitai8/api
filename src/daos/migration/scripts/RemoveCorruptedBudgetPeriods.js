const mongoose = require('mongoose');
const _ = require('lodash');
const AbstractMigrationScript = require('../AbstractMigrationScript');
const { Budget } = require('../../budget');
const { getCategories } = require('../../../controllers/budget');

class RemoveCorruptedBudgetPeriods extends AbstractMigrationScript {
    static async migrate() {
        const budgets = await Budget.find();
        for (let budget of budgets) {
            const categories = await getCategories(budget);
            for (let entry of budget.entries) {
                if (!categories.find((_c) => _c._id.toString() === entry.categoryId.toString())) {
                    entry.remove();
                }
            }
            await budget.save();
        }
    }
}
module.exports = RemoveCorruptedBudgetPeriods;
