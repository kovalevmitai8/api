const mongoose = require('mongoose');
const _ = require('lodash');

const AbstractMigrationScript = require('../AbstractMigrationScript');
const { Category } = require('../../category');
const { Budget } = require('../../budget');

class NewBudgetFormat extends AbstractMigrationScript {
    static async migrate() {
        let categoriesWithBudget = await Category.find({
            budget: { $exists: true },
            'budget.creationDate': {
                $exists: true,
                $ne: null,
            },
        });

        let groupedCategories = _.groupBy(categoriesWithBudget, (_category) => _category.userId);
        for (let [userId, categories] of Object.entries(groupedCategories)) {
            let existingBudgets = await Budget.find().forUser(userId);
            for (let _budget of existingBudgets) {
                await _budget.remove();
            }

            let categoriesSnapshot = await Category.find().forUser(userId);
            let lastYearBudget = new Budget({
                userId,
                year: new Date().getFullYear() - 1,
                plannedLineData: [], // TODO: We should init that too, but its too much work and it'll work anyway.
                entries: categories.map((_category) => ({
                    categoryId: _category._id,
                    isComplex: false,
                    amount: _category.budget.amount,
                    period: _category.budget.period,
                })),
                categoriesSnapshot: categoriesSnapshot.map((_cat) => _.omit(_cat, ['budget'])),
            });
            await lastYearBudget.save();

            let currentBudget = new Budget({
                userId,
                year: new Date().getFullYear(),
                plannedLineData: [], // TODO: We should init that too, but its too much work and it'll work anyway.
                entries: categories.map((_category) => ({
                    categoryId: _category._id,
                    isComplex: false,
                    amount: _category.budget.amount,
                    period: _category.budget.period,
                })),
            });
            await currentBudget.save();
        }
    }
}
module.exports = NewBudgetFormat;
