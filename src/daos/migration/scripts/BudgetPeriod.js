const AbstractMigrationScript = require('../AbstractMigrationScript');
const { Category } = require('../../category');

class CategorySystemType extends AbstractMigrationScript {
    static async migrate() {
        let categoriesToMigrate = await Category.find({
            budget: { $exists: true },
            'budget.period': { $exists: true },
        });
        for (let category of categoriesToMigrate) {
            if (parseFloat(category.budget.period) > 0) {
                category.budget.period = this._getPeriodShorthand(category.budget.period);
            }
            await category.save();
        }
    }

    static _getPeriodShorthand(periodInDays) {
        switch (parseFloat(periodInDays)) {
            case 7:
                return 'w';
            case 30.5:
                return 'M';
            case 121.75:
                return 't';
            case 365.25:
                return 'y';
        }
    }
}
module.exports = CategorySystemType;
