const AbstractMigrationScript = require('../AbstractMigrationScript');
const mongoose = require('mongoose');
const { Budget } = require('../../budget');

class UserAccessLevel extends AbstractMigrationScript {
    static async migrate() {
        for (const budget of await Budget.find()) {
            for (const entry of budget.entries) {
                if (entry.isComplex && entry.period) {
                    entry.rules.forEach((rule) => (rule.period = entry.period));
                    entry.period = undefined;
                }
            }
            budget.markModified('entries');
            await budget.save();
        }
    }
}
module.exports = UserAccessLevel;
