const mongoose = require('mongoose');
const AbstractMigrationScript = require('../AbstractMigrationScript');
const { Budget } = require('../../budget');
const { Category } = require('../../category');

class RemoveFuturBudgets extends AbstractMigrationScript {
    static async migrate() {
        for (let budget of await Budget.find({ year: { $gt: new Date().getFullYear() } })) {
            const currentBudget = await Budget.get(budget.userId, new Date().getFullYear());
            if (currentBudget && currentBudget.isClosed) {
                // restore previous year's categories and delete the budget.
                await Category.deleteMany({ userId: budget.userId });
                await Category.insertMany(currentBudget.categoriesSnapshot);
                currentBudget.categoriesSnapshot = [];
                await currentBudget.save();
                await budget.remove();
            }
        }
    }
}
module.exports = RemoveFuturBudgets;
