const mongoose = require('mongoose');
const AbstractMigrationScript = require('../AbstractMigrationScript');
const Transaction = mongoose.model('transaction', require('../../transaction'));

class OwnAccountNameRefactor extends AbstractMigrationScript {
    static async migrate() {
        const transactions = await Transaction.find().forUser('103617635888205550581');
        for (let transaction of transactions) {
            const account = (transaction.account && transaction.account.toLowerCase()) || '';
            if (account.includes('visa')) {
                transaction.account = '5011-CC';
                await transaction.save();
            } else if (account.includes('desjardins')) {
                const accountName = transaction.account.split(' ').pop();
                transaction.account = `302221-${accountName}`;
                await transaction.save();
            }
        }
    }
}
module.exports = OwnAccountNameRefactor;
