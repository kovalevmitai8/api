const AbstractMigrationScript = require('../AbstractMigrationScript');
const { Category } = require('../../category');

class InlineCategoryBudgetRemoval extends AbstractMigrationScript {
    static async migrate() {
        let categories = await Category.find({});
        for (let category of categories) {
            category.budget = null;
            await category.save();
        }
    }
}
module.exports = InlineCategoryBudgetRemoval;
