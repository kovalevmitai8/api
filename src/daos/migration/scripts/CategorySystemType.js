const mongoose = require('mongoose');
const AbstractMigrationScript = require('../AbstractMigrationScript');
const { Category } = require('../../category');

class CategorySystemType extends AbstractMigrationScript {
    static async migrate() {
        let categoriesToMigrate = await Category.find({
            systemType: [1, 2],
        });
        for (let category of categoriesToMigrate) {
            category.systemType = undefined;
            await category.save();
        }
    }
}
module.exports = CategorySystemType;
