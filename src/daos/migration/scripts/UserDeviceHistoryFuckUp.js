const AbstractMigrationScript = require('../AbstractMigrationScript');
const _ = require('lodash');
const { User } = require('../../user');

class UserDeviceHistoryFuckUp extends AbstractMigrationScript {
    static async migrate() {
        let users = await User.find({});
        // Cleanup a beutiful fuck-up where arrays of null were added as device and location
        // cause _.pluck was used where _.pick should have. Yay for not testing before prod deploy !
        for (let user of users) {
            if (user.devices) {
                user.devices = user.devices.filter((_dev) => !_.isArray(_dev));
            }
            if (user.locations) {
                user.locations = user.locations.filter((_loc) => !_.isArray(_loc));
            }
            await user.save();
        }
    }
}
module.exports = UserDeviceHistoryFuckUp;
