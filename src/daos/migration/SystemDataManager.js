const util = require('util');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');

const readdir = util.promisify(fs.readdir);
const writeFile = util.promisify(fs.writeFile);

const config = require('../../../mainConfig');
const { User } = require('../user');
const { UserLevel } = require('../../models/enums');
const service = require('../../services/service');
const logger = require('../../modules/simpleLogger');

class SystemDataManager {
    static async initData() {
        const demoUser = await User.createIfNotExist({
            sub: 'demo_user',
            email: '',
            given_name: 'demo',
            family_name: 'user',
        });
        demoUser.level = UserLevel.readOnly;
        await demoUser.save();
    }

    /**
     * Execute data migration. Everything here must be repeatable !
     * @returns {Promise<boolean>}
     */
    static async migrateData() {
        let successfulMigration = [];
        let migrationState;
        let migrationClasses;

        try {
            migrationState = await this._getMigrationState();
        } catch (err) {
            await this._handleMigrationError('Error reading the migration state config file', err);
            return false;
        }
        try {
            migrationClasses = await this._loadMigrationScripts();
        } catch (err) {
            await this._handleMigrationError('Error loading migration scripts', err);
            return false;
        }
        try {
            let filteredMigrations = {};
            for (let [className, migrationScript] of Object.entries(migrationClasses)) {
                if (!migrationState[className]) {
                    filteredMigrations[className] = migrationScript;
                }
            }
            await Promise.all(
                _.map(filteredMigrations, async (migrationScript, className) => {
                    logger.log(`Executing script '${className}'...`);
                    await migrationScript.migrate();
                    if (migrationScript.executeOnce) {
                        successfulMigration.push(className);
                    }
                })
            );
        } catch (err) {
            await this._handleMigrationError('Error during the data migration', err);
            return false;
        }
        try {
            await this._saveMigrationState(migrationState, successfulMigration);
        } catch (err) {
            await this._handleMigrationError('Error saving the migration state config file', err);
            return false;
        }
        return true;
    }

    /**
     * Send the error details to the admin
     * @param message
     * @param err
     * @returns {Promise<void>}
     * @private
     */
    static async _handleMigrationError(message, err) {
        if (process.env.NODE_ENV === 'DEV' || process.env.NODE_ENV === 'CI') {
            logger.error('--- Data migration error ---\n===========================================');
            logger.error(message);
            logger.error(err);
            return;
        }
        await service.mailer.sendError({
            from: config.system_emails.mailer,
            to: config.system_emails.admin,
            apiAddress: config.api_address,
            moduleName: 'Data migration service',
            errorMessage: message,
            errorStackTrace: err.stack || '',
        });
    }

    /**
     * Read the state of migration script from the migration state file
     * @returns {Promise<*>}
     * @private
     */
    static async _getMigrationState() {
        let migrationConfigState = {};
        try {
            migrationConfigState = require('../../../config/migrationState.json');
        } catch (e) {}
        return migrationConfigState;
    }

    /**
     * Save the successful migration to the state file
     * @param migrationConfigState
     * @param successfulMigrations
     * @returns {Promise<void>}
     * @private
     */
    static async _saveMigrationState(migrationConfigState, successfulMigrations) {
        for (let className of successfulMigrations) {
            migrationConfigState[className] = new Date().toISOString();
        }
        await writeFile(
            path.join(__dirname, '../../../config/migrationState.json'),
            JSON.stringify(migrationConfigState),
            {
                encoding: 'utf-8',
            }
        );
    }

    /**
     * Find and require all migration scripts
     * @returns {Promise<*>}
     * @private
     */
    static async _loadMigrationScripts() {
        const migrationScriptsBasePath = path.join(__dirname, 'scripts');
        let migrationClasses = {};
        let files = await readdir(migrationScriptsBasePath);
        for (let file of files) {
            migrationClasses[file] = require(path.join(migrationScriptsBasePath, file));
        }
        return migrationClasses;
    }
}
module.exports = SystemDataManager;
