/**
 * Created by hugo on 2018-05-13.
 */

import mongoose from 'mongoose';
import * as logger from '../modules/simpleLogger';

const config = require('../../mainConfig');

mongoose.Promise = global.Promise;
mongoose
    .connect(config.mongodb_host + config.instance, {})
    .catch((err: any) => logger.error('Error loading database: ' + err));
