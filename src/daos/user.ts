/**
 * Created by hugo on 2018-12-29
 */
import { HydratedDocument, Model, Schema, model, Types } from 'mongoose';
import { uniq } from 'lodash';
import { UserLevel, AuthenticationType } from '../models/enums';

export interface IUserDevice {
    _id?: Types.ObjectId;
    platform?: string;
    manufacturer?: string;
    model?: string;
    uuid?: string;
    version?: string;
    lastLocation?: string;
    timestamp?: number;
    refreshToken?: string;
    deviceToken?: string;
    pendingCacheExpirationEvents?: Array<string>;
    verified?: boolean;
}

export interface IUserLocation {
    _id?: Types.ObjectId;
    country?: string;
    region?: string;
    city?: string;
    ll?: [number, number];
    ip?: string;
    timestamp?: number;
}

interface IApiAccess {
    _id: Types.ObjectId;
    creationDate: Date;
    apiKey: string;
    lastActivity: Date;
    disabled: boolean;
}

export interface IUser {
    googleUserId: string;
    email?: string;
    givenName?: string;
    familyName?: string;
    creationDate: Date;
    lastLogin?: Date;
    level: UserLevel;
    authenticationType?: AuthenticationType;
    locations: [IUserLocation];
    devices: [IUserDevice];
    apiAccess: [IApiAccess];
}

interface IUserMethods {
    getDevice(deviceToken: string): any;
    getLocation(ip: string): any;
    addCacheExpirationEvents(events: string[]): Promise<void>;
}

export type HydratedUser = HydratedDocument<IUser, IUserMethods>;

interface UserDocument extends IUser, Document, IUserMethods {}

interface UserModel extends Model<UserDocument, {}, IUserMethods> {
    createIfNotExist(payload: any): Promise<HydratedDocument<UserDocument, IUserMethods>>;
}

let userSchema = new Schema<UserDocument, UserModel, IUserMethods>({
    googleUserId: { type: String, unique: true },
    email: String,
    givenName: String,
    familyName: String,
    creationDate: { type: Schema.Types.Date, default: Date.now },
    lastLogin: Schema.Types.Date,
    level: { type: String, enum: [UserLevel.user, UserLevel.admin, UserLevel.readOnly], default: UserLevel.user },
    authenticationType: {
        type: String,
        enum: [AuthenticationType.demo, AuthenticationType.google, AuthenticationType.local],
    },
    locations: [
        {
            country: String,
            region: String,
            city: String,
            ll: [Number], // this is longitude - latitude coordinates
            ip: String,
            timestamp: Number,
        },
    ],
    devices: [
        {
            platform: String,
            manufacturer: String,
            model: String,
            uuid: String,
            version: String,
            lastLocation: String,
            timestamp: Number,
            refreshToken: String,
            deviceToken: String,
            pendingCacheExpirationEvents: { type: [String], default: [] },
            verified: Boolean,
        },
    ],
    apiAccess: [
        {
            creationDate: { type: Schema.Types.Date, default: Date.now },
            apiKey: String,
            lastActivity: Schema.Types.Date,
            disabled: Boolean,
        },
    ],
});

userSchema.method('getDevice', function getDevice(deviceToken: string) {
    return (<HydratedDocument<IUser>>this).devices.find((device) => device.deviceToken === deviceToken);
});

userSchema.method('getLocation', function getLocation(ip: string) {
    return (<HydratedDocument<IUser>>this).locations.find((location) => location.ip === ip);
});

userSchema.method('addCacheExpirationEvents', async function addCacheExpirationEvents(events: string[]) {
    for (let device of (<HydratedDocument<IUser>>this).devices) {
        device.pendingCacheExpirationEvents = uniq([...device.pendingCacheExpirationEvents, ...events]);
    }
    this.markModified('devices');
    await this.save();
});

userSchema.static('createIfNotExist', async function createIfNotExist(payload) {
    const { sub, email, given_name, family_name } = payload;
    await this.findOneAndUpdate(
        { googleUserId: sub },
        {
            $set: {
                googleUserId: sub,
                email: email,
                givenName: given_name,
                familyName: family_name,
                lastLogin: Date.now(),
            },
        },
        { upsert: true }
    );
    return this.findOne({ googleUserId: sub });
});

export { userSchema };
export const User = model<UserDocument, UserModel>('user', userSchema);
