/**
 * Created by hugo on 2018-05-13.
 */

import { extend, flatten } from 'lodash';
import { Schema, Model, LeanDocument, model, Document } from 'mongoose';
import { CategoryType, SystemCategory } from '../models/enums';
import { forUser, IGenericSchema, GenericQueryHelpers, GenericSchema } from './genericModel';
import { Category as CategoryDto } from '../models/entities';

export interface ICategory extends IGenericSchema {
    name: string;
    creationDate: Date;
    type: CategoryType;
    systemType?: SystemCategory;
    hue?: string;
    parent?: string;
}

interface ICategoryMethods {
    toDto(): CategoryDto;
}

export type CategoryDocument = ICategory & Document & ICategoryMethods;

interface CategoryModel extends Model<CategoryDocument, GenericQueryHelpers<CategoryDocument>, ICategoryMethods> {
    findChildBaseCategories(
        userId: string,
        parentName: string,
        categories?: Array<LeanDocument<CategoryDocument>>
    ): Promise<Array<Partial<ICategory>>>;
    findSubCategories(
        userId: string,
        parentName: string,
        categories: Array<ICategory>
    ): Promise<Array<ICategory>>;
    createIfNotExists(
        userId: string,
        type: CategoryType,
        categoryName: string,
        systemType: SystemCategory
    ): Promise<void>;
    toDto(category: LeanDocument<CategoryDocument>): CategoryDto;
    createMissingBaseCategories(userId: string, categoryNames: string[]): Promise<void>;
}

/**
 * @class Category
 */
let categorySchema = new Schema<
    CategoryDocument,
    CategoryModel,
    ICategoryMethods,
    GenericQueryHelpers<CategoryDocument>
>(
    extend(
        {
            name: { type: String, required: true },
            creationDate: { type: Schema.Types.Date, default: Date.now },
            type: { type: Number, enum: [CategoryType.base, CategoryType.group], required: true },
            systemType: {
                type: Number,
                enum: [SystemCategory.goal, SystemCategory.revenu, SystemCategory.virement],
            },
            hue: { type: String },
            parent: String,
        },
        GenericSchema
    ),
    { collection: 'categories' }
);
categorySchema.index({ name: 1, userId: 1, parent: 1 });

categorySchema.query.forUser = forUser<CategoryDocument, GenericQueryHelpers<CategoryDocument>>;

categorySchema.method('toDto', function toDto(): CategoryDto {
    return Category.toDto(this);
});

categorySchema.static('findChildBaseCategories', async function findChildBaseCategories(
    userId: string,
    parentName: string,
    categories?: Array<LeanDocument<CategoryDocument>>
) {
    const allCategories = categories || (await this.find().forUser(userId).select('name parent type').lean());

    const search = (parentName: string) => {
        const subCategories = allCategories.filter((c) => c.parent === parentName);
        const subCatArrays = [[parentName]];
        for (let subCat of subCategories) {
            subCatArrays.push(search(subCat.name));
        }
        return flatten(subCatArrays);
    };

    const subCategoryNames = search(parentName);
    return allCategories.filter((_cat) => _cat.type === CategoryType.base && subCategoryNames.includes(_cat.name));
});

categorySchema.static('findSubCategories', async function findSubCategories(
    userId: string,
    parentName: string,
    categories: Array<ICategory>
) {
    const allCategories = categories || (await this.find().forUser(userId).lean());

    const search = (parent: ICategory, ignoreSelf = false) => {
        let subCategories = allCategories.filter((c) => c.parent === parent.name);
        let subCatArrays = ignoreSelf ? [] : [parent];
        for (let subCat of subCategories) {
            subCatArrays.push(...search(subCat));
        }
        return flatten(subCatArrays);
    };

    return search(
        allCategories.find((_cat) => _cat.name === parentName),
        true
    );
});

categorySchema.static('createIfNotExists', async function createIfNotExists(
    userId: string,
    type: CategoryType,
    categoryName: string,
    systemType: SystemCategory
) {
    let category = await this.findOne({
        userId: userId,
        name: categoryName,
    });
    if (!category) {
        await this.insertMany([
            {
                userId: userId,
                name: categoryName,
                type: type,
                systemType: systemType || null,
            },
        ]);
    }
});

categorySchema.static('toDto', function toDto(category: (CategoryDocument | LeanDocument<CategoryDocument>) & { children: string[] }) {
    return {
        id: category._id?.toString() || category.id,
        type: category.type,
        name: category.name,
        systemType: category.systemType,
        hue: category.hue,
        parent: category.parent,
        creationDate: category.creationDate,
        children: category.children || null,
    }
});

categorySchema.static('createMissingBaseCategories', async function createMissingBaseCategories(userId: string, categoryNames: string[]) {
    const existingCategories = await this.find({
        name: {
            $in: categoryNames
        }
    }).forUser(userId).select('name').lean();
    const existingCategoryNames = existingCategories.map(category => category.name);
    const missingCategories = categoryNames.filter(name => !existingCategoryNames.includes(name));
    const newCategories = missingCategories.map(name => ({
        userId,
        name,
        type: CategoryType.base,
    }));
    await this.insertMany(newCategories);
});

export { categorySchema };
export const Category = model<CategoryDocument, CategoryModel>('category', categorySchema);
