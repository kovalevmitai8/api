import { first } from 'lodash';

export enum UserLevel {
    user = 'user',
    admin = 'admin',
    readOnly = 'readOnly',
}

export enum AttachmentType {
    file = 'file',
    pdf = 'pdf',
    json = 'json',
    eml = 'eml',
}

export enum AuthenticationType {
    google = 'google',
    local = 'local',
    demo = 'demo',
}

export enum CategoryType {
    base = 1,
    group = 2,
    system = 3,
}

export enum GoalNecessity {
    low = 1,
    medium = 3,
    high = 5,
}

export enum StatementFileType {
    desjardins = 0,
    visa = 1,
    paypal = 2,
    webScraper = 3,
}

export function parseStatementFileType(fileType: number): string | null {
    if (Object.values(StatementFileType).includes(fileType)) {
        return Object.entries(StatementFileType).find(([key, value]) => value === fileType)[0];
    }
    return null;
}

export enum SystemCategory {
    virement = 3,
    revenu = 4,
    goal = 5,
}

export function isSpendingSystemCategory(systemCategory: SystemCategory) {
    return [SystemCategory.revenu, SystemCategory.virement].includes(systemCategory) === false;
}

export enum AccountType {
    EOP = 'EOP',
    CS = 'CS',
    ES = 'ES',
    ET = 'ET',
    MC = 'MC',
    PR = 'PR',
    CC = 'CC',
}

export function isSavingsAccount(type: AccountType) {
    return [AccountType.EOP, AccountType.CS, AccountType.ES, AccountType.ET].includes(type);
}

export function isLoanAccount(type: AccountType) {
    return [AccountType.MC, AccountType.PR].includes(type);
}

export function isCreditCard(type: AccountType) {
    return [AccountType.CC].includes(type);
}

export function parseAccountType(accountName: string): AccountType | null {
    const re = /(?=[\d-]*)(EOP|CS|ES|ET|MC|PR|CC)(?=\d*)/gi;
    let maybeType = first(re.exec(accountName));
    if (maybeType in AccountType) {
        return AccountType[maybeType as AccountType];
    }
    return null;
}
