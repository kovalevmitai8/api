import { AttachmentType, CategoryType, SystemCategory, GoalNecessity, StatementFileType } from './enums';
import { Types } from 'mongoose';
import { DateInterval, SimpleDateInterval } from '../modules/dateUtil';

export type Transaction = {
    id: string;
    description: string;
    descriptionInfo?: TransactionDescriptionInfo;
    date: string;
    amount: number;
    currency: string;
    conversionRate: number;
    creationDate: string;
    statementId: string;
    account: number;
    category?: string;
    attachments: Array<TransactionAttachment>;
    categoryHierarchy: Array<CategoryHierarchyItem>;
    recurring: boolean;
    isPending: boolean;
};

export type TransactionDescriptionInfo = {
    type: string;
    main: string;
    location: string;
};

export type TransactionAttachment = {
    fileId: string;
    type: AttachmentType;
    filename: string;
};

export type CategoryHierarchyItem = {
    _id: string;
    name: string;
    hue: string;
    systemType: SystemCategory;
    type: CategoryType;
};
// TODO - types: move to dao
export function toTransactionDto(dbTransaction: any): Transaction {
    return {
        id: dbTransaction._id || dbTransaction.id,
        account: dbTransaction.account,
        currency: dbTransaction.currency,
        amount: dbTransaction.amount,
        attachments: dbTransaction.attachments || [],
        category: dbTransaction.category,
        categoryHierarchy: dbTransaction.categoryHierarchy || [],
        conversionRate: dbTransaction.conversionRate,
        creationDate: dbTransaction.creationDate,
        date: dbTransaction.date,
        recurring: !!dbTransaction.recurring,
        statementId: dbTransaction.statementId,
        description: dbTransaction.description,
        isPending: dbTransaction.isPending || false,
        descriptionInfo: dbTransaction.descriptionInfo || {},
    };
}

export type Goal = {
    id: string;
    name: string;
    amount: number;
    description?: string;
    necessity?: GoalNecessity;
    deadline?: Date;
    transactions?: string[];
    finalAmount?: number;
    completionDate?: Date;
};

export type Category = {
    id: string;
    name: string;
    creationDate: Date;
    type: CategoryType;
    systemType?: SystemCategory;
    hue?: string;
    parent?: string;
    children?: Array<string>;
};

export type Statement = {
    id: string;
    date: Date;
    creationDate: Date;
    fileId?: string;
    fileType: StatementFileType;
    isApproved?: boolean;
};

export type Account = {
    id: string;
    amount: number;
    accountName: string;
    accountType: string;
    updatedAt: Date;
    creationDate: Date;
    isInvestmentAccount: boolean;
    displayName?: string;
    creditLimit?: number;
    disabled?: boolean;
};

export type Budget = {
    id: string;
    isClosed: boolean;
    year: number;
    hasBudget: boolean;
    budgetEntries: BudgetEntry[];
    rootCategories?: BudgetCategory[];
};
export type BudgetEntry = {
    _id: string;
    categoryId: Types.ObjectId;
    isComplex: boolean;
    // fields for simple budget
    amount: number;
    period: SimpleDateInterval;
    // fields for complex budget
    rules: [
        {
            period: DateInterval;
            amount: number;
            startDate: Date;
            endDate: Date;
        }
    ];
};
export type BudgetCategory = Category & {
    children?: BudgetCategory[];
};
