import * as mongoose from 'mongoose';
import { StatementFileType } from '../models/enums';
import * as dayjs from 'dayjs';
import { filterRecurringTransactions } from './budget';
import { Budget } from '../daos/budget';
import { HydratedStatement, Statement } from '../daos/statement';

const esCtrl = require('../daos/elasticsearch/elasticsearch');
const Transaction: any = mongoose.model('transaction', require('../daos/transaction'));

export async function createStatementWithTransactions({
    userId,
    transactions,
    date,
    fileType,
    fileId,
}: {
    userId: string;
    transactions: any[];
    date: Date;
    fileType: StatementFileType;
    fileId: string;
}) {
    let statement: HydratedStatement;
    try {
        let statements = await Statement.insertMany([
            {
                date,
                fileType,
                fileId,
                userId,
            },
        ]);
        statement = statements[0];

        // check if some transactions are recurring
        let recurringTransactions: any[] = [];
        const currentBudget = await Budget.get(userId, dayjs().year(), true);
        if (currentBudget) {
            recurringTransactions = await filterRecurringTransactions(currentBudget, transactions);
        }

        transactions.forEach((_t) => {
            _t.statementId = statement._id;
            _t.userId = userId;
            _t.files = [];
            _t.creationDate = undefined;
            _t.recurring = recurringTransactions.includes(_t);
        });
        await Transaction.insertMany(transactions);

        await esCtrl.upsertTransactions(
            transactions.map((t) => {
                let doc = new Transaction(t);
                return {
                    ...doc.toEsEntity(),
                    accountType: fileType,
                };
            })
        );
    } catch (err) {
        if (statement) {
            await Transaction.remove({ statementId: statement._id });
            await Statement.findByIdAndRemove(statement._id);
        }
        throw err;
    }

    return statement;
}

export async function updateApprovedStatus({ userId, statementId }: { userId: string; statementId: string }) {
    let statement = await Statement.findOne({
        _id: statementId,
        userId,
    });
    // Auto approve the statement if all transactions are assigned to a category.
    if (statement.fileType === StatementFileType.webScraper) {
        let transactions = await Transaction.find({
            statementId: statement.id,
        })
            .forUser(userId)
            .select('category')
            .lean();
        statement.isApproved = transactions.every((t: any) => t.category);
        await statement.save();
    }
}
