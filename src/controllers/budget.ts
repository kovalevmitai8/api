import * as mongoose from 'mongoose';
import { Category, CategoryDocument } from '../daos/category';
import * as _ from 'lodash';
import * as dayjs from 'dayjs';
import { ClientError } from '../routes/utils/errors';
import { Budget, BudgetDocument } from '../daos/budget';
import { LeanDocument } from 'mongoose';
import { BudgetEntryDocument } from '../daos/budgetEntry';
import * as dateHelper from '../modules/dateUtil';
import { BudgetModelingBuilder, BudgetModelType } from './utils/BudgetModeling';
import { createPlannedTransactionMatcher } from './transaction';
const Transaction: any = mongoose.model('transaction', require('../daos/transaction'));

export async function getCategories(
    budget: BudgetDocument
): Promise<Array<CategoryDocument | LeanDocument<CategoryDocument>>> {
    if (budget.isClosed) {
        return budget.categoriesSnapshot || [];
    } else {
        return Category.find().forUser(budget.userId).lean();
    }
}

export async function timeTravel(userId: string, targetYear: number) {
    const budgets = await Budget.find({ userId }).sort('year');
    // fixme: this validation assume that there is not gap year between budgets
    if (_.isEmpty(budgets) || targetYear < _.first(budgets).year || _.last(budgets).year < targetYear) {
        throw new ClientError(`Aucun budget n'existe pour l'année choisie.`);
    }

    // hide every budget (this will snapshot the budget that's currently active, regardless of if its before or
    // after the target date)
    const budgetsToHide = budgets.filter((_budget) => !_budget.isHidden);
    await Promise.all(budgetsToHide.map((_budget) => hideBudget(_budget)));

    // un-hide the budgets before the target (without restoring categories)
    const budgetToUnHide = budgets.filter((_budget) => _budget.year < targetYear && _budget.isHidden);
    await Budget.updateMany(
        { _id: { $in: budgetToUnHide.map((_budget) => _budget._id) } },
        { $set: { isHidden: false } }
    );

    // restore the target budget
    const targetBudget = budgets.find((_budget) => _budget.year === targetYear);
    await restoreBudget(targetBudget);
}

async function hideBudget(budget: BudgetDocument) {
    if (!budget.isClosed) {
        budget.categoriesSnapshot = await Category.find().forUser(budget.userId);
    }
    budget.isHidden = true;

    await budget.save();
}

async function restoreBudget(budget: BudgetDocument) {
    await Category.deleteMany({ userId: budget.userId });
    await Category.insertMany(budget.categoriesSnapshot);
    budget.isHidden = false;
    budget.categoriesSnapshot = [];
    await budget.save();
}

export async function filterRecurringTransactions(budget: BudgetDocument, transactions: any[]) {
    let recurringTransaction = [];
    for (let budgetEntry of budget.entries.filter((entry) => entry.isComplex)) {
        recurringTransaction.push(...(await filterRelatedTransactions(budgetEntry, transactions)));
    }
    return recurringTransaction;
}

async function filterRelatedTransactions(budgetEntry: BudgetEntryDocument, transactions: any[]) {
    if (!budgetEntry.isComplex) {
        return [];
    }

    const maximumSloppinessInDays = budgetEntry.getMaximumSloppinessInDays();

    const budget: BudgetDocument = (<any>budgetEntry).parent();
    const category =
        budget.categoriesSnapshot.find((category) => category._id.equals(budgetEntry.categoryId)) ||
        (await Category.findOne({
            _id: budgetEntry.categoryId.toString(),
        })
            .forUser(budget.userId)
            .lean());
    const baseCategories = await Category.findChildBaseCategories(budget.userId, category.name);
    const baseCategoryNames = baseCategories.map((category) => category.name);

    const _transactions = transactions.filter((_t) => baseCategoryNames.includes(_t.category));
    if (_.isEmpty(_transactions)) {
        return [];
    }

    const startDate = _.minBy(
        _transactions.map((_t) => dayjs(_t.date)),
        (_d) => _d.valueOf()
    );
    const endDate = _.maxBy(
        _transactions.map((_t) => dayjs(_t.date)),
        (_d) => _d.valueOf()
    );
    const plannedTransactions = budgetEntry.getPlannedTransactions(
        startDate.subtract(maximumSloppinessInDays, 'day'),
        endDate.add(maximumSloppinessInDays, 'day')
    );

    const matcher = createPlannedTransactionMatcher(transactions, plannedTransactions, budgetEntry);
    return matcher.transactions.filterIsPlanned();
}

export async function findRelatedTransactions(budgetEntry: BudgetEntryDocument) {
    const budget: BudgetDocument = (<any>budgetEntry).parent();

    // account for a bit of slop in the dates ..
    const startDate = dayjs().year(budget.year).startOf('year').subtract(3, 'day');
    const endDate = dayjs().year(budget.year).endOf('year').add(3, 'day');

    const category =
        budget.categoriesSnapshot.find((_cat) => _cat._id.equals(budgetEntry.categoryId)) ||
        (await Category.findOne({
            _id: budgetEntry.categoryId.toString(),
        })
            .forUser(budget.userId)
            .lean());
    const baseCategories = await Category.findChildBaseCategories(budget.userId, category.name);
    const transactions = await Transaction.find({
        date: {
            $gte: startDate.toISOString(),
            $lt: endDate.toISOString(),
        },
        category: { $in: baseCategories.map((_c) => _c.name) },
    })
        .forUser(budget.userId)
        .sort('date')
        .lean();

    const relatedTransactions = await filterRelatedTransactions(budgetEntry, transactions);
    return relatedTransactions.map((_t) => _t._id.toString());
}

type BudgetStatusesOptions = {
    selectedCategories: Array<CategoryDocument>;
    budget: BudgetDocument;
    budgetModel: BudgetModelType;
    userId: string;
};
export async function getBudgetStatuses({ userId, budget, budgetModel, selectedCategories }: BudgetStatusesOptions) {
    const budgets = [];
    for (let category of selectedCategories) {
        const budgetEntry = budget?.findEntryByCategory(category._id.toString());

        // range of date defaults to the current month, or the period if it's a simple budget
        let startDate =
            !budgetEntry || budgetEntry.isComplex
                ? dateHelper.startOfPeriod('t')
                : dateHelper.startOfPeriod(budgetEntry.period);
        let endDate =
            !budgetEntry || budgetEntry.isComplex
                ? dateHelper.endOfPeriod('t')
                : dateHelper.endOfPeriod(budgetEntry.period);

        const subCategoryNames = budgetModel
            .findNode((node) => node.value.category.name === category.name)
            .getLeaves()
            .map((value) => value.category.name);

        const medianQuery = {
            userId: userId,
            category: subCategoryNames,
            date: {
                $gte: dayjs().subtract(3, 'month').toDate(),
            },
        };
        const count = await Transaction.countDocuments(medianQuery);
        const medianAmountRes = await Transaction.find(medianQuery)
            .sort({ amount: 1 })
            .skip(Math.max(count / 2 - 1, 0))
            .limit(1);

        // if a budget entry exists, use the planned amount specified by the user. Otherwise, calculate it.
        const tempModel = await BudgetModelingBuilder.create({
            userId: userId,
            budgetYear: dayjs().year(),
            from: startDate,
            to: endDate,
        })
            .trimToRoots([category._id.toString()])
            .appendObservedAmount()
            .appendPlannedAmount()
            .build();
        const rootNode = _.first(tempModel.getRootNodes());
        const plannedAmount = rootNode.value?.plannedAmount || 0;
        const observedAmount = rootNode.value?.observedAmount || 0;

        budgets.push({
            categoryId: category._id.toString(),
            observedAmount: Math.abs(observedAmount),
            medianAmount: !_.isEmpty(medianAmountRes) && Math.abs(medianAmountRes[0].amount),
            plannedAmount,
            categoryName: category.name,
            period: (budgetEntry && !budgetEntry.isComplex && budgetEntry.period) || 't',
        });
    }

    return budgets;
}
