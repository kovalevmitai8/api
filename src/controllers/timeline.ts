import * as dayjs from 'dayjs';
import * as _ from 'lodash';
import { BudgetModelingBuilder, BudgetModelType, CategoryInfo } from './utils/BudgetModeling';
import { PlannedTransaction } from '../daos/budgetEntry';
import { Category as CategoryDto, toTransactionDto, Transaction as TransactionDto } from '../models/entities';
import { SystemCategory } from '../models/enums';
import { TreeNode } from '../modules/nTree';
import { Category } from '../daos/category';
import { createPlannedTransactionMatcher } from './transaction';
const esCtrl = require('../daos/elasticsearch/elasticsearch');

export enum TimelineFields {
    categoryHierarchy = 'categoryHierarchy',
    plannedTransactions = 'plannedTransactions',
}

export enum TimelineEntityType {
    transaction = 'transaction',
    category = 'category',
    // TODO: To be determined
    //budgetEntry = 'budgetEntry'
}

type TimelineTransaction = {
    entity: TimelineEntityType;
    plannedAmount?: number;
    plannedDate?: string;
} & Partial<TransactionDto>;

type TimelineCategory = {
    entity: TimelineEntityType;
    observed?: number;
    // planned?: number;
    transactionIds?: string[];
    children?: TimelineCategory[];
} & Partial<Omit<CategoryDto, 'children'>>;

export type TimelineEntity = TimelineTransaction | TimelineCategory;

type ViewBuilderParams = {
    fields?: TimelineFields[];
    granularity?: string;
};

export const timelineViewMapping = {
    [TimelineEntityType.transaction]: buildTransactionView,
    [TimelineEntityType.category]: buildCategoryView,
};

export function splitPeriodByYear(from: dayjs.Dayjs, to: dayjs.Dayjs) {
    let currentYear = from.year();
    const endYear = to.year();
    const periods = [];
    while (currentYear <= endYear) {
        const startOfYear = dayjs().year(currentYear).startOf('year');
        const endOfYear = dayjs().year(currentYear).endOf('year');
        periods.push({
            yearStart: from.isAfter(startOfYear) ? from.clone() : startOfYear,
            yearEnd: to.isBefore(endOfYear) ? to.clone() : endOfYear,
        });
        currentYear++;
    }
    return periods;
}

export async function prepareModel({
    userId,
    entity,
    filter,
    fields,
    search,
}: {
    userId: string;
    entity: TimelineEntityType;
    filter: Record<string, string>;
    fields: TimelineFields[];
    search?: string;
}) {
    let transactionIds = null;
    if (search) {
        let from = filter?.date_from && new Date(filter?.date_from);
        let to = filter?.date_to && new Date(filter?.date_to);
        transactionIds = await esCtrl.search(userId, search, null, null, from, to);
    }

    const budgetModelBuilder = BudgetModelingBuilder.create({
        userId,
        budgetYear: filter?.date_from ? dayjs(filter?.date_from).year() : dayjs().year(),
        from: filter?.date_from ? dayjs(filter?.date_from) : null,
        to: filter?.date_to ? dayjs(filter?.date_to) : null,
        filter,
        transactionIds,
    }).appendTransactions();
    if (entity === TimelineEntityType.category) {
        budgetModelBuilder.appendObservedAmount();
    }
    if (fields?.includes(TimelineFields.plannedTransactions) || entity === TimelineEntityType.category) {
        budgetModelBuilder.appendPlannedAmount();
    }
    return await budgetModelBuilder.build();
}

function buildTransactionView(model: BudgetModelType, params: ViewBuilderParams) {
    const items: TimelineTransaction[] = [];
    const hasCategoryHierarchy = params.fields?.includes(TimelineFields.categoryHierarchy) || undefined;
    const hasPlannedTransactions = params.fields?.includes(TimelineFields.plannedTransactions);

    const hierarchyMap = model.getCategoryHierarchy((categoryInfo) => ({
        _id: categoryInfo.category._id,
        name: categoryInfo.category.name,
        hue: categoryInfo.category.hue,
        systemType: categoryInfo.category.systemType,
        type: categoryInfo.category.type,
    }));
    model.iterate((node) => {
        const { transactions, plannedTransactions, budgetEntry, category } = node.value;

        const matcher = createPlannedTransactionMatcher(transactions, plannedTransactions, budgetEntry);
        let plannedTransactionMapping = new Map<string, PlannedTransaction | null>();
        if (transactions && budgetEntry && !_.isEmpty(plannedTransactions)) {
            plannedTransactionMapping = matcher.pair();
        }
        items.push(
            ...(transactions?.map((t) => {
                const plannedTransaction = plannedTransactionMapping.get(t._id.toString());
                return {
                    ...toTransactionDto(t),
                    entity: TimelineEntityType.transaction,
                    categoryHierarchy: hasCategoryHierarchy && hierarchyMap.get(t.category),
                    recurring: hasPlannedTransactions ? !!plannedTransaction : t.recurring,
                    plannedAmount: plannedTransaction?.amount || null,
                    plannedDate: plannedTransaction?.date ? new Date(plannedTransaction?.date).toISOString() : null,
                };
            }) || [])
        );

        const pendingPlannedTransactions = matcher.plannedTransactions.filterIsPending();
        items.push(
            ...pendingPlannedTransactions.map((t) => ({
                entity: TimelineEntityType.transaction,
                plannedAmount: t?.amount || null,
                plannedDate: t?.date ? new Date(t?.date).toISOString() : null,
                category: category.name,
                categoryHierarchy: hasCategoryHierarchy && hierarchyMap.get(category.name),
                isPending: true,
                recurring: true,
            }))
        );
    });
    return items;
}

function buildCategoryView(model: BudgetModelType): Array<TimelineCategory> {
    const nonTransferRootNodes = model
        .getRootNodes()
        .filter((node) => node.value.category.systemType !== SystemCategory.virement);
    return nonTransferRootNodes.map(buildCategorySpendingTree).filter(Boolean);
}

function buildCategorySpendingTree(node: TreeNode<CategoryInfo>): TimelineCategory {
    let { category, observedAmount, transactions } = node.value;

    if (observedAmount === 0) {
        return null;
    }

    return {
        ...Category.toDto(category),
        observed: observedAmount,
        children: node.children?.map(buildCategorySpendingTree).filter(Boolean),
        transactionIds: transactions?.map((t) => t._id),
        entity: TimelineEntityType.category,
    };
}
