import * as mongoose from 'mongoose';
import { LeanDocument } from 'mongoose';
import * as dayjs from 'dayjs';
import * as _ from 'lodash';
import { Category, CategoryDocument } from '../../daos/category';
import NTree, { TreeNode } from '../../modules/nTree';
import { CategoryType, SystemCategory } from '../../models/enums';
import plotDataBuilder from '../../modules/plotDataBuilder';
import { getCategories } from '../budget';
import { Budget, BudgetDocument } from '../../daos/budget';
import { BudgetEntryDocument, PlannedTransaction } from '../../daos/budgetEntry';
import { searchTransactions } from '../transaction';

const Transaction: any = mongoose.model('transaction', require('../../daos/transaction'));

export type CategoryInfo = {
    category: LeanDocument<CategoryDocument>;
    budgetEntry?: BudgetEntryDocument;
    transactions?: any[];
    plannedTransactions?: PlannedTransaction[];
    extendedPlannedTransactions?: PlannedTransaction[];
    plannedAmountPerDay?: number;
    plannedAmount?: number;
    plannedAdjustedAmount?: number;
    observedAmount?: number;
    transactionCount?: number;
};

// The sorting of these properties is important !
// Observed amount and count can be calculated from transactions, so processing transactions before the others has a
// performance impact.
enum BudgetModelProperties {
    transactions,
    observedAmount,
    plannedAmount,
    plannedAdjustedAmount,
    transactionCount,
}

class BudgetModel {
    public budget: BudgetDocument;
    public categories: LeanDocument<CategoryDocument>[];
    private readonly userId: string;
    private readonly nTree: NTree<CategoryInfo>;
    private readonly from: dayjs.Dayjs;
    private readonly to: dayjs.Dayjs;

    constructor(
        userId: string,
        budget: BudgetDocument,
        nTree: NTree<CategoryInfo>,
        categories: LeanDocument<CategoryDocument>[],
        from: dayjs.Dayjs,
        to: dayjs.Dayjs
    ) {
        this.userId = userId;
        this.budget = budget;
        this.nTree = nTree;
        this.categories = categories;
        this.from = from;
        this.to = to;
    }

    getRootNodes() {
        return this.nTree.getRootNodes();
    }

    getCategoryHierarchy<T>(propertySelector: (element: CategoryInfo) => T) {
        return this.nTree.getCategoryHierarchy(propertySelector);
    }

    findNode(searchFn: (node: TreeNode<CategoryInfo>) => boolean) {
        return this.nTree.findNode(searchFn);
    }

    iterate(predicate: (node: TreeNode<CategoryInfo>) => void) {
        return this.nTree.iterate(predicate);
    }

    getGlobalPlannedSumParDay() {
        let globalSum = 0;
        this.nTree.iterateConditional((node) => {
            const { budgetEntry, plannedAmountPerDay } = node.value;
            if (budgetEntry && !budgetEntry.isComplex) {
                globalSum += plannedAmountPerDay;
            } else {
                return true;
            }
        });
        return globalSum;
    }

    buildPlannedPlotData(granularity: string) {
        const daysInRange = this.to.diff(this.from, 'day', true);
        const { sumPerDay, plannedTransactions } = this.getPlannedData();
        return plotDataBuilder
            .create({
                from: this.from,
                to: this.to,
                interval: granularity,
            })
            .addTransactions(plannedTransactions)
            .addAmount(daysInRange * sumPerDay)
            .value();
    }

    async buildPlannedAdjustedPlotData(granularity: string) {
        const recurringOrIgnoredSubCategoryIds = (this.budget?.entries || [])
            .filter((entry) => entry.isComplex || entry.amount === 0) // FIXME: We have to exclude some categories with a
            //                                                                     "zero amount budget" because those categories
            //                                                                     had a complex budget last year but we know wont
            //                                                                     have any transactions this year. Fix this.
            .map((entry) => entry.categoryId);
        const nonRecurringNonTransferSubCategoryNames = this.categories
            .filter(
                (_cat) =>
                    _cat.type === CategoryType.base &&
                    _cat.systemType !== SystemCategory.virement &&
                    !recurringOrIgnoredSubCategoryIds.some((_id) => _id.equals(_cat._id))
            )
            .map((_cat) => _cat.name);
        const sumForAYear = await Transaction.sumAmountsForSubCategories(
            this.userId,
            nonRecurringNonTransferSubCategoryNames,
            this.from.year() >= dayjs().year()
                ? dayjs().subtract(1, 'year').toDate()
                : dayjs().year(this.from.year()).startOf('year').toDate(),
            this.from.year() >= dayjs().year()
                ? dayjs().toDate()
                : dayjs().year(this.from.year()).endOf('year').toDate()
        );
        const sumPerDay = sumForAYear / 365;

        const daysInRange = this.to.diff(this.from, 'day', true);
        const { plannedTransactions } = this.getPlannedData();
        return plotDataBuilder
            .create({
                from: this.from,
                to: this.to,
                interval: granularity,
            })
            .addTransactions(plannedTransactions)
            .addAmount(daysInRange * sumPerDay)
            .value();
    }

    private getPlannedData() {
        let sumPerDay = 0;
        const plannedTransactions: PlannedTransaction[] = [];
        this.nTree.iterateConditional((node) => {
            const { budgetEntry, extendedPlannedTransactions, plannedAmountPerDay } = node.value;
            if (extendedPlannedTransactions) {
                plannedTransactions.push(...extendedPlannedTransactions);
            } else if (budgetEntry && !budgetEntry.isComplex) {
                sumPerDay += plannedAmountPerDay;
            } else {
                return true;
            }
        });
        return {
            sumPerDay,
            plannedTransactions,
        };
    }
}

export type BudgetModelType = BudgetModel;

export type BudgetModelingParams = {
    userId: string;
    budgetYear: number;
    from?: dayjs.Dayjs;
    to?: dayjs.Dayjs;
    filter?: Record<string, any>;
    transactionIds?: string[];
};

export class BudgetModelingBuilder {
    static create(config: BudgetModelingParams) {
        return new BudgetModelingBuilder(config);
    }

    private readonly userId: string;
    private readonly budgetYear: number;
    private readonly originalFrom?: dayjs.Dayjs;
    private readonly from?: dayjs.Dayjs;
    private readonly originalTo?: dayjs.Dayjs;
    private readonly to?: dayjs.Dayjs;
    private readonly transactionFilters: {
        filter?: Record<string, any>;
        transactionIds?: string[];
    };
    private requiredProperties: BudgetModelProperties[] = [];
    private rootCategoryIds: string[];
    public budget: BudgetDocument;
    public categories: LeanDocument<CategoryDocument>[];
    private daysInRange: number;
    private transactionCountAggregate: { _id: string; count: number }[];
    private nTree: NTree<CategoryInfo>;

    private get propertyBuilderMapping() {
        return {
            [BudgetModelProperties.transactions]: () => this.attachTransactions(),
            [BudgetModelProperties.observedAmount]: (nodes: TreeNode<CategoryInfo>[]) =>
                this.calculateObservedAmount(nodes),
            [BudgetModelProperties.plannedAmount]: (nodes: TreeNode<CategoryInfo>[]) =>
                this.calculatePlannedAmount(nodes),
            [BudgetModelProperties.plannedAdjustedAmount]: (nodes: TreeNode<CategoryInfo>[]) =>
                this.calculatePlannedAdjustedAmount(nodes),
            [BudgetModelProperties.transactionCount]: (nodes: TreeNode<CategoryInfo>[]) =>
                this.calculateTransactionCount(nodes),
        };
    }

    private constructor(config: BudgetModelingParams) {
        const defaultFrom = dayjs().year(config.budgetYear).startOf('year');
        const defaultTo = dayjs().year(config.budgetYear).endOf('year');

        this.userId = config.userId;
        this.budgetYear = config.budgetYear;
        this.transactionFilters = {
            filter: config.filter,
            transactionIds: config.transactionIds,
        };

        this.originalFrom = config.from || defaultFrom;
        this.originalTo = config.to || defaultTo;

        // force the model to scope within the selected budget year
        this.from = config.from && config.from.isAfter(defaultFrom) ? config.from : defaultFrom;
        this.to = config.to && config.to.isBefore(defaultTo) ? config.to : defaultTo;
    }

    appendTransactions() {
        this.requiredProperties.push(BudgetModelProperties.transactions);
        return this;
    }

    appendObservedAmount() {
        this.requiredProperties.push(BudgetModelProperties.transactions);
        this.requiredProperties.push(BudgetModelProperties.observedAmount);
        return this;
    }

    appendPlannedAmount() {
        this.requiredProperties.push(BudgetModelProperties.plannedAmount);
        return this;
    }

    appendPlannedAdjustedAmount() {
        this.requiredProperties.push(BudgetModelProperties.plannedAmount);
        this.requiredProperties.push(BudgetModelProperties.observedAmount);
        this.requiredProperties.push(BudgetModelProperties.plannedAdjustedAmount);
        return this;
    }

    appendTransactionCount() {
        this.requiredProperties.push(BudgetModelProperties.transactionCount);
        return this;
    }

    trimToRoots(rootCategoryIds: string[]) {
        this.rootCategoryIds = rootCategoryIds;
        return this;
    }

    async build() {
        this.budget = await Budget.getClosest(this.userId, this.budgetYear, true);
        this.categories =
            (this.budget && (await getCategories(this.budget))) || (await Category.find().forUser(this.userId).lean());

        this.daysInRange = this.to.diff(this.from, 'day', true);
        this.nTree = new NTree(
            this.categories.map((category) => {
                return {
                    category,
                    budgetEntry: this.budget?.findEntryByCategory(category._id),
                };
            }),
            (c) => c.category.name,
            (c) => c.category.parent
        );

        if (this.rootCategoryIds) {
            this.trimAndRebuildTree();
        }

        for (let property of _.uniq(this.requiredProperties.sort())) {
            await this.propertyBuilderMapping[property](this.nTree.getRootNodes());
        }

        return new BudgetModel(
            this.userId,
            this.budget,
            this.nTree,
            this.categories,
            this.originalFrom,
            this.originalTo
        );
    }

    private trimAndRebuildTree() {
        const rootNodes: TreeNode<CategoryInfo>[] = [];
        this.nTree.iterateConditional((node) => {
            const { category } = node.value;
            if (this.rootCategoryIds.includes(category._id.toString())) {
                rootNodes.push(node);
            } else {
                return true;
            }
        });
        const nodeValues = _.chain(rootNodes)
            .forEach((node) => (node.value.category.parent = null))
            .map((node) => node.toArray())
            .flatten()
            .value();
        this.categories = nodeValues.map((info) => info.category);
        this.nTree = new NTree(
            nodeValues,
            (c) => c.category.name,
            (c) => c.category.parent
        );
    }

    private async attachTransactions() {
        const transactions = await searchTransactions({
            userId: this.userId,
            filter: _.extend(this.transactionFilters?.filter, {
                date_from: this.originalFrom.toDate(),
                date_to: this.to.toDate(),
            }),
            preSearchIds: this.transactionFilters?.transactionIds,
        });
        this.nTree.iterate((node) => {
            const { category } = node.value;
            if (category.type === CategoryType.base) {
                node.value.transactions = transactions.filter((t: any) => t.category === category.name);
            }
        });
        const uncategorizedTransactions = transactions.filter((t: any) => !t.category);
        const [revenues, spendings] = _.partition(uncategorizedTransactions, (t) => t.amount > 0);
        if (!_.isEmpty(revenues)) {
            this.nTree.add({
                transactions: revenues,
                category: new Category({
                    name: '(Revenues sans catégorie)',
                    type: CategoryType.base,
                    systemType: SystemCategory.revenu,
                    userId: this.userId,
                }),
            });
        }
        if (!_.isEmpty(spendings)) {
            this.nTree.add({
                transactions: spendings,
                category: new Category({
                    name: '(Dépenses sans catégorie)',
                    type: CategoryType.base,
                    systemType: null,
                    userId: this.userId,
                }),
            });
        }
    }

    private async calculateObservedAmount(nodes: TreeNode<CategoryInfo>[]) {
        for (let node of nodes) {
            const { transactions } = node.value;
            if (node.value.category.type === CategoryType.base) {
                if (_.isArray(transactions)) {
                    node.value.observedAmount = _.sumBy(transactions, 'amount');
                }
            } else {
                await this.calculateObservedAmount(node.children);
                node.value.observedAmount = _.sumBy(node.children, (node) => node.value.observedAmount);
            }
        }
    }

    private calculatePlannedAmount(nodes: TreeNode<CategoryInfo>[]) {
        this.nTree.iterateConditional((node) => {
            const { budgetEntry } = node.value;
            if (budgetEntry && !budgetEntry.isComplex) {
                const categories = node.toArray().map((node) => node.category);
                node.value.plannedAmountPerDay = budgetEntry.calculatePlannedBudgetSumPerDay(categories);
            } else {
                return true;
            }
        });
        this.nTree.iterate((node) => {
            const { budgetEntry, category } = node.value;
            if (budgetEntry?.isComplex && category) {
                node.value.plannedTransactions = budgetEntry
                    .getPlannedTransactions(this.from, this.to)
                    .map((transaction) => {
                        if (category.systemType !== SystemCategory.revenu) {
                            transaction.amount *= -1;
                        }
                        return transaction;
                    });
                if (this.originalTo.isAfter(this.to)) {
                    node.value.extendedPlannedTransactions = budgetEntry
                        .getPlannedTransactions(this.from, this.originalTo)
                        .map((transaction) => {
                            if (category.systemType !== SystemCategory.revenu) {
                                transaction.amount *= -1;
                            }
                            return transaction;
                        });
                } else {
                    node.value.extendedPlannedTransactions = node.value.plannedTransactions;
                }
            }
        });
        this.combinePlannedAmounts(nodes);
    }

    private combinePlannedAmounts(nodes: TreeNode<CategoryInfo>[]) {
        for (let node of nodes) {
            const { budgetEntry, plannedTransactions } = node.value;
            if (budgetEntry && !budgetEntry.isComplex) {
                node.value.plannedAmount = Math.abs(this.daysInRange * node.value.plannedAmountPerDay);
            } else if (plannedTransactions) {
                node.value.plannedAmount = Math.abs(_.sumBy(plannedTransactions, (transaction) => transaction.amount));
            } else {
                this.combinePlannedAmounts(node.children);
                node.value.plannedAmount = _.sumBy(node.children, (node) => node.value.plannedAmount);
            }
        }
    }

    private async calculatePlannedAdjustedAmount(nodes: TreeNode<CategoryInfo>[]) {
        const dateFrom =
            this.from.year() >= dayjs().year()
                ? dayjs().subtract(1, 'year').toDate()
                : dayjs().year(this.from.year()).startOf('year').toDate();
        const dateTo =
            this.from.year() >= dayjs().year()
                ? dayjs().toDate()
                : dayjs().year(this.from.year()).endOf('year').toDate();
        const searchConfig = {
            userId: this.userId,
            filter: {
                date_from: dateFrom.toISOString(),
                date_to: dateTo.toISOString(),
            },
        };
        const transactions = this.from.year() > dayjs().year() ? await searchTransactions(searchConfig) : [];
        this.sumUpPlannedAdjustedAmount(nodes, transactions);
    }

    private sumUpPlannedAdjustedAmount(nodes: TreeNode<CategoryInfo>[], transactions: any[]) {
        for (let node of nodes) {
            const { category, plannedTransactions, budgetEntry } = node.value;
            if (category.type === CategoryType.base) {
                const sumPerDay =
                    _.chain(transactions)
                        .filter((t) => t.category === category.name)
                        .sumBy((t) => t.amount)
                        .value() / 365;
                const plannedSum = _.chain(plannedTransactions)
                    // As to not sum the count transaction twice (planned + observed), we filter out the ones in the past.
                    .filter((t) => t.date >= Date.now())
                    .sumBy((t) => t.amount)
                    .value();

                if (this.from.year() < dayjs().year()) {
                    node.value.plannedAdjustedAmount = Math.abs(node.value.observedAmount);
                } else if (this.from.year() === dayjs().year()) {
                    const daysInRange = this.to.diff(dayjs(), 'day', true);
                    node.value.plannedAdjustedAmount =
                        budgetEntry && budgetEntry.isComplex
                            ? Math.abs(node.value.observedAmount) + Math.abs(plannedSum)
                            : Math.abs(node.value.observedAmount) + Math.abs(daysInRange * sumPerDay);
                } else {
                    node.value.plannedAdjustedAmount =
                        budgetEntry && budgetEntry.isComplex
                            ? Math.abs(plannedSum)
                            : Math.abs(this.daysInRange * sumPerDay);
                }
            } else {
                this.sumUpPlannedAdjustedAmount(node.children, transactions);
                node.value.plannedAdjustedAmount = _.sumBy(node.children, (node) => node.value.plannedAdjustedAmount);
            }
        }
    }

    private async calculateTransactionCount(nodes: TreeNode<CategoryInfo>[]) {
        for (let node of nodes) {
            const { category, transactions } = node.value;
            if (category.type === CategoryType.base) {
                node.value.transactionCount = _.isArray(transactions)
                    ? transactions.length
                    : (await this.getTransactionCountAggregate()).find((aggregate) => aggregate._id === category.name)
                          ?.count || 0;
            } else {
                await this.calculateTransactionCount(node.children);
                node.value.transactionCount = _.sumBy(node.children, (node) => node.value.transactionCount);
            }
        }
    }

    private async getTransactionCountAggregate() {
        if (!this.transactionCountAggregate) {
            this.transactionCountAggregate = await Transaction.countByCategories(
                this.userId,
                this.categories
                    .filter((category) => category.type === CategoryType.base)
                    .map((category) => category.name),
                this.originalFrom.toDate(),
                this.originalTo.toDate()
            );
        }
        return this.transactionCountAggregate;
    }
}
