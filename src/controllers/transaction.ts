import { CategoryType, SystemCategory } from '../models/enums';
import * as dayjs from 'dayjs';
import { FilterQuery, PipelineStage, LeanDocument } from 'mongoose';
import { CategoryDocument } from '../daos/category';
import { Dayjs } from 'dayjs';
import { Category } from '../daos/category';
import * as mongoose from 'mongoose';
import * as _ from 'lodash';
import { Budget } from '../daos/budget';
import { ClientError } from '../routes/utils/errors';
import { toTransactionDto } from '../models/entities';
import { getCategories } from './budget';
import { BudgetModelingBuilder } from './utils/BudgetModeling';
import { BudgetEntryDocument, PlannedTransaction } from '../daos/budgetEntry';
import { nanoid } from 'nanoid';
import { pairCoordinatesByNearestNeighbour } from '../modules/geometryUtil';
const Transaction: any = mongoose.model('transaction', require('../daos/transaction'));

export async function getLastActivity(
    userId: string,
    from: Dayjs,
    to: Dayjs,
    categories?: Array<LeanDocument<CategoryDocument>>
) {
    let baseCategories =
        categories || (await Category.find({ type: CategoryType.base }).forUser(userId).select('name').lean());
    let recentTransactions = await Transaction.find({
        category: {
            $exists: true,
            $nin: [null, ''],
        },
        date: {
            $gte: (from || dayjs().subtract(1, 'year')).toDate(),
            $lte: (to || dayjs()).toDate(),
        },
    })
        .forUser(userId)
        .sort('-date')
        .select('category date')
        .lean();

    let lastActivityMap: Record<string, string> = {};
    let visitedCategories: string[] = [];
    for (let recentTransaction of recentTransactions) {
        let _transaction = recentTransaction;
        if (!visitedCategories.includes(_transaction.category)) {
            visitedCategories.push(_transaction.category);
            let category = baseCategories.find((_cat) => _cat.name === _transaction.category);
            // Need to make sure the category still exists (we can hit a transaction of last year that was matched
            // to a category that was deleted for this year's budget)
            if (category) {
                lastActivityMap[category._id.toString()] = _transaction.date;
            }
        }
    }

    return lastActivityMap;
}

const allowedFields = [
    'description',
    'date',
    'amount',
    'currency',
    'conversionRate',
    'creationDate',
    'account',
    'category',
];
export async function searchTransactions({
    userId,
    size,
    skip,
    filter,
    sort,
    preSearchIds,
}: {
    userId: string;
    size?: number;
    skip?: number;
    filter?: Record<string, any>;
    sort?: Record<string, 1 | -1>;
    preSearchIds?: string[];
}) {
    let query: FilterQuery<any> = {
        userId,
        ...(await applyFilters(filter, userId)),
    };
    if (preSearchIds) {
        query._id = {
            $in: preSearchIds.map((id) => new mongoose.Types.ObjectId(id)),
        };
    }

    const sortQuery: Record<string, 1 | -1> = {};
    if (!_.isEmpty(sort)) {
        for (const [field, direction] of Object.entries(sort)) {
            if (allowedFields.includes(field)) {
                sortQuery[field] = direction;
            }
        }
    }
    sortQuery['_id'] = 1;

    const pipeline: PipelineStage[] = [
        {
            $match: query,
        },
        {
            $sort: sortQuery,
        },
    ];
    if (!preSearchIds && size) {
        pipeline.push(
            {
                $skip: skip || 0,
            },
            {
                $limit: size,
            }
        );
    }

    return await Transaction.aggregate(pipeline);
}

async function applyFilters(filter: Record<string, any>, userId: string) {
    const query: FilterQuery<any> = {};
    if (_.isEmpty(filter)) {
        return query;
    }

    const applyFilterMapping: Record<string, (field: string, value: any) => Promise<void>> = {
        statementId: async (field, value) => {
            query[field] = new mongoose.Types.ObjectId(value);
        },

        categoryId: async (field, value) => {
            const budget = await Budget.getClosest(
                userId,
                'date_from' in filter ? dayjs(filter['date_from']).year() : dayjs().year()
            );
            const categories = await getCategories(budget);
            const category = categories.find((c) => c._id.equals(value));
            if (category) {
                const baseCategories = await Category.findChildBaseCategories(userId, category.name, categories);
                query['category'] = {
                    $in: baseCategories.map((c) => c.name),
                };
            } else {
                // The category doesn't exist, `$in: []` essentially means that the filter will never match anything.
                query['category'] = { $in: [] };
            }
        },

        non_transfer: async (field, value) => {
            const excludedBaseCategories = await Category.find({
                type: CategoryType.base,
                systemType: SystemCategory.virement,
            })
                .forUser(userId)
                .select('name')
                .lean();
            query['category'] = {
                $nin: excludedBaseCategories.map((c) => c.name),
            };
        },

        no_category: async () => {
            query['category'] = { $in: [null, ''] };
            query['isPending'] = { $ne: true };
        },

        date_from: async (field, value) => {
            query['date'] = {
                ...query['date'],
                $gte: new Date(value),
            };
        },

        date_to: async (field, value) => {
            query['date'] = {
                ...query['date'],
                $lt: new Date(value),
            };
        },

        date: async (field, value) => {
            query[field] = new Date(value);
        },

        recurring: async (field, value) => {
            query['recurring'] = !!value ? true : { $ne: true };
        },
    };

    for (const [field, value] of Object.entries(filter)) {
        if (!!applyFilterMapping[field]) {
            await applyFilterMapping[field](field, value);
        } else if (allowedFields.includes(field)) {
            query[field] = _.isArray(value) ? { $in: value } : value;
        }
    }
    return query;
}

export async function appendOptionalFields(userId: string, fields: string[], transactions: any[]) {
    const additionalFields = new Map();
    for (let field of fields) {
        if (field === 'categoryHierarchy') {
            // group transactions by year and get the correct categories for the year's budget
            const groups: Record<string, any[]> = _.groupBy(transactions, (_t: any) => _t.date.getFullYear());
            for (let [year, transactionsGroup] of Object.entries(groups)) {
                const budgetModel = await BudgetModelingBuilder.create({
                    userId,
                    budgetYear: parseInt(year),
                }).build();
                const hierarchyMap = budgetModel.getCategoryHierarchy((categoryInfo) => ({
                    _id: categoryInfo.category._id,
                    name: categoryInfo.category.name,
                    hue: categoryInfo.category.hue,
                    systemType: categoryInfo.category.systemType,
                    type: categoryInfo.category.type,
                }));

                for (let transaction of transactionsGroup) {
                    const id = transaction._id.toString();
                    let values = {
                        ...additionalFields.get(id),
                        [field]: hierarchyMap.get(transaction.category),
                    };
                    additionalFields.set(id, values);
                }
            }
        } else {
            throw new ClientError(`Le champs '${field}' n'est pas valide.`);
        }
    }

    return transactions.map((_transaction) =>
        toTransactionDto({
            ..._transaction,
            ...additionalFields.get(_transaction._id.toString()),
        })
    );
}

export async function assignCategories(userId: string, transactions: any[]) {
    const distinctDescriptions = _.chain(transactions)
        .map((transaction) => transaction.description)
        .uniq()
        .value();
    for (let description of distinctDescriptions) {
        const referenceTransaction = await Transaction.findOne({
            ignoreForAutoCategoryAssignment: { $ne: true },
            description,
        })
            .sort('-date')
            .forUser(userId)
            .select('category')
            .lean();
        if (referenceTransaction) {
            transactions
                .filter((t) => t.description === description)
                .forEach((t) => (t.category = referenceTransaction.category));
        }
    }

    // TODO: some day, this es auto-assignation code should get fixed.
    // let results = await esDao.assignRelevantCategories(userId, distinctDescriptions, fileType);
    //
    // let categories = await this.model('category').find().forUser(userId).select('name').lean();
    //
    // // assign category to transactions
    // for (let transaction of transactions) {
    //     let categoryMatches = results.filter(
    //         (_result) => _result.type === transaction.type && _result.description === transaction.description
    //     );
    //
    //     let category =
    //         !_.isEmpty(categoryMatches) &&
    //         categories.find((_cat) => _cat.name.toLowerCase() === _.first(categoryMatches).category);
    //     if (
    //         category &&
    //         (_.first(categoryMatches).score >= importConfig.category_es_score_threshold ||
    //             categoryMatches.length === 1)
    //     ) {
    //         transaction.category = category.name;
    //     }
    // }

    return transactions;
}

export function createPlannedTransactionMatcher(
    transactions?: any[],
    plannedTransactions?: Array<PlannedTransaction>,
    budgetEntry?: BudgetEntryDocument
) {
    const identifiedTransactions =
        transactions?.map((t) => ({
            ...t,
            __comparingId: t._id?.toString() || nanoid(),
        })) || [];
    const identifiedPlannedTransactions =
        plannedTransactions?.map((t) => ({
            ...t,
            __comparingId: nanoid(),
        })) || [];

    return {
        pair: () => pairPlannedTransactions(identifiedTransactions, identifiedPlannedTransactions, budgetEntry),
        transactions: {
            filterIsPlanned: () => {
                const plannedTransactionMapping = pairPlannedTransactions(
                    identifiedTransactions,
                    identifiedPlannedTransactions,
                    budgetEntry
                );
                return identifiedTransactions
                    .filter((t) => plannedTransactionMapping.get(t.__comparingId))
                    .map((t) => {
                        delete t.__comparingId;
                        return t;
                    });
            },
        },
        plannedTransactions: {
            filterIsPending: () => {
                const plannedTransactionMapping = pairPlannedTransactions(
                    identifiedTransactions,
                    identifiedPlannedTransactions,
                    budgetEntry
                );
                const matchedPlannedTransactionIds = [...plannedTransactionMapping.values()]
                    .filter(Boolean)
                    .map((t) => t.__comparingId);
                return identifiedPlannedTransactions
                    .filter((t) => !matchedPlannedTransactionIds.includes(t.__comparingId))
                    .map((t) => {
                        delete t.__comparingId;
                        return t;
                    });
            },
        },
    };
}

function pairPlannedTransactions(
    transactions: any[],
    plannedTransactions: Array<PlannedTransaction & { __comparingId: string }>,
    budgetEntry?: BudgetEntryDocument
) {
    const maximumSloppinessInDays = budgetEntry?.getMaximumSloppinessInDays() || 0;

    // zip planned transactions with the observed ones
    // Timestamps is on a scale of milliseconds. The smallest useful resolution is a day (86400000 ms)
    // One day has the same distance as 1000$
    const timestampFactor = 1 / 86400;
    const amountFactor = 1;
    const formatValue = (value: number, factor: number) => Math.round(value * factor);
    const coordinateMapping = pairCoordinatesByNearestNeighbour(
        transactions.map((t) => ({
            id: t.__comparingId,
            x: formatValue(new Date(t.date).getTime(), timestampFactor),
            y: formatValue(t.amount, amountFactor),
        })),
        plannedTransactions.map((t) => ({
            id: t.__comparingId,
            x: formatValue(t.date, timestampFactor),
            y: formatValue(t.amount, amountFactor),
        })),
        { distanceThreshold: maximumSloppinessInDays * 1000 }
    );

    const plannedTransactionMapping = new Map<string, (PlannedTransaction & { __comparingId: string }) | null>();
    for (const [transactionId, plannedId] of coordinateMapping) {
        plannedTransactionMapping.set(
            transactionId,
            plannedTransactions.find((t) => t.__comparingId === plannedId)
        );
    }
    return plannedTransactionMapping;
}
