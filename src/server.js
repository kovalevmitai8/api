/**
 * Created by hugo on 2018-05-13.
 */
'use strict';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Packages
///
const path = require('path');
const fs = require('fs');
const dayjs = require('dayjs');

const worker = require('./worker');
const daemon = require('./daemon');
const es = require('./daos/elasticsearch/elasticsearch');
require('./daos/database');
const conn = require('mongoose').connection;
const SystemDataManager = require('./daos/migration/SystemDataManager');
const logger = require('./modules/simpleLogger');

const REINDEX_ON_STARTUP = process.env.NODE_ENV === 'PROD';

process
    .on('unhandledRejection', (reason, p) => {
        logger.critical(`unhandledRejection: ${reason}`);
        reason.stack && logger.critical(reason.stack);
    })
    .on('uncaughtException', (err) => {
        logger.critical(
            `Uncaught Exception thrown: ${JSON.stringify({ message: err.message, stack: err.stack }, null, 4)}`
        );
    });

class server {
    static async start() {
        const fsCompatibleDate = dayjs().format('YYYY-MM-DD[_]HH.mm.ss.SSS[_]ZZ');

        if (process.env.NODE_ENV === 'PROD') {
            const access = fs.createWriteStream(path.join(__dirname, '..', 'logs', `${fsCompatibleDate}__system.txt`), {
                flags: 'a',
            });

            process.stdout.write = process.stderr.write = access.write.bind(access);

            process.on('uncaughtException', function (err) {
                console.error(err && err.stack ? err.stack : err);
            });
        }

        await this._connectToDb();
        await this._startMaster();

        await worker.start();

        if (process.env.NODE_ENV === 'PROD') {
            await daemon.start();
        }

        // Server reboot 💣💥💣

        if (process.env.NODE_ENV === 'DEV') {
            require('../../client/server');
        }
    }

    static _connectToDb() {
        return new Promise((resolve, reject) => {
            logger.log(`Connecting to database..`);
            conn.on('error', (err) => reject(err));
            conn.once('open', () => resolve());
        });
    }

    static async _startMaster() {
        // initialize elasticsearch indices
        logger.log(`Initializing Elasticsearch indices..`);
        await es.waitForConnection();
        let doesExists = await es.exists();
        if (doesExists === false) {
            await es.initialize();
        }

        if (process.env.NODE_ENV === 'CI') {
            logger.log(`Skipping data migration`);
        } else {
            logger.log(`Initializing system data..`);
            await SystemDataManager.initData();
            let success = await SystemDataManager.migrateData();
            if (success === false) {
                logger.log('Migration error, admin was notified. Worker will stop now.');
                process.exit(-1);
                return;
            }
        }

        // stuff to execute on startup
        if (REINDEX_ON_STARTUP) {
            await es.reindex();
        }
    }
}

server.start().catch((err) => logger.error(err));
