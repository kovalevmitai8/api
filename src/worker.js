const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const morgan = require('morgan');
const fs = require('fs');
const compression = require('compression');
const https = require('https');
const dayjs = require('dayjs');
const graylog2 = require('graylog2');
const rateLimit = require('express-rate-limit');

const errorHandler = require('./routes/utils/errorHandler');
const logger = require('./modules/simpleLogger');

const securityCtrl = require('./routes/security');

const config = require('../mainConfig.json');
const { deviceAuthorisationMiddleware } = require('./routes/security');

class worker {
    static async start() {
        let app = express();
        const httpPort = process.env.NODE_ENV === 'DEV' ? 9091 : 80;
        const httpsPort = process.env.NODE_ENV === 'DEV' ? 9092 : 443;

        this._initLogger(app);

        app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
        app.use(bodyParser.json({ limit: '5mb' }));

        // directory where files are stored temporarily during upload/download
        app.use(express.static(path.join(__dirname, 'files')));
        app.use('/email_res', express.static(path.join(__dirname, 'email_res')));

        app.use(require('express-useragent').express());

        const authRateLimiter = rateLimit({
            windowMs: 5_000,
            max: 1,
            standardHeaders: true,
        });
        const apiRateLimiter = rateLimit({
            windowMs: 60_000,
            max: 150,
        });

        // public route to get a web token
        if (process.env.NODE_ENV === 'PROD') {
            app.use('/api/authenticate', authRateLimiter);
            app.use('/api/service_authenticate', authRateLimiter);
            app.use('/authorize', authRateLimiter);
        }
        app.post('/api/authenticate', ...securityCtrl.getAuthenticate());
        app.post('/api/service_authenticate', ...securityCtrl.getServiceAuthenticate());
        app.get('/authorize', ...deviceAuthorisationMiddleware());
        // used by the clients to test if the server is reachable
        app.get('/api/ping', (req, res) => res.status(200).end());
        // used to get a fresh token.
        app.post('/api/token/refresh', securityCtrl.refreshToken);

        // compress all routes returning json
        app.use(compression());
        if (process.env.NODE_ENV === 'PROD') {
            app.use(apiRateLimiter);
        }

        app.use('/api/timeline', require('./routes/timeline').default());
        app.use('/api/transaction', require('./routes/transaction').getRouter());
        app.use('/api/statement', require('./routes/statement').getRouter());
        app.use('/api/category', require('./routes/category').getRouter());
        app.use('/api/account', require('./routes/account').getRouter());
        app.use('/api/charts', require('./routes/charts').getRouter());
        app.use('/api/budget', require('./routes/budget').getRouter());
        app.use('/api/goal', require('./routes/goal').default());
        app.use('/api/user', require('./routes/user').getRouter());
        app.use('/api/attachment', require('./routes/attachment').getRouter());
        app.use('/api/extract', require('./routes/extract').getRouter());

        app.get('/favicon.ico', (req, res) => res.send());

        // 404 request handler
        app.use(errorHandler.handle404);

        // available on http in DEV only
        if (process.env.NODE_ENV === 'DEV' || process.env.NODE_ENV === 'CI') {
            const _port = process.env.CI_PORT || httpPort;
            await app.listen(_port);
            logger.log(`http server started on port ${_port}`);
        }

        try {
            let credentials = {
                key: fs.readFileSync(process.env.HTTPS_PRIVK_PATH, 'utf8'),
                cert: fs.readFileSync(process.env.HTTPS_CERT_PATH, 'utf8'),
            };
            let httpsServer = https.createServer(credentials, app);
            await httpsServer.listen(httpsPort);
            logger.log(`https server started on port ${httpsPort}`);
        } catch (err) {
            logger.error('Could not load the https credentials.');
        }
    }

    static _initLogger(app) {
        const dateFormat = {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit',
            timeZoneName: 'short',
        };

        morgan.token('local-date', function (req) {
            return new Date().toLocaleString('fr-CA', dateFormat);
        });

        // log request in a seperate file
        const fsCompatibleDate = dayjs().format('YYYY-MM-DD[_]HH.mm.ss.SSS[_]ZZ');
        let accessLogStream = fs.createWriteStream(path.join(__dirname, '..', 'logs', `${fsCompatibleDate}__api.txt`), {
            flags: 'a',
        });
        app.use(
            morgan(':local-date :method :status :url :response-time ms - :res[content-length]', {
                stream: accessLogStream,
            })
        );

        if (process.env.NODE_ENV === 'PROD') {
            const graylogLogger = new graylog2.graylog({
                servers: [{ host: config.graylog_host, port: config.graylog_port }],
                hostname: 'api.finances-app.com',
                facility: 'Node.js',
            });
            graylogLogger.on('error', function (err) {
                console.error('[Expressjs logger] Error while trying to write to graylog2:', err);
            });
            // TODO: gracefuly terminate connection to the greylog server with "logger.close(() => {})"
            app.use(
                morgan('combined', {
                    stream: { write: (message) => graylogLogger.log(message, { type: 'express' }) },
                })
            );
        }
    }
}

module.exports = worker;
