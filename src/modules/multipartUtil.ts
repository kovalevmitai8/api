import * as multiparty from 'multiparty';
import { Part } from 'multiparty';
import { Request } from 'express';

/**
 * Retrieve the file stream from the request
 */
export async function getStream(request: Request): Promise<{ stream?: Part; filename?: string }> {
    return new Promise((resolve, reject) => {
        const form = new multiparty.Form();
        let fileFound = false;
        const result: { stream: Part; filename: string } = {
            stream: null,
            filename: null,
        };

        form.on('error', (err) => reject(err));
        form.on('part', (part) => {
            if (part.filename) {
                fileFound = true;
                result.stream = part;
                result.filename = part.filename;
                resolve(result);
            }
        });
        form.on('close', () => {
            if (!fileFound) {
                resolve(result);
            }
        });

        form.parse(request);
    });
}

/**
 * Extract fields and file streams data from a multipart form.
 */
export async function parseMultiPart(
    request: Request,
    fieldNames: string[],
    isFile: boolean
): Promise<{ fields?: Map<string, any>; stream?: Part; filename?: string }> {
    return new Promise((resolve, reject) => {
        const form = new multiparty.Form();
        const fields = new Map();
        let file: Part = null;

        form.on('error', (err) => reject(err));
        form.on('field', (name, value) => {
            if (fieldNames.includes(name)) {
                fields.set(name, value);
                if ((!isFile || file) && fields.size === fieldNames.length) {
                    if (file) {
                        resolve({ fields, stream: file, filename: file.filename });
                    } else {
                        resolve({ fields });
                    }
                }
            }
        });
        form.on('part', (part) => {
            if (part.filename) {
                file = part;
                if ((!isFile || file) && fields.size === fieldNames.length) {
                    if (file) {
                        resolve({ fields, stream: file, filename: file.filename });
                    } else {
                        resolve({ fields });
                    }
                }
            }
        });
        form.on('close', () => {
            if (!((!isFile || file) && fields.size === fieldNames.length)) {
                resolve({});
            }
        });

        form.parse(request);
    });
}
