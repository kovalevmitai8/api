const mongoose = require('mongoose');

const { CategoryType, SystemCategory } = require('../models/enums');

const Transaction = mongoose.model('transaction', require('../daos/transaction'));
const { Category } = require('../daos/category');

class cacheUtil {
    static async getTransactionExpirationEvents(transactions) {
        const cacheExpirationEvents = [];

        let excludedCategories = await Category.find({
            type: CategoryType.base,
            systemType: SystemCategory.virement,
        })
            .forUser(transactions[0].userId)
            .select('name')
            .lean();
        let excludedCategoryNames = excludedCategories.map((c) => c.name);
        let nonTransferTransactionCount = await Transaction.countDocuments({
            statementId: transactions[0].statementId.toString(),
            category: {
                $nin: excludedCategoryNames,
            },
        });
        if (nonTransferTransactionCount > 0) {
            cacheExpirationEvents.push('NON_TRANSFER_TRANSACTIONS_IMPORTED');
        } else if (transactions.length > 0) {
            cacheExpirationEvents.push('TRANSACTIONS_IMPORTED');
        }

        return cacheExpirationEvents;
    }
}

module.exports = cacheUtil;
