import { pbkdf2Sync, randomBytes } from 'crypto';
import * as fs from 'fs';
import * as jwt from 'jsonwebtoken';

const env = process.env.NODE_ENV;

export function generateClearTextApiKey() {
    const rawKey = randomBytes(32).toString('base64');
    return `${rawKey.slice(0, 4)}.${rawKey.slice(4)}`;
}

export function hashApiKey(apiKey: string) {
    const [prefix, key] = apiKey.split('.');
    const hashedKey = pbkdf2Sync(key, 'no_salt', 1000, 40, 'sha512').toString('base64');
    return `${prefix}.${hashedKey}`;
}

export function tryHashApiKey(apiKey: string) {
    try {
        const [prefix, key] = apiKey.split('.');
        const hashedKey = pbkdf2Sync(key, 'no_salt', 1000, 40, 'sha512').toString('base64');
        return `${prefix}.${hashedKey}`;
    } catch (err) {
        return null;
    }
}

const refresh_private_key = fs.readFileSync(
    env === 'PROD' ? './certificate/refresh_private.key' : './certificate/TEST_private.key',
    'utf-8'
);
const refresh_public_key = fs.readFileSync(
    env === 'PROD' ? './certificate/refresh_public.key' : './certificate/TEST_public.key',
    'utf-8'
);

export function sign(payload: any, expiresIn: string) {
    return jwt.sign(payload, refresh_private_key, { algorithm: 'RS256', expiresIn: expiresIn });
}

export function verify<TPayload>(token: string) {
    try {
        return jwt.verify(token, refresh_public_key) as TPayload;
    } catch (err) {
        return false;
    }
}

export function verifyErr(token: string) {
    try {
        jwt.verify(token, refresh_public_key);
    } catch (err) {
        return err;
    }
}
