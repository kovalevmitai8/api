const mongoose = require('mongoose');
const _ = require('lodash');

const importHelper = require('../import/importHelper');
const Transaction = mongoose.model('transaction', require('../daos/transaction'));

class transactionService {
    /**
     * Merge the local and remote transaction list into a diff collection.
     * @param {Transaction[]} newTransactions
     * @param {Transaction[]} existingTransactions
     * @returns {{ equal?: Transaction[], removed?: Transaction[], added?: Transaction[] }[]}
     */
    static processDiff(newTransactions, existingTransactions) {
        const diffs = [];

        this.iterateDiffs(newTransactions, existingTransactions, (added, removed) => {
            if (added && removed) {
                if (diffs.length && _.last(diffs).equal) {
                    _.last(diffs).equal.push(removed);
                } else {
                    diffs.push({ equal: [removed] });
                }
            } else if (added) {
                if (diffs.length && _.last(diffs).added) {
                    _.last(diffs).added.push(added);
                } else {
                    diffs.push({ added: [added] });
                }
            } else if (removed) {
                if (diffs.length && _.last(diffs).removed) {
                    _.last(diffs).removed.push(removed);
                } else {
                    diffs.push({ removed: [removed] });
                }
            }
        });

        return diffs;
    }

    /**
     * iterate over each differences or pair of matching items.
     * @param newTransactions
     * @param existingTransactions
     * @param predicate
     */
    static iterateDiffs(newTransactions, existingTransactions, predicate) {
        // diffs should contain all transactions sorted by date and grouped by type of diff
        const local = this._prepareTransactions(newTransactions);
        const remote = this._prepareTransactions(existingTransactions);

        while (local.length || remote.length) {
            // mark removed if local is empty or if the first transaction of remote is not present in local
            const isRemoved =
                _.isEmpty(local) ||
                (remote.length && local.every((_t) => !this._areTransactionSameIsh(_t, _.first(remote), 1)));
            // mark added if remote is empty or if the first transaction of local is not present in remote
            const isAdded =
                _.isEmpty(remote) ||
                (local.length && remote.every((_t) => !this._areTransactionSameIsh(_t, _.first(local), 1)));

            if (isRemoved) {
                predicate(null, remote.shift());
            } else if (isAdded) {
                predicate(local.shift(), null);
            } else {
                if (_.first(remote).date.getTime() <= _.first(local).date.getTime()) {
                    let transaction = remote.shift();
                    let spliced = local.splice(
                        local.findIndex((_t) => this._areTransactionSameIsh(_t, transaction, 1)),
                        1
                    );
                    predicate(spliced[0], transaction);
                } else {
                    let transaction = local.shift();
                    let spliced = remote.splice(
                        remote.findIndex((_t) => this._areTransactionSameIsh(_t, transaction, 1)),
                        1
                    );
                    predicate(transaction, spliced[0]);
                }
            }
        }
    }

    /**
     * Convert dates to Date objects and sort by date.
     * @param {Transaction[]} transactions
     * @returns {Transaction[]}
     * @private
     */
    static _prepareTransactions(transactions) {
        return _.chain(transactions)
            .map((_t) => {
                _t.date = new Date(_t.date);
                return _t;
            })
            .sortBy((_t) => importHelper.removeDiacritics(_t.description))
            .sortBy((_t) => _t.date.getTime())
            .value();
    }

    /**
     * Compare transactions, allowing N minor property to be different
     * @param a
     * @param b
     * @param allowedDifferences
     * @private
     */
    static _areTransactionSameIsh(a, b, allowedDifferences = 0) {
        return this._compareTransactions(a, b) <= allowedDifferences;
    }

    /**
     * Compare transactions by date, amount, type and account.
     * Will return 999 if one of the items is null or if they are not comparable.
     * @param {Transaction} a
     * @param {Transaction} b
     * @returns {number} Number of differences.
     * @private
     */
    static _compareTransactions(a, b) {
        if (!a || !b) {
            return 999;
        } else if (a.date.getTime() !== b.date.getTime()) {
            return 999;
        } else if (a.amount !== b.amount) {
            return 999;
        }

        let count = 0;
        if ((a.description || '') !== (b.description || '')) {
            count++;
        }
        if (a.account !== b.account) {
            count++;
        }
        return count;
    }

    static findUniqAccounts(transactions) {
        return _.chain(transactions)
            .map((_t) => _t.account)
            .uniq()
            .filter(Boolean)
            .value();
    }

    /**
     * Find transactions in the database matching the date range and accounts.
     * @param {Transaction[]} transactions
     * @param {string} userId
     * @returns {Promise<Transaction[]>}
     */
    static async findExistingTransactions(transactions, userId) {
        const accounts = this.findUniqAccounts(transactions);

        const transactionDates = _.chain(transactions)
            .map((_t) => new Date(_t.date))
            .sortBy((_date) => _date.getTime())
            .value();
        return Transaction.find({
            account: {
                $in: accounts,
            },
            date: {
                $gte: _.first(transactionDates),
                $lte: _.last(transactionDates),
            },
        })
            .forUser(userId)
            .lean();
    }
}
module.exports = transactionService;
