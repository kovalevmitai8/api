import * as _cluster from 'cluster';
const cluster = (_cluster as unknown) as _cluster.Cluster; // typings fix
import * as graylog2 from 'graylog2';
const config = require('../../mainConfig.json');

const dateFormat = {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    timeZoneName: 'short',
};

let logger: graylog2.graylog = null;
if (process.env.NODE_ENV === 'PROD') {
    logger = new graylog2.graylog({
        servers: [{ host: config.graylog_host, port: config.graylog_port }],
        hostname: 'api.finances-app.com',
        facility: 'Node.js',
    });
    logger.on('error', function (err) {
        console.error(prependSystemInfo('[System logger] Error while trying to write to graylog2:'));
        console.error(prependSystemInfo(err));
    });
}
// TODO: gracefuly terminate connection to the greylog server with "logger.close(() => {})"

export function log(message: any) {
    console.log(prependSystemInfo(message));
    logger?.log(formatMessage(message), { type: 'system' });
}

export function debug(message: any) {
    console.debug(prependSystemInfo(message));
    logger?.debug(formatMessage(message), { type: 'system' });
}

export function info(message: any) {
    console.info(prependSystemInfo(message));
    logger?.info(formatMessage(message), { type: 'system' });
}

export function notice(message: any) {
    console.warn(prependSystemInfo(message));
    logger?.notice(formatMessage(message), { type: 'system' });
}

export function warning(message: any) {
    console.warn(prependSystemInfo(message));
    logger?.warning(formatMessage(message), { type: 'system' });
}

export function error(message: any) {
    console.error(prependSystemInfo(message));
    logger?.error(formatMessage(message), { type: 'system' });
}

export function critical(message: any) {
    console.error(prependSystemInfo(message));
    logger?.critical(formatMessage(message), { type: 'system' });
}

export function alert(message: any) {
    console.error(prependSystemInfo(message));
    logger?.alert(formatMessage(message), { type: 'system' });
}

export function emergency(message: any) {
    console.error(prependSystemInfo(message));
    logger?.emergency(formatMessage(message), { type: 'system' });
}

function formatMessage(message: any) {
    if (message instanceof Error) {
        return JSON.stringify(
            {
                message: message.message,
                stack: message.stack,
            },
            null,
            4
        );
    } else if (typeof message === 'object') {
        return JSON.stringify(message, null, 4);
    }
    return message;
}

function prependSystemInfo(message: any) {
    let workerName;
    if (cluster.isPrimary) {
        workerName = '[master]';
    } else {
        let paddedId = `#${cluster.worker.id}`;
        workerName = cluster.isPrimary ? '[master]' : `[worker ${paddedId.padStart(3, ' ')}]`;
    }
    // @ts-ignore
    let timestamp = new Date().toLocaleString('fr-CA', dateFormat);
    return workerName + ' ' + timestamp + ' ' + message;
}
