type Coordinate = {
    x: number;
    y: number;
    id: string;
};

type NearestNeighbourParams = {
    distanceThreshold: number;
};

export function pairCoordinatesByNearestNeighbour(
    referenceCoordinates: Coordinate[],
    matchingCoordinates: Coordinate[],
    params?: NearestNeighbourParams
): Map<string, string> {
    const finalMapping = new Map();

    const distanceMatrix: Record<string, Record<string, number>> = {};
    for (let refCoordinate of referenceCoordinates) {
        distanceMatrix[refCoordinate.id] = {};
        for (let matchCoordinate of matchingCoordinates) {
            // fast euclidean distance (https://en.wikibooks.org/wiki/Algorithms/Distance_approximations)
            // calculated without square root, used for comparison only (distance value is meaningless here)
            distanceMatrix[refCoordinate.id][matchCoordinate.id] =
                Math.pow(matchCoordinate.x - refCoordinate.x, 2) + Math.pow(matchCoordinate.y - refCoordinate.y, 2);
        }
    }

    const calculatedThreshold = params?.distanceThreshold
        ? Math.pow(params!.distanceThreshold, 2)
        : Number.MAX_SAFE_INTEGER;

    let shortestDistance = null;
    do {
        shortestDistance = null;
        let shortestRefId = null;
        let shortestMatchId = null;
        for (let [refId, row] of Object.entries(distanceMatrix)) {
            for (let [matchId, distance] of Object.entries(row)) {
                if ((shortestDistance === null || shortestDistance > distance) && distance < calculatedThreshold) {
                    shortestDistance = distance;
                    shortestRefId = refId;
                    shortestMatchId = matchId;
                }
            }
        }
        if (shortestDistance !== null) {
            finalMapping.set(shortestRefId, shortestMatchId);
            for (let [refId, row] of Object.entries(distanceMatrix)) {
                delete row[shortestMatchId];
            }
            delete distanceMatrix[shortestRefId];
        }
    } while (shortestDistance !== null);

    return finalMapping;
}
