const mongoose = require('mongoose');
const Cron = require('cron').CronJob;
const { fork } = require('child_process');
const geoip = require('geoip-lite');
const fs = require('fs');
const dayjs = require('dayjs');

const es = require('./daos/elasticsearch/elasticsearch');
const logger = require('./modules/simpleLogger');
const { BudgetModelingBuilder } = require('./controllers/utils/BudgetModeling');

let secrets = {};
if (process.env.API_CONFIG_PATH) {
    const secretData = fs.readFileSync(process.env.API_CONFIG_PATH);
    secrets = JSON.parse(secretData);
}

class daemon {
    static async start() {
        // every day @ 5am
        let reindexJob = new Cron('0 5 * * *', () => es.reindex());
        reindexJob.start();

        // every sunday @ 3am
        let backupJob = new Cron('0 3 * * 0', () => this._backupDatabase());
        backupJob.start();

        if (secrets.maxmind_key) {
            // every sunday @ 4am
            let geoipUpdateJob = new Cron('0 4 * * 0', () => this._updateGeoIpData());
            geoipUpdateJob.start();
        } else {
            logger.warning('Missing "maxmind_key". GeoIP update job disabled.');
        }

        // first day of each month
        let setMonthlyBudgetPlannedDataJob = new Cron('0 0 1 * *', () => this._saveMonthlyBudgetPlannedPlotData());
        setMonthlyBudgetPlannedDataJob.start();
    }

    static _backupDatabase() {
        this._forkScript('./scripts/backup.js', null, (exitCode, signal) => {
            if (exitCode && exitCode !== 0) {
                // log the error
                logger.error('backup script exited with code (stderr output above): ' + exitCode);
                return;
            } else if (signal) {
                logger.error('backup script was terminated by signal (stderr output above): ' + signal);
                return;
            }
            logger.log('Backup script executed successfully !');
        });
    }

    static _updateGeoIpData() {
        this._forkScript(
            './node_modules/geoip-lite/scripts/updatedb.js',
            [`license_key=${secrets.maxmind_key}`],
            (exitCode, signal) => {
                if (exitCode && exitCode !== 0) {
                    // log the error
                    logger.error('geoip updatedb process exited with code: ' + exitCode);
                    return;
                } else if (signal) {
                    logger.error('geoip updatedb process was terminated by signal: ' + signal);
                    return;
                }
                geoip.reloadData(() => logger.log('geoip-lite database reloaded successfully'));
            }
        );
    }

    static _forkScript(scriptPath, args, cb) {
        const scriptName = scriptPath.split('/').pop();
        let backupProcess = fork(scriptPath, args, { stdio: 'pipe' });

        backupProcess.stdout.on('data', (data) => logger.debug(`[${scriptName}] ${data}`));
        backupProcess.stderr.on('data', (data) => logger.error(`[${scriptName}] ${data}`));
        backupProcess.on('exit', cb);
    }

    static async _saveMonthlyBudgetPlannedPlotData() {
        const { Budget } = require('./daos/budget');

        const currentBudgets = await Budget.find({ year: dayjs().year() });
        for (let budget of currentBudgets) {
            if (budget.plannedBudgetSumHistory.find((_entry) => _entry.month === dayjs().month())) {
                continue;
            }

            const from = dayjs().year(budget.year).startOf('year');
            const to = from.clone().endOf('year');
            const daysInRange = to.diff(from, 'day', true);
            const budgetModel = await BudgetModelingBuilder.create({
                userId: budget.userId,
                budgetYear: budget.year,
                from,
                to,
            })
                .appendPlannedAmount()
                .build();

            budget.plannedBudgetSumHistory.push({
                month: dayjs().month(),
                value: daysInRange * budgetModel.getGlobalPlannedSumParDay(),
            });

            budget.markModified('plannedBudgetSumHistory');
            await budget.save();
        }
    }
}

module.exports = daemon;
