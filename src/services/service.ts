import * as superagent from 'superagent';
import { ResponseError } from 'superagent';
import * as stream from 'stream';

class baseService {
    static handleError(err: ResponseError) {
        if (err.response && err.response.status >= 400) {
            let message = (err.response.body && err.response.body.message) || err.response.text;
            throw new Error(`[${err.response.status}] ${message}`);
        }
        throw err;
    }
}

type ErrorEmailConfig = {
    from: string;
    to: string;
    apiAddress: string;
    moduleName: string;
    errorMessage: string;
    errorStackTrace: string;
};

type DeviceAuthorizationConfig = {
    from: string;
    to: string;
    apiAddress: string;
    isDesktop: boolean;
    device: string;
    location: string;
    authorizeToken: string;
};

class mailerService extends baseService {
    static async sendError(config: ErrorEmailConfig): Promise<ServiceResponse<void>> {
        let res;
        try {
            res = await superagent.post(`http://${process.env.MAILER_HOSTNAME}/send/error`).send(config);
        } catch (err) {
            this.handleError(err);
        }
        return res;
    }

    static async sendNewDeviceActivity(config: DeviceAuthorizationConfig): Promise<ServiceResponse<void>> {
        let res;
        try {
            res = await superagent.post(`http://${process.env.MAILER_HOSTNAME}/send/new_device`).send(config);
        } catch (err) {
            this.handleError(err);
        }
        return res;
    }
}

type ServiceResponse<TData> = {
    body: TData;
};

type PdfData = {
    hash: string;
    text_pages: Array<string>;
    pdf_path: string;
    single_page_pdf_file_paths: Array<string>;
};

class pdfExtractService extends baseService {
    static async extract(fileStream: stream.Readable): Promise<ServiceResponse<PdfData>> {
        let encodedFile = await new Promise((resolve, reject) => {
            let data = '';
            fileStream.setEncoding('base64');
            fileStream.on('readable', function () {
                let chunk;
                while (null !== (chunk = fileStream.read())) {
                    data += chunk;
                }
            });
            fileStream.on('end', function () {
                resolve(data);
            });
            fileStream.on('error', function (err) {
                reject(err);
            });
        });

        let res;
        try {
            res = <ServiceResponse<PdfData>>(
                await superagent.post(`http://${process.env.PDF_EXTRACT_HOSTNAME}/extract`).send({
                    file: encodedFile,
                })
            );
        } catch (err) {
            this.handleError(err);
        }
        return res;
    }
}

export const mailer = mailerService;
export const pdfExtract = pdfExtractService;
