import * as _ from 'lodash';
import * as mongoose from 'mongoose';
import * as dayjs from 'dayjs';
import { AuthenticatedRequest, makeBaseRouter, QueryHandler } from './utils/queryUtil';
import { defaultContext } from './utils/context';
import { SystemCategory, CategoryType } from '../models/enums';
import { ClientError } from './utils/errors';
import * as dateUtil from '../modules/dateUtil';

import { Goal } from '../daos/goal';
import { Category } from '../daos/category';
import { Budget } from '../daos/budget';
import { getCategories } from '../controllers/budget';
const Transaction: any = mongoose.model('transaction', require('../daos/transaction'));

export default function getRouter() {
    const queryHandler = QueryHandler.getHandlerFactory(this);
    const router = makeBaseRouter();

    router
        .route('/:goal_id')
        .all(defaultContext)
        .put(queryHandler.makeHandler(_update))
        .delete(queryHandler.makeHandler(_delete));
    router.route('/:goal_id/complete').all(defaultContext).put(queryHandler.makeHandler(_complete));
    router.route('/').get(queryHandler.makeHandler(_listGoals)).post(queryHandler.makeHandler(_create));

    return router;
}

async function _complete(req: AuthenticatedRequest) {
    const goal = req.context.goal_id;
    const transactionIds =
        req.body.transactions?.map((id: mongoose.Types.ObjectId) => new mongoose.Types.ObjectId(id)) || [];
    if (_.isEmpty(transactionIds)) {
        throw new ClientError('Au moins une transaction doit être sélectionné.');
    }

    const { category: goalCategory } = await _getGlobalGoalBudget(req.userId);
    const subCategory = new Category({
        userId: req.userId,
        name: goal.name,
        parent: goalCategory.name,
        type: CategoryType.base,
    });
    await subCategory.save();

    const transactions = await Transaction.find({ _id: transactionIds }).forUser(req.userId);
    for (let transaction of transactions) {
        transaction.category = goal.name;
        transaction.ignoreForAutoCategoryAssignment = true;
        await transaction.save();
    }

    goal.completionDate = new Date();
    goal.finalAmount = Math.abs(_.sumBy(transactions, (t: any) => t.amount));
    goal.transactions = transactionIds;
    await goal.save();
}

async function _update(req: AuthenticatedRequest) {
    const goal = req.context.goal_id;
    const isCompleted = !!goal.completionDate;
    if (isCompleted) {
        throw new ClientError('Impossible de modifier un objectif complété.');
    }

    const updatedGoal = _.pick(req.body, ['name', 'amount', 'description', 'deadline', 'necessity']);
    goal.name = updatedGoal.name;
    goal.amount = updatedGoal.amount;
    goal.description = updatedGoal.description;
    goal.deadline = updatedGoal.deadline;
    goal.necessity = updatedGoal.necessity;
    await goal.save();

    return goal.toDto();
}

async function _delete(req: AuthenticatedRequest) {
    const goal = req.context.goal_id;
    const isCompleted = !!goal.completionDate;
    if (isCompleted) {
        throw new ClientError('Impossible de supprimer un objectif complété.');
    }
    await goal.remove();
}

async function _listGoals(req: AuthenticatedRequest) {
    const goals = await Goal.find({
        $or: [
            { completionDate: { $exists: false } },
            { completionDate: null },
            {
                completionDate: {
                    $gte: dayjs().startOf('year').toDate(),
                    $lte: dayjs().endOf('year').toDate(),
                },
            },
        ],
    }).forUser(req.userId);
    const { budgetEntry: goalBudgetEntry } = await _getGlobalGoalBudget(req.userId);
    return {
        // all goals completed that year + all pending goals
        items: goals.map((goal) => goal.toDto()),
        // infos about the goals summary
        goalYearlyBudget: goalBudgetEntry ? dateUtil.periodsInYear(goalBudgetEntry.period) * goalBudgetEntry.amount : 0,
    };
}

async function _getGlobalGoalBudget(userId: string) {
    const budget = await Budget.getClosest(userId, dayjs().year(), true);
    if (!budget || budget.year !== dayjs().year()) {
        throw new ClientError(`Aucun budget d'ouvert pour l'année courante.`);
    }
    const goalCategory = (await getCategories(budget)).find((category) => category.systemType === SystemCategory.goal);
    const goalBudgetEntry =
        goalCategory && (await budget.entries.find((entry) => entry.categoryId.equals(goalCategory._id)));
    return {
        category: goalCategory,
        budgetEntry: goalBudgetEntry,
    };
}

async function _create(req: AuthenticatedRequest) {
    // pretend validation is done already
    const goal = new Goal({
        ..._.pick(req.body, ['name', 'amount', 'description', 'deadline', 'necessity']),
        userId: req.userId,
    });
    await goal.save();
    return goal.toDto();
}
