/**
 * Created by hugo on 2018-05-13.
 */
const mongoose = require('mongoose');
const _ = require('lodash');
const dayjs = require('dayjs');

const BaseCtrl = require('./BaseController');
const FileDao = require('../daos/file');
const esCtrl = require('../daos/elasticsearch/elasticsearch');
const { StatementFileType, parseStatementFileType, parseAccountType, isLoanAccount } = require('../models/enums');
const queryUtil = require('./utils/queryUtil');
const { ClientError } = require('./utils/errors');
const { parseMultiPart } = require('../modules/multipartUtil');
const cacheUtil = require('../modules/cacheUtil');
const transactionService = require('../modules/transactionService');
const { defaultContext } = require('./utils/context');
const transactionProcessor = require('../import/csv_processor/transactionProcessor');
const searchablePdfProcessor = require('../import/pdf_processor/searchablePdfProcessor');

const { Statement } = require('../daos/statement');
const Transaction = mongoose.model('transaction', require('../daos/transaction'));
const { Account } = require('../daos/account');
const { Category } = require('../daos/category');
const { createStatementWithTransactions } = require('../controllers/statement');

class StatementCtrl extends BaseCtrl {
    static getRouter() {
        super.getRouter();

        this._router.route('/approved/:is_approved').get(this._handlerFactory.makeHandler(this._getByApprovedState));
        this._router.route('/history').get(this._handlerFactory.makeHandler(this._getRecent));
        this._router.route('/count').get(this._handlerFactory.makeHandler(this._countApproved));
        this._router
            .route('/:statement_id')
            .all(defaultContext)
            .get(this._handlerFactory.makeHandler(this._getStatement))
            .delete(this._handlerFactory.makeHandler(this._deleteStatement));
        this._router
            .route('/:statement_id/approve')
            .all(defaultContext)
            .put(this._handlerFactory.makeHandler(this._approveStatement));
        this._router
            .route('/')
            .get(this._handlerFactory.makeHandler(this._getMany))
            .post(this._handlerFactory.makeHandler(this._create))
            .put(this._handlerFactory.makeHandler(this._replace));

        return this._router;
    }

    /**
     * Find transactions by statement approval status
     * @param req
     * @param res
     * @private
     */
    // GET  /api/statement/approved/:is_approved
    static async _getByApprovedState(req, res) {
        /**
         * @property is_approved {string}
         */
        let params = req.params;

        let statements = await Statement.find({
            isApproved: queryUtil.toBoolean(params.is_approved) || false,
        })
            .forUser(req.userId)
            .lean();
        return { statements };
    }

    // GET  /api/statement?size={number}&skip={number}&sort={object}&filter={object}
    static async _getMany(req, res) {
        /**
         * @property {string} size
         * @property {string} skip
         * @property {string} filter
         * @property {string} sort
         */
        const query = req.query;

        const size = queryUtil.toInt(query.size || '50');
        const skip = queryUtil.toInt(query.skip || '0');
        const filter = queryUtil.toObject(query.filter) || {};
        const sort = queryUtil.toObject(query.sort) || {};

        const statements = await Statement.searchForUser(req.userId, size, skip, filter, sort);
        return { items: statements.map((_s) => _s.toDto()) };

        // TODO: - Add the optional field function call here
        //       - Implement the transaction_count and last_transaction_date fields
    }

    /**
     * Get a list of statements by descending order of date.
     * @param req
     * @param res
     * @private
     * @deprecated
     * FIXME: move the functionality of this route to the generic _getMany route (add a transaction count optional field)
     */
    // GET  /api/statement/history?size={number}&skip={number}
    static async _getRecent(req, res) {
        /**
         * @property size {string}
         * @property skip {string}
         */
        let query = req.query;

        let size = queryUtil.toInt(query.size) || 40;
        let skip = queryUtil.toInt(query.skip) || 0;

        let statementInfo = await Statement.aggregateTransactionCount(req.userId, size, skip);

        return {
            importHistory: statementInfo.map((s) => {
                s.fileType = parseStatementFileType(parseInt(s.fileType));
                return {
                    statementData: s,
                    lastTransactionDate: s.lastTransactionDate,
                    transactionCount: s.transactionCount,
                };
            }),
        };
    }

    /**
     * Statement count query
     * @param req
     * @param res
     * @private
     */
    // GET  /api/statement/count
    static async _countApproved(req, res) {
        let count = await Statement.countDocuments({
            userId: req.userId,
            isApproved: false,
        });

        return { count };
    }

    /**
     * Get a statement by its id.
     * @param req
     * @param res
     * @private
     */
    // GET  /api/statement/:statement_id
    static async _getStatement(req, res) {
        return { statement: req.context.statement_id };
    }

    // DELETE   /api/statement/:statement_id
    static async _deleteStatement(req, res) {
        const statement = req.context.statement_id;

        await Transaction.deleteMany({ statementId: req.params.statement_id });
        await esCtrl.deleteTransactionsByStatement(req.params.statement_id);
        if (statement.fileType !== StatementFileType.webScraper) {
            await FileDao.remove(statement.fileId);
        }

        await statement.remove();
    }

    // PUT  /api/statement/:statement_id/approve
    static async _approveStatement(req, res) {
        const statementId = req.params.statement_id;
        const statement = req.context.statement_id;

        let transactions = await Transaction.find({ statementId: statementId }).forUser(req.userId);

        await esCtrl.deleteTransactionsByStatement(statementId);
        await esCtrl.indexTransactions(transactions, statement.fileType);

        statement.isApproved = true;
        await statement.save();
    }

    // POST /api/statement
    static async _create(req, res) {
        let insertedStatement;
        let cacheExpirationEvents = ['STATEMENT_UNAPPROVED'];

        if (req.headers['content-type'].includes('multipart/form-data')) {
            // assume we are receiving a file and transactions data
            const { statement, file } = await this._extractAndValidateStatementData(req);

            statement.transactions.sort((a, b) => {
                return new Date(a.date).getTime() - new Date(b.date).getTime();
            });
            insertedStatement = await createStatementWithTransactions({
                userId: req.userId,
                transactions: statement.transactions,
                date: statement.transactions[0].date,
                fileType: statement.fileType,
                fileId: file._id,
            });

            await this._updateAccounts(req.userId, statement.transactions, file, statement.fileType);

            cacheExpirationEvents.push(...(await cacheUtil.getTransactionExpirationEvents(statement.transactions)));
        } else {
            // creating an empty statement
            insertedStatement = new Statement({
                userId: req.userId,
                date: req.body.date,
                fileType: req.body.fileType,
            });
            await insertedStatement.save();
        }

        await req.user.addCacheExpirationEvents(cacheExpirationEvents);

        return {
            statement: insertedStatement.toDto(),
        };
    }

    /**
     * Create a new statement, replacing the transactions within that date range.
     * @param req
     * @param res
     * @private
     */
    // PUT  /api/statement
    static async _replace(req, res) {
        const { statement, file } = await this._extractAndValidateStatementData(req);
        const existingTransactions = await transactionService.findExistingTransactions(
            statement.transactions,
            req.userId
        );

        let unchangedTransactions = [];
        let newTransactions = [];
        let removedTransactions = [];
        transactionService.iterateDiffs(statement.transactions, existingTransactions, (added, removed) => {
            if (added && removed) {
                unchangedTransactions.push(removed);
            } else if (added) {
                newTransactions.push(added);
            } else if (removed) {
                removedTransactions.push(removed);
            }
        });

        const categoryNames = _.chain(newTransactions)
            .map((_t) => _t.category)
            .uniq()
            .filter(Boolean)
            .value();
        await Category.createMissingBaseCategories(req.userId, categoryNames);

        const statementTransactions = [...unchangedTransactions, ...newTransactions];
        const transactionDates = _.chain(statementTransactions)
            .map((_t) => _t.date)
            .sortBy((_date) => _date.getTime())
            .value();
        let newStatement = await createStatementWithTransactions({
            userId: req.userId,
            transactions: newTransactions,
            date: _.first(transactionDates),
            fileType: statement.fileType,
            fileId: file._id,
        });
        await Transaction.updateMany(
            { _id: { $in: unchangedTransactions.map((_t) => _t._id) } },
            {
                $set: { statementId: newStatement._id },
            }
        );

        // Sometimes, transactions of a particular day will be split between two statements, if that day is the
        // first day of the statement (and the last day of the previous statement).
        // Let two statements, A and B, where statement A ends the same day statement B starts.
        // If B is uploaded through this route, existing transactions dated for the 1st day of B will be deleted.
        // ---> This is why we filter out removed transactions this next line :
        removedTransactions = removedTransactions.filter(
            (_t) =>
                _t.date.getTime() !== _.first(transactionDates).getTime() &&
                _t.date.getTime() !== _.last(transactionDates).getTime()
        );
        const removedIds = removedTransactions.map((_t) => _t._id);
        await Transaction.remove({
            _id: { $in: removedIds },
        });

        const overlappingStatementIds = _.uniq(
            [...unchangedTransactions, ...removedTransactions].map((_t) => _t.statementId.toString())
        );
        for (let statementId of overlappingStatementIds) {
            // this code doesnt work, FIXME
            const transactionCount = await Transaction.countDocuments({ statementId });
            if (transactionCount === 0) {
                const deletedStatement = await Statement.findByIdAndRemove(statementId);
                if (deletedStatement.fileId) {
                    await FileDao.remove(deletedStatement.fileId);
                }
            }
        }

        await this._updateAccounts(req.userId, statement.transactions, file, statement.fileType);

        await esCtrl.deleteTransactionsById(removedIds);

        await req.user.addCacheExpirationEvents([
            'STATEMENT_UNAPPROVED',
            ...(await cacheUtil.getTransactionExpirationEvents(statementTransactions)),
        ]);

        return {
            statement: newStatement,
        };
    }

    static async _updateAccounts(userId, transactions, file, fileType) {
        const accountIds = transactionService.findUniqAccounts(transactions);
        const accounts = await Account.find({
            accountName: { $in: accountIds },
        })
            .forUser(userId)
            .lean();

        let statementAccounts = null;

        for (let accountId of accountIds) {
            let account = accounts.find((_a) => _a.accountName === accountId);
            let mostRecentTransactionDate = _.maxBy(
                transactions.filter((_t) => _t.account.includes(accountId)).map((_t) => dayjs(_t.date)),
                (_d) => _d.valueOf()
            );

            if (!account || dayjs(account.updatedAt).isBefore(mostRecentTransactionDate)) {
                // account either doesn't exist or a new transaction has newer balance info
                if (!statementAccounts) {
                    statementAccounts = await this._extractAccountBalances(file, fileType);
                }
                const statementAccount = statementAccounts.find((_a) => _a.accountName === accountId);
                const accountType = parseAccountType(accountId);
                await Account.createOrUpdate(
                    {
                        _id: (account && account._id) || null,
                        accountName: accountId,
                        amount: isLoanAccount(accountType) ? statementAccount.balance * -1 : statementAccount.balance,
                        updatedAt: statementAccount.date,
                        accountType,
                    },
                    userId
                );
            }
        }
    }

    static async _extractAccountBalances(file, fileType) {
        const stream = await FileDao.download(file._id);
        let accounts = [];

        let _file = await FileDao.find({ _id: file._id }, true);
        let fileExtension = _file.filename.split('.').pop();
        switch (fileExtension.toLowerCase()) {
            case 'csv':
                accounts = await transactionProcessor.getBalance(fileType, stream);
                break;
            case 'pdf':
                accounts = await searchablePdfProcessor.getBalance(fileType, stream);
                break;
            default:
                throw new ClientError(`Type de fichier non supporté (${fileExtension}).`);
        }

        return accounts;
    }

    static async _extractAndValidateStatementData(request) {
        const { fields, stream } = await parseMultiPart(request, ['statement'], true);
        if (!stream) {
            throw new ClientError(`Aucun fichier n'a été reçu.`);
        }

        let statement;
        try {
            statement = JSON.parse(fields.get('statement'));
        } catch (err) {
            throw new ClientError('Invalid statement JSON.');
        }

        let file = await FileDao.save(stream, statement.filename);

        if (!parseStatementFileType(statement.fileType)) {
            throw new ClientError('Invalid statement file type.');
        } else if (!(await Transaction.validate(request.userId, statement.transactions))) {
            throw new ClientError('Erreur de validation des transactions.');
        }

        return { statement, file };
    }
}

module.exports = StatementCtrl;
