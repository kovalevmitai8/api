/**
 * Created by hugo on 2018-05-18
 */
const mongoose = require('mongoose');
const _ = require('lodash');
const dayjs = require('dayjs');

const BaseCtrl = require('./BaseController');
const queryUtil = require('./utils/queryUtil');
const { ClientError } = require('./utils/errors');
const attachmentCtrl = require('./attachment');
const { defaultContext } = require('./utils/context');

const Transaction = mongoose.model('transaction', require('../daos/transaction'));
const { Category } = require('../daos/category');
const { Statement } = require('../daos/statement');
const { Budget } = require('../daos/budget');

const { CategoryType, SystemCategory, StatementFileType } = require('../models/enums');
const { toTransactionDto } = require('../models/entities');
const transactionService = require('../modules/transactionService');
const esCtrl = require('../daos/elasticsearch/elasticsearch');
const { filterRecurringTransactions } = require('../controllers/budget');
const { updateApprovedStatus } = require('../controllers/statement');
const { searchTransactions, appendOptionalFields, assignCategories } = require('../controllers/transaction');

class TransactionCtrl extends BaseCtrl {
    static getRouter() {
        super.getRouter();

        this._router.use(attachmentCtrl.getTransactionRouter());

        this.registerRoute('/pending').put(this.makeHandler(this._replacePendingTransactions));
        this.registerRoute('/assign_category/:transaction_id')
            .all(defaultContext)
            .put(this.makeHandler(this._assignCategory));
        this.registerRoute('/goal/:goal_id')
            .all(defaultContext)
            .get(this.makeHandler(this._listGoalRelatedTransactions));
        this.registerRoute('/:transaction_id')
            .all(defaultContext)
            .get(this.makeHandler(this._getTransaction))
            .put(this.makeHandler(this._updateTransaction))
            .delete(this.makeHandler(this._deleteTransaction));
        this.registerRoute('/').get(this.makeHandler(this._getMany)).put(this.makeHandler(this._replaceMany));

        return this._router;
    }

    /**
     * List transactions for the user, with optional filters, sorting and paging.
     * @param req
     * @param res
     * @returns {Promise<{ items: Transaction[] }>}
     * @private
     */
    // GET  /api/transaction?size={number}&skip={number}&filter={object}&sort={object}&fields={array}
    static async _getMany(req, res) {
        /**
         * @property {string} size
         * @property {string} skip
         * @property {string} filter
         * @property {string} sort
         */
        const query = req.query;

        const size = queryUtil.toInt(query.size || '50');
        const skip = queryUtil.toInt(query.skip || '0');
        const filter = queryUtil.toObject(query.filter) || {};
        const sort = queryUtil.toObject(query.sort) || {};
        const fields = queryUtil.toObject(query.fields) || [];

        let transactionIds = null;
        if (query.q) {
            let from = filter?.date_from && new Date(filter?.date_from);
            let to = filter?.date_to && new Date(filter?.date_to);
            transactionIds = await esCtrl.search(req.userId, query.q, size, skip, from, to);
        }

        const transactions = await searchTransactions({
            userId: req.userId,
            size,
            skip,
            filter,
            sort,
            preSearchIds: transactionIds,
        });
        return {
            items: await appendOptionalFields(req.userId, fields, transactions),
        };
    }

    static async _listGoalRelatedTransactions(req) {
        const goal = req.context.goal_id;
        const transactions = await Transaction.find({ _id: { $in: goal.transactions } })
            .forUser(req.userId)
            .lean();

        return {
            items: await appendOptionalFields(req.userId, ['categoryHierarchy'], transactions),
        };
    }

    static async _replacePendingTransactions(req) {
        let transactions = req.body;

        if (!(await Transaction.validate(req.userId, transactions))) {
            throw new ClientError(`Transaction validation error.`);
        }

        await Transaction.deleteMany({
            userId: req.userId,
            isPending: true,
        });

        const categoryGuesser = await esCtrl.createCategoryGuesstimator(req.userId, transactions);
        const categories = await Category.find().forUser(req.userId).lean();

        transactions = await assignCategories(req.userId, transactions);
        for (let transaction of transactions) {
            transaction.userId = req.userId;
            transaction.isPending = true;
            let categoryMatch = categoryGuesser.find(transaction.descriptionInfo || transaction.description);
            if (categoryMatch) {
                let category = categories.find((category) => category.name.toLowerCase() === categoryMatch.category);
                transaction.category = category?.name;
            }
        }

        await Transaction.insertMany(transactions);
    }

    /**
     * Insert new transactions only, ignoring the ones that already exist.
     * Assign a category if none is specified
     * @param req
     * @param res
     * @private
     */
    // PUT  /api/transaction
    static async _replaceMany(req, res) {
        let transactions = req.body;

        if (!(await Transaction.validate(req.userId, transactions))) {
            throw new ClientError(`Transaction validation error.`);
        }

        const existingTransactions = await transactionService.findExistingTransactions(transactions, req.userId);

        let newTransactions = [];
        transactionService.iterateDiffs(transactions, existingTransactions, (added, removed) => {
            if (added && !removed) {
                newTransactions.push(added);
            }
        });

        if (_.isEmpty(newTransactions)) {
            return { items: [] };
        }

        for (let [month, transactionGroup] of Object.entries(
            _.groupBy(newTransactions, (_t) => dayjs(_t.date).month())
        )) {
            let firstOfMonth = dayjs(transactionGroup[0].date).month(month).startOf(`month`).toDate();

            let statement = await Statement.findOne({
                date: firstOfMonth,
                fileType: StatementFileType.webScraper,
            }).forUser(req.userId);
            if (!statement) {
                statement = new Statement({
                    userId: req.userId,
                    date: firstOfMonth,
                    fileType: StatementFileType.webScraper,
                });
                await statement.save();
            }

            for (let transaction of transactionGroup) {
                transaction.statementId = statement._id;
            }
        }

        newTransactions = await assignCategories(req.userId, newTransactions);

        const startDate = _.minBy(
            newTransactions.map((_t) => dayjs(_t.date)),
            (_d) => _d.valueOf()
        );
        let recurringTransactions = [];
        const currentBudget = await Budget.get(req.userId, startDate.year());
        if (currentBudget) {
            recurringTransactions = await filterRecurringTransactions(currentBudget, newTransactions);
        }
        newTransactions.forEach((_t) => (_t.recurring = recurringTransactions.includes(_t)));

        newTransactions = await Transaction.insertMany(
            newTransactions.map((_t) => ({
                userId: req.userId,
                ..._t,
            }))
        );

        // create new categories
        const distinctCategories = _.chain(newTransactions)
            .map((t) => t.category)
            .filter(Boolean)
            .uniq()
            .value();
        for (const category of distinctCategories) {
            const matchingTransactions = newTransactions.filter((t) => t.category === category);
            await Category.createIfNotExists(
                req.userId,
                CategoryType.base,
                category,
                _.first(matchingTransactions).amount > 0 ? SystemCategory.revenu : null
            );
        }

        await this._createCacheExpirationEvents(req.user, newTransactions);
        await esCtrl.indexTransactions(newTransactions, StatementFileType.webScraper);

        return { items: newTransactions.map((t) => toTransactionDto(t)) };
    }

    /**
     * Create the appropriate cache expiration events based on the inserted entities
     * @param {User} user
     * @param {Transaction[]} transactions
     * @returns {Promise<void>}
     */
    static async _createCacheExpirationEvents(user, transactions) {
        let cacheExpirationEvents = [];

        let excludedCategories = await Category.find({
            type: CategoryType.base,
            systemType: SystemCategory.virement,
        })
            .forUser(user.googleUserId)
            .select('name')
            .lean();
        let excludedCategoryNames = excludedCategories.map((c) => c.name);

        if (transactions.filter((_t) => !excludedCategoryNames.includes(_t.category)).length > 0) {
            cacheExpirationEvents.push('NON_TRANSFER_TRANSACTIONS_IMPORTED');
        } else if (transactions.length > 0) {
            cacheExpirationEvents.push('TRANSACTIONS_IMPORTED');
        }

        await user.addCacheExpirationEvents(cacheExpirationEvents);
    }

    static async _getTransaction(req, res) {
        /**
         * @property forSubCategoryAssignment {string}
         */
        let query = req.query;
        const transactionId = req.params.transaction_id;
        const transaction = req.context.transaction_id;

        // GET  /api/transaction/:transaction_id?forSubCategoryAssignment={boolean}
        if (transaction && !!query.forSubCategoryAssignment) {
            let transactions = await Transaction.getForSubCategoryAssignment(req.userId, transactionId);

            // get previous and next transactions id
            let distinctTransactions = await Transaction.getDistinctForSubCategoryAssignment(
                req.userId,
                transactions[0].statementId,
                true
            );
            let distinctTransactionIds = distinctTransactions.map((t) => t._id.toString());
            let currentTransactionIndex = distinctTransactionIds.indexOf(transactionId);
            let previousTransactionId = null;
            let nextTransactionId = null;

            if (currentTransactionIndex > 0) {
                previousTransactionId = distinctTransactionIds[currentTransactionIndex - 1];
            }
            if (currentTransactionIndex < distinctTransactionIds.length - 1) {
                nextTransactionId = distinctTransactionIds[currentTransactionIndex + 1];
            }

            return {
                transactions: transactions,
                previousTransactionId: previousTransactionId,
                nextTransactionId: nextTransactionId,
            };

            // GET  /api/transaction/:transaction_id
        } else if (transaction) {
            return { transaction };
        } else {
            throw new ClientError(`Transaction cannot be queried for field(s): ${Object.keys(query)}`, 404);
        }
    }

    // PUT  /api/transaction/assign_category/:transaction_id
    static async _assignCategory(req, res) {
        const transactionId = req.params.transaction_id;
        const transaction = req.context.transaction_id;

        /**
         * @property category_name {string}
         * @property system_type {string}
         */
        let body = req.body;
        if (!body.category_name) {
            throw new ClientError(`category_name is required.`);
        }

        const relatedBudgets = await Budget.find({
            year: {
                $lte: transaction.date.getFullYear(),
            },
        })
            .forUser(req.userId)
            .limit(1)
            .sort('-year')
            .select('categoriesSnapshot')
            .lean();
        const relatedBudget = relatedBudgets[0];
        // if the transaction can be scoped to a closed budget of previous years, prevent editing the transaction
        if (relatedBudget && !_.isEmpty(relatedBudget.categoriesSnapshot)) {
            throw new ClientError(
                `La transaction est lié à un budget fermé. Pour modifier la transaction, ` +
                    `utilisez la fonctionnalité 'Time Travel' vers le budget correspondant.`
            );
        }

        let transactionsToUpdate = await Transaction.getForSubCategoryAssignment(req.userId, transactionId);

        await Category.createIfNotExists(req.userId, CategoryType.base, body.category_name, body.system_type);
        await Transaction.updateMany(
            { _id: { $in: transactionsToUpdate.map((_t) => _t._id) } },
            { $set: { category: body.category_name, manuallyApproved: true } }
        );

        // check if the statement was created by the web scrapper
        if (transactionsToUpdate.length > 0) {
            await updateApprovedStatus({
                userId: req.userId,
                statementId: transactionsToUpdate[0].statementId.toString(),
            });
        }

        await esCtrl.upsertTransactions(
            transactionsToUpdate.map((t) => ({
                id: t._id,
                category: body.category_name,
                manuallyApproved: true,
            }))
        );
    }

    // PUT  /api/transaction/:transaction_id
    static async _updateTransaction(req, res) {
        /**
         * @property category {string}
         * @property account {string}
         * @property amount {number}
         * @property currency {string}
         * @property date {string}
         * @property description {string}
         * @property type {string}
         * @property statementId {string}
         */
        let updatedTransaction = req.body.transaction;
        const transaction = req.context.transaction_id;

        transaction.account = updatedTransaction.account;
        transaction.amount = updatedTransaction.amount;
        transaction.currency = updatedTransaction.currency;
        transaction.date = updatedTransaction.date;
        transaction.description = updatedTransaction.description;
        transaction.location = updatedTransaction.location;
        transaction.statementId = updatedTransaction.statementId;

        if (transaction.category !== updatedTransaction.category) {
            const relatedBudgets = await Budget.find({
                userId: req.userId,
                year: {
                    $lte: dayjs(updatedTransaction.date).year(),
                },
            })
                .limit(1)
                .sort('-year')
                .select('categoriesSnapshot year')
                .lean();
            const relatedBudget = relatedBudgets[0];
            // if the transaction can be scoped to a closed budget of previous years, prevent editing the transaction
            if (relatedBudget && !_.isEmpty(relatedBudget.categoriesSnapshot) && relatedBudget.year < dayjs().year()) {
                throw new ClientError(
                    `La transaction est lié à un budget fermé. Pour modifier la transaction, ` +
                        `utilisez la fonctionnalité 'Time Travel' vers le budget correspondant.`
                );
            }

            await Category.createIfNotExists(
                req.userId,
                CategoryType.base,
                updatedTransaction.category,
                req.body.systemType
            );

            const applyCategoryToAll = queryUtil.toBoolean(req.query.applyCategoryToAll) ?? false;
            transaction.ignoreForAutoCategoryAssignment = !applyCategoryToAll;
            transaction.category = updatedTransaction.category;

            await transaction.save();

            if (applyCategoryToAll) {
                await Transaction.assignCategoryToSimilarTransactions(
                    req.userId,
                    req.params.transaction_id,
                    updatedTransaction.category
                );
            }

            await updateApprovedStatus({
                userId: req.userId,
                statementId: updatedTransaction.statementId.toString(),
            });
        }

        await esCtrl.upsertTransactions([transaction.toEsEntity()]);
    }

    // DELETE   /api/transaction/:transaction_id
    static async _deleteTransaction(req, res) {
        const transaction = req.context.transaction_id;
        await transaction.remove();
        await esCtrl.deleteTransaction(req.params.transaction_id);
    }
}
module.exports = TransactionCtrl;
