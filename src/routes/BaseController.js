const express = require('express');
const { QueryHandler } = require('./utils/queryUtil');
const securityCtrl = require('./security');

class BaseController {
    static getRouter() {
        this._router = express.Router({});
        this._handlerFactory = QueryHandler.getHandlerFactory(this);
        this._router.use(securityCtrl.verificationMiddleware);
        return this._router;
    }

    static registerRoute(url) {
        return this._router.route(url);
    }

    static makeHandler(routeHandler) {
        return this._handlerFactory.makeHandler(routeHandler);
    }
}

module.exports = BaseController;
