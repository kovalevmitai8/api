const mongoose = require('mongoose');
const _ = require('lodash');
const dayjs = require('dayjs');

const BaseCtrl = require('./BaseController');
const transactionService = require('../modules/transactionService');
const queryUtil = require('./utils/queryUtil');
const { ClientError } = require('./utils/errors');
const { getStream } = require('../modules/multipartUtil');
const { toTransactionDto } = require('../models/entities');
const transactionProcessor = require('../import/csv_processor/transactionProcessor');
const searchablePdfProcessor = require('../import/pdf_processor/searchablePdfProcessor');
const { assignCategories } = require('../controllers/transaction');

class extractCtrl extends BaseCtrl {
    static getRouter() {
        super.getRouter();

        this._router
            .route('/diff/:statement_type')
            .post(this._handlerFactory.makeHandler(this._extractStatementAndCompare));

        return this._router;
    }

    // POST /api/extract/diff:statement_type
    static async _extractStatementAndCompare(req, res) {
        /**
         * @property statement_type {string} Type of file as integer
         */
        let params = req.params;
        let fileType = queryUtil.toInt(params.statement_type);

        const { stream, filename } = await getStream(req);
        if (!stream) {
            throw new ClientError(`Aucun fichier n'a été reçu.`);
        }

        let newTransactions = [];

        let fileExtension = filename.split('.').pop();
        switch (fileExtension.toLowerCase()) {
            case 'csv':
                newTransactions = await transactionProcessor.extract(fileType, stream);
                break;
            case 'pdf':
                newTransactions = await searchablePdfProcessor.extract(fileType, stream);
                break;
            default:
                throw new ClientError(`Type de fichier non supporté (${fileExtension}).`);
        }

        newTransactions = await assignCategories(req.userId, newTransactions);
        newTransactions = _.sortBy(newTransactions, (_t) => new Date(_t.date).getTime());

        let existingTransactions = await transactionService.findExistingTransactions(newTransactions, req.userId);
        existingTransactions = _.sortBy(existingTransactions, (_t) => new Date(_t.date).getTime());

        const diffs = transactionService.processDiff(
            newTransactions.map((_t) => toTransactionDto(_t)),
            existingTransactions.map((_t) => toTransactionDto(_t))
        );
        return {
            items: newTransactions.map((_t) => toTransactionDto(_t)),
            // remove "removed" diffs on the first and last day, those will be ignored anyways at the import.
            diffs: diffs.filter((_d) => {
                if (_d.removed) {
                    _d.removed = _d.removed.filter(
                        (_t) =>
                            !dayjs(_.first(newTransactions).date).isSame(dayjs(_t.date), 'day') &&
                            !dayjs(_.last(newTransactions).date).isSame(dayjs(_t.date), 'day')
                    );
                    if (_d.removed.length === 0) {
                        return false;
                    }
                }
                return true;
            }),
        };
    }
}

module.exports = extractCtrl;
