/**
 * Created by hugo on 2020-03-04
 */
const mongoose = require('mongoose');
const _ = require('lodash');
const dayjs = require('dayjs');

const BaseCtrl = require('./BaseController');
const queryUtil = require('./utils/queryUtil');
const NTree = require('../modules/nTree').default;
const { CategoryType, SystemCategory } = require('../models/enums');
const dateHelper = require('../modules/dateUtil');
const { ClientError } = require('./utils/errors');
const transactionCtrl = require('../controllers/transaction');
const { getCategories, findRelatedTransactions, timeTravel, getBudgetStatuses } = require('../controllers/budget');

const { BudgetModelingBuilder } = require('../controllers/utils/BudgetModeling');

const { Budget } = require('../daos/budget');
const Transaction = mongoose.model('transaction', require('../daos/transaction'));
const { Category } = require('../daos/category');

class BudgetCtrl extends BaseCtrl {
    static getRouter() {
        super.getRouter();

        this.registerRoute('/:year/category_summary').get(this.makeHandler(this._categorySummary));
        this.registerRoute('/:year/category_summary/:category_id').get(this.makeHandler(this._categorySummary));
        this.registerRoute('/status').get(this.makeHandler(this._getBudgetStatuses));

        this.registerRoute('/:year/category/:category_id')
            .get(this.makeHandler(this._getBudgetEntry))
            .put(this.makeHandler(this._putCategoryBudget))
            .delete(this.makeHandler(this._deleteCategoryBudget));
        this.registerRoute('/:year').get(this.makeHandler(this._getBudget)).post(this.makeHandler(this._createBudget));

        this.registerRoute('/time_travel/:target_year').put(this.makeHandler(this._timeTravel));

        this.registerRoute('/').get(this.makeHandler(this._listBudgets));

        return this._router;
    }

    /**
     * @param req
     * @returns {Promise<{items: *}>}
     * @private
     */
    static async _listBudgets(req) {
        const budgets = await Budget.find()
            .forUser(req.userId)
            .sort('year')
            .select('_id categoriesSnapshot year isHidden')
            .lean();
        return {
            items: budgets.map((_budget) => ({
                ..._budget,
                isClosed: !_.isEmpty(_budget.categoriesSnapshot),
                categoriesSnapshot: undefined,
            })),
            isTimeTravel: !_.isEmpty(budgets) && _.last(budgets).isHidden,
        };
    }

    /**
     * Calculate the sum of planned and observed transactions for each root category for the year
     * @param req
     * @private
     */
    // GET  /api/budget/:year/category_summary/:category_id?from={date_str}&to={date_str}
    static async _categorySummary(req) {
        /**
         * @property year
         * @property [category_id]
         */
        const params = req.params;
        const year = queryUtil.toInt(params.year);
        /**
         * @property from
         * @property to
         */
        const query = req.query;
        const from = (query.from && dayjs(query.from)) || dateHelper.startOfYear(year);
        const to = (query.to && dayjs(query.to)) || dateHelper.endOfYear(year);

        let rootCategoryIds = null;
        if (params.category_id) {
            const tempModel = await BudgetModelingBuilder.create({
                userId: req.userId,
                budgetYear: year,
                from,
                to,
            }).build();
            const newRoot = tempModel.findNode((node) => node.value.category._id.toString() === params.category_id);
            rootCategoryIds = newRoot.children.map((node) => node.value.category._id.toString());
        }

        const budgetModel = await BudgetModelingBuilder.create({
            userId: req.userId,
            budgetYear: year,
            from,
            to,
        })
            .trimToRoots(rootCategoryIds)
            .appendObservedAmount()
            .appendPlannedAdjustedAmount()
            .appendTransactionCount()
            .build();

        const budget = budgetModel.budget;
        const baseResponse = {
            budgetYear: budget && budget.year,
            hasBudget: !!budget,
            isBudgetClosed: budget ? budget.isClosed : false,
        };
        let nonTransferRootNodes = budgetModel
            .getRootNodes()
            .filter((_rootNode) => _rootNode.value.category.systemType !== SystemCategory.virement);
        return _.chain(nonTransferRootNodes)
            .partition((categoryNode) => categoryNode.value.category.systemType === SystemCategory.revenu)
            .reduce(
                (response, categoryNodes, index) => ({
                    ...response,
                    [index === 0 ? 'revenue' : 'spending']: _.chain(categoryNodes)
                        .map((categoryNode) => {
                            const {
                                category,
                                plannedAmount,
                                observedAmount,
                                plannedAdjustedAmount,
                            } = categoryNode.value;
                            return {
                                name: category.name,
                                hue: category.hue || '',
                                observed: Math.abs(observedAmount),
                                planned: plannedAmount || null,
                                plannedAdjusted: plannedAdjustedAmount || null,
                            };
                        })
                        .filter((_entry) => _entry.planned || _entry.observed)
                        .sortBy((entry) => _.deburr(entry.name))
                        .value(),
                }),
                baseResponse
            )
            .value();
    }

    /**
     * Get the budget and category trees.
     * @param req
     * @private
     */
    // GET  /api/budget/:year
    static async _getBudget(req) {
        let year = queryUtil.toInt(req.params.year);
        let budget = await Budget.get(req.userId, year, true);

        if (!budget) {
            throw new ClientError(`Pas de budget pour l'année ${year}.`);
        }

        return await this._prepareBudgetForClient(req.userId, year, budget);
    }

    static async _prepareBudgetForClient(userId, year, budget) {
        let categories = (budget && (await getCategories(budget))) || (await Category.find().forUser(userId).lean());
        let categoryTree = new NTree(
            categories,
            (c) => c.name,
            (c) => c.parent
        );
        let categoryTrees = _.chain(categoryTree.getRootNodes())
            .map((_rootNode) => this._prepareCategoryTree(_rootNode))
            .sortBy((_category) => _.deburr(_category.name))
            .value();
        return Budget.toDto(budget, categoryTrees);
    }

    static _prepareCategoryTree(categoryNode) {
        return Category.toDto(
            _.extend(categoryNode.value, {
                children: categoryNode.children.map((_childNode) => this._prepareCategoryTree(_childNode)),
            })
        );
    }

    /**
     * Create the budget of the year, if it does not exist and if the current budget can be closed.
     * A budget can be closed starting at the middle of the year.
     * @param req
     * @private
     */
    // POST /api/budget/:year
    static async _createBudget(req) {
        let year = queryUtil.toInt(req.params.year);
        let now = dayjs();
        if (year < now.year()) {
            throw new ClientError(`Impossible de créer un budget pour une année passé.`);
        }

        let existingBudget = await Budget.findOne({ userId: req.userId, year }).select('_id').lean();
        if (existingBudget) {
            throw new ClientError(`Un budget existe déjà pour l'année ${year}.`);
        }

        if (year > now.year()) {
            throw new ClientError(`Le budget actif ne peut pas être fermé avant la fin de l'année.`);
        }

        // Close the current budget, if there is one
        let budgets = await Budget.find({
            userId: req.userId,
        }).sort('-year');
        let activeBudget = budgets[0];
        if (activeBudget && !activeBudget.isClosed) {
            activeBudget.categoriesSnapshot = await Category.find().forUser(req.userId).lean();
            await activeBudget.save();
        }

        // Create the new one and return the appropriate data
        let budget = new Budget({
            userId: req.userId,
            year,
            entries: activeBudget ? activeBudget.cloneEntriesForYear(year) : [],
        });
        await budget.save();

        await this._clearUnUsedCategories(activeBudget);

        return await this._prepareBudgetForClient(req.userId, year, budget);
    }

    /**
     * Delete sub-categories that don't match the following conditions:
     * - A transaction in the past 365 days is bound to that sub-category
     * - It was created less than a year ago
     * - A budget entry exists for that sub-category
     * @param budget
     * @returns {Promise<void>}
     * @private
     */
    static async _clearUnUsedCategories(budget) {
        let categories = ((await getCategories(budget)) || []).filter(
            (category) => category.type === CategoryType.base
        );
        const previousYearActivityMap = await transactionCtrl.getLastActivity(
            budget.userId,
            dayjs().year(budget.year).startOf('year'),
            dayjs().year(budget.year).endOf('year'),
            categories
        );
        const namesToKeep = _.keys(previousYearActivityMap).map(
            (categoryId) => categories.find((c) => c._id.equals(categoryId)).name
        );
        categories = await Category.find({ type: CategoryType.base }).forUser(budget.userId);
        const currentYearActivityMap = await transactionCtrl.getLastActivity(
            budget.userId,
            dayjs().startOf('year'),
            dayjs().endOf('year'),
            categories
        );
        namesToKeep.push(
            ..._.keys(currentYearActivityMap).map((categoryId) => categories.find((c) => c._id.equals(categoryId)).name)
        );
        namesToKeep.push(
            ...budget.entries
                .map((entry) => categories.find((c) => c._id.equals(entry.categoryId))?.name)
                .filter(Boolean)
        );
        await Category.deleteMany({
            name: {
                $nin: _.uniq(namesToKeep),
            },
            type: CategoryType.base,
            creationDate: {
                $lt: dayjs().subtract(1, 'year').toDate(),
            },
        });
    }

    /**
     * Get a single budget entry and related category
     * @param req
     * @private
     */
    // GET  /api/budget/:year/category/:category_id
    static async _getBudgetEntry(req) {
        const { category, budget } = await this._getAndValidateData(req.userId, req.params);
        return {
            category: Category.toDto(category),
            budgetEntry: budget.findEntryByCategory(req.params.category_id),
        };
    }

    static async _getAndValidateData(userId, params) {
        const year = queryUtil.toInt(params.year);
        const categoryId = params.category_id;

        const budget = await Budget.get(userId, year);
        if (!budget) {
            throw new ClientError(`Aucune budget pour l'année '${year}'`);
        }

        const category =
            budget.categoriesSnapshot.find((_cat) => _cat._id.equals(categoryId)) ||
            (await Category.findOne({ _id: categoryId }).forUser(userId).lean());
        if (!category) {
            throw new ClientError(`The category does not exist.`, 404);
        }

        return { budget, category };
    }

    /**
     * Create or update a budget entry for that category.
     * @param req
     * @private
     */
    // PUT  /api/budget/:year/category/:category_id
    static async _putCategoryBudget(req) {
        const { budget, category } = await this._getAndValidateData(req.userId, req.params);
        const categoryId = req.params.category_id;

        const budgetEntry = _.pick(req.body, ['categoryId', 'isComplex', 'amount', 'period', 'rules']);
        this._validateBudgetEntry(budgetEntry, category);

        let existingBudgetEntry = budget.findEntryByCategory(categoryId);
        if (existingBudgetEntry) {
            if (existingBudgetEntry.isComplex) {
                const matchedTransactionIds = await findRelatedTransactions(existingBudgetEntry);
                await Transaction.updateMany({ _id: { $in: matchedTransactionIds } }, { recurring: false });
            }

            // Updating an existing entry
            existingBudgetEntry.isComplex = budgetEntry.isComplex;
            existingBudgetEntry.amount = budgetEntry.amount;
            existingBudgetEntry.period = budgetEntry.period;
            existingBudgetEntry.rules = budgetEntry.rules;
        } else {
            budget.entries.push(budgetEntry);
        }

        await budget.save();

        if (budgetEntry.isComplex) {
            existingBudgetEntry = budget.findEntryByCategory(categoryId);
            const matchedTransactionIds = await findRelatedTransactions(existingBudgetEntry);
            await Transaction.updateMany({ _id: { $in: matchedTransactionIds } }, { recurring: true });
        }
    }

    static _validateBudgetEntry(budgetEntry, category) {
        if (budgetEntry.isComplex) {
            if (_.isEmpty(budgetEntry.rules)) {
                throw new ClientError(`Vous devez configurer au moins une règle pour définir le budget.`);
            } else if (category.type !== CategoryType.base) {
                throw new ClientError(`Seule les sous-catégories peuvent avoir un budget avancé.`);
            }
        } else {
            if (!budgetEntry.period) {
                throw new ClientError(`La période doit être spécifié.`);
            } else if (!['w', 'M', 't', 'y'].includes(budgetEntry.period)) {
                throw new ClientError(`Période non-valide pour un budget simple.`);
            } else if (budgetEntry.amount === null) {
                throw new ClientError(`Vous devez spécifier le montant.`);
            }
        }
    }

    /**
     * Delete a budget entry for that category.
     * @param req
     * @private
     */
    // DELETE /api/budget/:year/category/:category_id
    static async _deleteCategoryBudget(req) {
        let year = queryUtil.toInt(req.params.year);
        let categoryId = req.params.category_id;

        let budget = await Budget.get(req.userId, year);
        if (!budget) {
            throw new ClientError(`No budget for year '${year}'`);
        }

        let budgetEntry = budget.findEntryByCategory(categoryId);
        if (!budgetEntry) {
            throw new ClientError(`No budget entry for category '${categoryId}'`);
        }

        if (budgetEntry.isComplex) {
            const matchedTransactionIds = await findRelatedTransactions(budgetEntry);
            await Transaction.updateMany({ _id: { $in: matchedTransactionIds } }, { recurring: false });
        }

        await budgetEntry.remove();
        await budget.save();
    }

    /**
     * Get the status of the most active budgets. Specific categoryIds can be specified, as well as the number of entries
     * that should be returned.
     * @param req
     * @private
     */
    // GET  /api/budget/status?size={int}&categories=[...categoryIds]
    static async _getBudgetStatuses(req) {
        /**
         * @property categories
         * @property size
         */
        const query = req.query;
        const selectedCategoryIds = (query.categories && query.categories.split(',')) || [];
        const size = queryUtil.toInt(query.size) || 3;

        const budgetModel = await BudgetModelingBuilder.create({
            userId: req.userId,
            budgetYear: dayjs().year(),
            from: dayjs().subtract(3, 'month'),
            to: dayjs(),
        })
            .appendTransactionCount()
            .build();

        const findNodesWithBudget = (nodes) => {
            const nodesWithBudget = [];
            for (let node of nodes) {
                const { budgetEntry } = node.value;
                if (budgetEntry && !budgetEntry.isComplex) {
                    nodesWithBudget.push(node);
                } else {
                    nodesWithBudget.push(...findNodesWithBudget(node.children));
                }
            }
            return nodesWithBudget;
        };
        const nodesWithBudget = findNodesWithBudget(budgetModel.getRootNodes());

        const budget = budgetModel.budget;
        const categories = budgetModel.categories;

        const mostActiveCategories = _.chain(nodesWithBudget)
            .sortBy((node) => -node.value.transactionCount)
            .filter((node) => !selectedCategoryIds.includes(node.value.category._id.toString()))
            .map((node) => node.value.category)
            .value();
        const selectedCategories = _.chain(selectedCategoryIds)
            .map((categoryId) => categories.find((_c) => _c._id.toString() === categoryId))
            .filter(Boolean)
            .concat(mostActiveCategories)
            .slice(0, size)
            .value();

        return {
            budgets: await getBudgetStatuses({
                userId: req.userId,
                budgetModel,
                budget,
                selectedCategories,
            }),
        };
    }

    /**
     * @param req
     * @returns {Promise<void>}
     * @private
     */
    // PUT  /api/budget/time_travel/:target_year
    static async _timeTravel(req) {
        /**
         * @property {string} target_year
         */
        const params = req.params;
        const targetYear = queryUtil.toInt(params.target_year);

        await timeTravel(req.userId, targetYear);
    }
}
module.exports = BudgetCtrl;
