/**
 * Created by hugo on 2018-10-15
 */
const _ = require('lodash');
const queryUtil = require('./utils/queryUtil');
const BaseCtrl = require('./BaseController');
const { Account } = require('../daos/account');
const { Category } = require('../daos/category');
const esDao = require('../daos/elasticsearch/elasticsearch');
const NTree = require('../modules/nTree').default;
const { toTransactionDto } = require('../models/entities');
const { defaultContext } = require('./utils/context');

class AccountCtrl extends BaseCtrl {
    static getRouter() {
        super.getRouter();

        this._router
            .route('/:account_id/pending_transactions')
            .all(defaultContext)
            .get(this._handlerFactory.makeHandler(this._getPendingTransactions));
        this._router
            .route('/:account_id')
            .all(defaultContext)
            .patch(this._handlerFactory.makeHandler(this._patch))
            .delete(this._handlerFactory.makeHandler(this._delete));
        this._router
            .route('/')
            .get(this._handlerFactory.makeHandler(this._getAll))
            .put(this._handlerFactory.makeHandler(this._createOrUpdate));

        return this._router;
    }

    static async _getAll(req, res) {
        const activeOnly = queryUtil.toBoolean(req.query.activeOnly);
        let accounts;
        if (activeOnly) {
            accounts = await Account.getActiveAccounts(req.userId);
        } else {
            accounts = await Account.find().forUser(req.userId);
        }
        return {
            items: _.chain(accounts)
                .sortBy((account) => -account.recordCount)
                .map((account) => account.toDto())
                .value(),
        };
    }

    static async _createOrUpdate(req, res) {
        /**
         * @property {number} amount
         * @property {number} creditLimit
         * @property {string} accountName
         * @property {string} accountType
         * @property {string} accountId
         * @property {string} recordCount
         * @property {object[]} pendingRecords
         */
        const body = req.body;

        const account = await Account.createOrUpdate({ ...body, updatedAt: null }, req.userId);
        await req.user.addCacheExpirationEvents(['ACCOUNTS_UPDATED']);
        return { account: Account.toDto(account) };
    }

    static async _patch(req, res) {
        // allow to update SOME fields on an existing account
        const allowedFields = ['displayName', 'isInvestmentAccount', 'disabled'];
        const filteredBody = _.pick(req.body, allowedFields);
        const account = req.context.account_id;
        _.forEach(filteredBody, (value, key) => {
            account[key] = value;
        });
        await account.save();
        return account.toDto();
    }

    static async _delete(req, res) {
        const account = req.context.account_id;
        await account.remove();
    }

    /**
     * Returns the transactions "Posted but not authorized" of a credit card account
     * @param req
     * @param res
     * @private
     */
    // GET  /api/account/:account_id/pending_transactions
    static async _getPendingTransactions(req, res) {
        const account = req.context.account_id;

        const pendingRecords = account.pendingRecords.map((record) => {
            if (record.description?.length === 41) {
                return {
                    ...record,
                    descriptionInfo: {
                        main: record.description.substring(0, 25),
                        location: record.description.substring(25),
                    },
                };
            } else {
                return record;
            }
        });

        let categoryGuesser = await esDao.createCategoryGuesstimator(req.userId, pendingRecords);

        let categories = await Category.find().forUser(req.userId).lean();
        let categoryTree = new NTree(
            categories,
            (_cat) => _cat.name,
            (_cat) => _cat.parent
        );
        const hierarchyMap = categoryTree.getCategoryHierarchy((_category) => ({
            _id: _category._id,
            name: _category.name,
            hue: _category.hue,
            systemType: _category.systemType,
            type: _category.type,
        }));

        // convert records to transactions
        return {
            items: pendingRecords.map((_record) => {
                let categoryMatch = categoryGuesser.find(_record.descriptionInfo || _record.description);

                let categoryName = null;
                if (categoryMatch) {
                    let category = categories.find((_cat) => _cat.name.toLowerCase() === categoryMatch.category);
                    if (category) {
                        categoryName = category.name;
                    }
                }

                return toTransactionDto({
                    date: _record.date,
                    description: _record.description,
                    descriptionInfo: _record.descriptionInfo,
                    amount: _record.amount || _record.deposit || (_record.withdrawal ? _record.withdrawal * -1 : 0),
                    account: _record.account,
                    currency: _record.currency,
                    category: categoryName,
                    categoryHierarchy: hierarchyMap.get(categoryName),
                });
            }),
        };
    }
}

module.exports = AccountCtrl;
