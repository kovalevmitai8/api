import { pick } from 'lodash';
import * as mongoose from 'mongoose';
import { ClientError } from './errors';
import { makeMiddleware } from './queryUtil';

import { Account, HydratedAccount } from '../../daos/account';
import { Category, CategoryDocument } from '../../daos/category';
import { HydratedStatement, Statement } from '../../daos/statement';
const Transaction = mongoose.model('transaction', require('../../daos/transaction'));
import { Goal, HydratedGoal } from '../../daos/goal';

export type ValidContextKeys = 'account_id' | 'goal_id' | 'category_id' | 'statement_id' | 'transaction_id';

export type Context = {
    account_id?: HydratedAccount;
    goal_id?: HydratedGoal;
    category_id?: CategoryDocument;
    statement_id?: HydratedStatement;
    // TODO - types: Fill those in
    transaction_id?: any;
};

export const defaultContext = makeMiddleware(async (req) => {
    req.context = {};

    const daoMapping = {
        account_id: Account,
        category_id: Category,
        statement_id: Statement,
        transaction_id: Transaction,
        goal_id: Goal,
    };

    let params = pick(req.params, Object.keys(daoMapping));
    for (let [param, value] of Object.entries(params)) {
        const dao = daoMapping[param as ValidContextKeys];
        if (dao) {
            let sanitizedId = value.replace(/[^0-9a-f]/g, '');
            // @ts-ignore
            let item = await dao.findOne({ _id: sanitizedId }).forUser(req.userId);
            if (!item) {
                throw new ClientError(`${param} not found for the current user.`, 404);
            }
            req.context[param as ValidContextKeys] = item;
        }
    }
});
