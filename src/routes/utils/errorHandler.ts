/**
 * Created by hugo on 2018-05-13.
 */
// @ts-ignore
import * as ipware from 'ipware';
const getIp = ipware().get_ip as (request: Request) => { clientIp: string; clientIpRoutable: boolean };

import * as service from '../../services/service';
import * as logger from '../../modules/simpleLogger';
import { Request, Response } from 'express';
const config = require('../../../mainConfig');

/**
 * Handle an internal server error
 */
export function handle500(req: Request, res: Response, err: Error) {
    let stack: string[] = [];
    notifyAdmin(`<b>Erreur 500</b><br/>${err.message}`, err.stack);
    err.stack.split('\n').forEach((line) => {
        stack.push(line);
    });
    res.status(500).send({
        url: req.originalUrl,
        message: err.message,
        internalError: stack,
    });
}

/**
 * Handle generic request error.
 * Server side validation can return this type of error.
 */
export function handle400(req: Request, res: Response, message: string, status = 400, notify = false) {
    if (notify) {
        const ip = getIp(req).clientIp;
        let locationStr = '';
        if (process.env.NODE_ENV === 'PROD') {
            const geoip = require('geoip-lite');
            const location = geoip.lookup(ip);
            if (location) {
                locationStr = `${location.city}, ${location.region}, ${location.country}`;
            } else {
                locationStr = 'local';
            }
            logger.log(`[geoip data] ${ip} - ${locationStr} - ${req.originalUrl}`);
        }
        notifyAdmin(
            `<b>Erreur ${status}</b>`,
            `Adresse ip: ${ip} - ${locationStr}\nurl: ${req.originalUrl}\n${message}`
        );
    }
    res.status(status).send({
        url: req.originalUrl,
        status: status,
        message: message,
    });
}

/**
 * Handle a non-authorized error
 */
export function handle401(req: Request, res: Response, message = 'Accès non authorisé à la ressource.') {
    handle400(req, res, message, 401);
}

/**
 * handle a forbidden error
 */
export function handle403(req: Request, res: Response, err: Error) {
    handle400(
        req,
        res,
        JSON.stringify(
            {
                message: err.message,
                stack: err.stack,
            },
            null,
            2
        ),
        403,
        true
    );
}

/**
 * Handle a not found error
 */
export function handle404(req: Request, res: Response, err: Error) {
    handle400(
        req,
        res,
        JSON.stringify(
            {
                message: err.message,
                stack: err.stack,
            },
            null,
            2
        ),
        404
    );
}

/**
 * Email the app's admin
 */
function notifyAdmin(message: string, stackTrace: string) {
    if (process.env.NODE_ENV === 'DEV' || process.env.NODE_ENV === 'CI') {
        logger.log(`\n===========================================`);
        message.split('<br/>').forEach((chunk) => logger.log(chunk));
        logger.log(`-------------------------------------------`);
        stackTrace.split('\n').forEach((chunk) => logger.log(chunk));
        logger.log(`===========================================\n`);
        return;
    }

    service.mailer
        .sendError({
            from: 'admin@finances-app.com',
            to: 'admin@finances-app.com',
            moduleName: 'Express',
            errorMessage: message,
            apiAddress: config.api_address,
            errorStackTrace: stackTrace || '',
        })
        .catch((err) => {
            logger.error(`-------- Failed to notify the admin ! --------`);
            logger.error('Mail service error :');
            logger.error(err);
            logger.error('Actual error :');
            logger.error(message);
            logger.error(stackTrace);
        });
}
