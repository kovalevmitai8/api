const express = require('express');
const mongoose = require('mongoose');
const Readable = require('stream').Readable;
const _ = require('lodash');

const BaseCtrl = require('./BaseController');
const errorHandler = require('./utils/errorHandler');
const queryUtil = require('./utils/queryUtil');
const FileDao = require('../daos/file');
const { ClientError } = require('./utils/errors');
const { getStream } = require('../modules/multipartUtil');
const { defaultContext } = require('./utils/context');

const { Statement } = require('../daos/statement');
const Transaction = mongoose.model('transaction', require('../daos/transaction'));

const { AttachmentType } = require('../models/enums');

class AttachmentCtrl extends BaseCtrl {
    static getRouter() {
        super.getRouter();

        this._router
            .route('/:attachment_id')
            .get((req, res) => this._download(req, res))
            .delete(this._handlerFactory.makeHandler(this._delete));

        return this._router;
    }

    static getTransactionRouter() {
        // The base route of this router is /api/transaction/
        const router = express.Router({});
        const handlerFactory = queryUtil.QueryHandler.getHandlerFactory(this);

        router
            .route('/:transaction_id/attachment')
            .all(defaultContext)
            .post(handlerFactory.makeHandler(this._uploadTransactionAttachment));
        router
            .route('/:transaction_id/attachment/:attachment_id')
            .all(defaultContext)
            .delete(handlerFactory.makeHandler(this._deleteTransactionAttachment));

        return router;
    }

    static async _download(req, res) {
        /**
         * @property {string} attachment_id
         */
        let params = req.params;

        try {
            const file = await FileDao.find({ _id: params.attachment_id }, true);
            if (!file) {
                return errorHandler.handle404(
                    req,
                    res,
                    new Error(`Aucune pièce jointe avec l'ID ${params.attachment_id}.`)
                );
            }

            // check the owner of the statement / transaction this file is attached to
            // TODO: This is a bad way to secure the file
            const statement = await Statement.findOne({
                fileId: params.attachment_id,
            })
                .forUser(req.userId)
                .lean();
            const transaction = await Transaction.findOne({
                'attachments.fileId': params.attachment_id,
            })
                .forUser(req.userId)
                .lean();
            if (!statement && !transaction) {
                return errorHandler.handle403(req, res, new Error(`Vous n'avez pas accès à ce fichier.`));
            }

            const stream = await FileDao.download(params.attachment_id);
            res.attachment(file.filename);
            stream.pipe(res);
        } catch (err) {
            errorHandler.handle500(req, res, err);
        }
    }

    static async _delete(req, res) {
        return null;
    }

    static async _deleteTransactionAttachment(req, res) {
        /**
         * @property {string} transaction_id
         * @property {string} attachment_id
         */
        let params = req.params;
        const transaction = req.context.transaction_id;

        transaction.attachments = transaction.attachments.filter(
            (_attachment) => _attachment.fileId !== params.attachment_id
        );
        transaction.markModified('attachments');
        await transaction.save();

        await FileDao.remove(params.attachment_id);
    }

    static async _uploadTransactionAttachment(req, res) {
        const transaction = req.context.transaction_id;
        let body = req.body;

        let multipartData;
        if (_.isEmpty(body)) {
            multipartData = await getStream(req);
        }

        if (!_.isEmpty(body)) {
            if (body.eml) {
                const stream = Readable.from(new Buffer(body.eml, 'base64').toString('ascii'));
                return await transaction.attachFile(stream, AttachmentType.eml, body.filename);
            } else {
                const stream = Readable.from(JSON.stringify(body));
                return await transaction.attachFile(stream, AttachmentType.json, 'data.json');
            }
        } else if (multipartData) {
            return await transaction.attachFile(multipartData.stream, AttachmentType.file, multipartData.filename);
        } else {
            throw new ClientError(`Aucun fichier n'a été reçu.`);
        }
    }
}

module.exports = AttachmentCtrl;
