/**
 * Created by Hugo on 2018-07-08
 */
const _ = require('lodash');
const mongoose = require('mongoose');
const dayjs = require('dayjs');

const BaseCtrl = require('./BaseController');
const { CategoryType } = require('../models/enums');
const { ClientError } = require('./utils/errors');
const es = require('../daos/elasticsearch/elasticsearch');
const { defaultContext } = require('./utils/context');
const transactionCtrl = require('../controllers/transaction');

const { Statement } = require('../daos/statement');
const Transaction = mongoose.model('transaction', require('../daos/transaction'));
const { Category } = require('../daos/category');
const { Budget } = require('../daos/budget');

class CategoryCtrl extends BaseCtrl {
    static getRouter() {
        super.getRouter();

        this._router
            .route('/')
            .get(this._handlerFactory.makeHandler(this._getCategories))
            .put(this._handlerFactory.makeHandler(this._updateCategory));
        this._router.route('/count').get(this._handlerFactory.makeHandler(this._countUncategorized));
        this._router.route('/last_activity').get(this._handlerFactory.makeHandler(this._lastActivity));
        this._router.route('/suggest_match/:transaction_id').get(this._handlerFactory.makeHandler(this._suggestMatch));
        this._router.route('/:category_type').get(this._handlerFactory.makeHandler(this._getByType));
        this._router.route('/:category_id').all(defaultContext).delete(this._handlerFactory.makeHandler(this._delete));

        return this._router;
    }

    /**
     * Return all categories.
     * @param req
     * @param res
     * @private
     */
    // GET  /api/category
    static async _getCategories(req, res) {
        /**
         * @property {string} [name]
         */
        let query = req.query;

        let dbQuery = {};

        if (query.name) {
            dbQuery['name'] = decodeURIComponent(query.name);
        }
        let categories = await Category.find(dbQuery).forUser(req.userId).lean();

        return {
            categories: categories.map((c) => Category.toDto(c)),
        };
    }

    /**
     * Create or update a category.
     * @param req
     * @param res
     * @private
     */
    // PUT  /api/category
    static async _updateCategory(req, res) {
        /**
         * @type {Category}
         */
        let body = req.body;

        let categoryDto = Category.toDto(body);

        if (!categoryDto.name) {
            throw new ClientError(`La catégorie doit avoir un nom.`);
        } else if (categoryDto.name === categoryDto.parent) {
            throw new ClientError(`Référence de parent circulaire.`);
        }

        let existingCategory = await Category.findOne({
            name: categoryDto.name,
        })
            .forUser(req.userId)
            .lean();
        if (existingCategory && existingCategory._id.toString() !== categoryDto.id) {
            throw new ClientError(`Il y a déjà une catégorie avec le nom '${categoryDto.name}'`);
        }

        // The system type is inherited from the parent. A validation is done client side to communicate that.
        if (categoryDto.parent) {
            const parentCategory = await Category.findOne({ name: categoryDto.parent }).forUser(req.userId).lean();
            categoryDto.systemType = parentCategory.systemType;
        }

        const activeBudget = await Budget.findOne({ categoriesSnapshot: [] }).forUser(req.userId).select('year').lean();

        let oldCategory = await Category.findById(categoryDto.id);
        let removedChildren, updatedCategory;
        if (oldCategory) {
            if (oldCategory.userId !== req.userId) {
                throw new ClientError('', 403);
            }

            removedChildren = await this._removeReferencesAsParent(req.userId, oldCategory.name);
            if (oldCategory.type === CategoryType.base) {
                const filterQuery = {
                    userId: req.userId,
                    category: oldCategory.name,
                };
                if (activeBudget) {
                    filterQuery['year'] = {
                        $gte: dayjs().year(activeBudget.year).startOf('year'),
                        $lte: dayjs().year(activeBudget.year).endOf('year'),
                    };
                }
                await Transaction.updateMany(filterQuery, { $set: { category: categoryDto.name } });
            }

            await Category.findByIdAndUpdate(categoryDto.id, categoryDto);
            updatedCategory = categoryDto;
        } else {
            let category = new Category(categoryDto);
            category.userId = req.userId;
            await category.save();
            updatedCategory = category;
        }

        await this._addReferencesToChildren(req.userId, categoryDto, categoryDto.children || removedChildren);

        return Category.toDto(updatedCategory);
    }

    static async _addReferencesToChildren(userId, category, children) {
        await Category.updateMany(
            { userId, name: { $in: children } },
            { $set: { parent: category.name, systemType: category.systemType } }
        );
        const subCategory = await Category.findSubCategories(userId, category.name);
        await Category.updateMany(
            { userId, name: { $in: subCategory.map((c) => c.name).filter((c) => !children.includes(c)) } },
            { $set: { systemType: category.systemType } }
        );
    }

    /**
     * Category count query
     * @param req
     * @param res
     * @private
     */
    // GET  /api/category/count
    static async _countUncategorized(req, res) {
        let count = await Category.countDocuments({
            userId: req.userId,
            type: CategoryType.base,
            parent: null,
        });

        return {
            count: count,
        };
    }

    /**
     * @param req
     * @param res
     * @private
     */
    static async _getByType(req, res) {
        /**
         * @property category_type {string}
         */
        let params = req.params;
        /**
         * @property {string} include_children
         */
        let query = req.query;

        // GET  /api/category/:category_type?include_children
        if (params.category_type) {
            let parentCategories = await Category.find({
                type: params.category_type,
            })
                .forUser(req.userId)
                .lean();
            let categories = parentCategories.map((c) => Category.toDto(c));

            if (query.include_children) {
                for (let category of categories) {
                    let children = await Category.find({
                        parent: category.name,
                    })
                        .forUser(req.userId)
                        .select('name')
                        .lean();
                    category.children = children.map((c) => c.name);
                }
            }

            return { categories };
        } else {
            throw new ClientError(`Category cannot be queried for field(s): ${Object.keys(query)}`, 404);
        }
    }

    /**
     * @param req
     * @param res
     * @private
     */
    // GET  /api/category/last_activity?from={date_str}&to={date_str}
    static async _lastActivity(req, res) {
        /**
         * @property from
         * @property to
         */
        const query = req.query;
        const from = (query.from && dayjs(query.from)) || null;
        const to = (query.to && dayjs(query.to)) || null;

        return {
            activityMap: await transactionCtrl.getLastActivity(req.userId, from, to),
        };
    }

    /**
     * Delete a category and its references.
     * @param req
     * @param res
     * @private
     */
    // DELETE   /api/category/:category_id
    static async _delete(req, res) {
        /**
         * @property category_id {string}
         */
        let params = req.params;
        let categoryId = params.category_id;
        let category = req.context.category_id;

        // find the active budget
        const activeBudget = await Budget.findOne({ categoriesSnapshot: [] }).forUser(req.userId);

        // delete related budget entries
        if (activeBudget) {
            let budgetEntry = activeBudget.findEntryByCategory(categoryId);
            if (budgetEntry) {
                budgetEntry.remove();
                await activeBudget.save();
            }
        }

        if (category.type === CategoryType.base) {
            let filterQuery = {
                userId: req.userId,
                category: category.name,
            };
            if (activeBudget) {
                filterQuery['date'] = {
                    $gte: dayjs().year(activeBudget.year).startOf('year').toDate(),
                    $lte: dayjs().year(activeBudget.year).endOf('year').toDate(),
                };
            }
            let referencedStatementIds = await Transaction.find(filterQuery).select('statementId').lean();

            await Statement.updateMany(
                {
                    userId: req.userId,
                    _id: _.chain(referencedStatementIds)
                        .map((t) => t.statementId)
                        .uniqBy((_id) => _id.toString())
                        .value(),
                },
                { $set: { isApproved: false } }
            );

            await Transaction.updateMany(filterQuery, { $set: { category: null } });

            await category.remove();
        } else {
            await this._removeReferencesAsParent(req.userId, category.name);
            await category.remove();
        }
    }

    /**
     * @param userId {string}
     * @param categoryName {string}
     * @returns {Promise<string[]>}
     */
    static async _removeReferencesAsParent(userId, categoryName) {
        let subCategories = await Category.find({
            userId,
            parent: categoryName,
        })
            .select('_id name')
            .lean();
        await Category.updateMany({ _id: { $in: subCategories.map((_cat) => _cat._id) } }, { $set: { parent: null } });
        return subCategories.map((_c) => _c.name);
    }

    /**
     * Suggest categories matching the transaction
     * @param req
     * @param res
     * @private
     */
    // GET      /api/category/suggest_match/:transaction_id
    static async _suggestMatch(req, res) {
        const transactionId = req.params.transaction_id;

        const transaction = await Transaction.findOne({ _id: transactionId }).forUser(req.userId).lean();
        if (!transaction) {
            throw new ClientError(`No transaction with id ${transactionId}.`);
        }

        const statement = await Statement.findOne({ _id: transaction.statementId }).forUser(req.userId).lean();

        const suggestions = await es.getRelevantCategories(
            req.userId,
            _.isEmpty(transaction.descriptionInfo) ? transaction.description : transaction.descriptionInfo,
            statement.fileType
        );
        const meanScore = _.meanBy(suggestions, 'score');
        const medianScore = _.nth(suggestions.length / 2)?.score || 0;

        return {
            categories: _.chain(suggestions)
                .filter((hit) => hit.score >= Math.max(meanScore, medianScore))
                .map((hit) => hit.value)
                .slice(0, 9)
                .value(),
        };
    }
}

module.exports = CategoryCtrl;
