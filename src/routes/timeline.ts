import * as dayjs from 'dayjs';
import * as _ from 'lodash';
import { AuthenticatedRequest, makeBaseRouter, QueryHandler, toObject } from './utils/queryUtil';
import {
    prepareModel,
    splitPeriodByYear,
    TimelineEntity,
    TimelineEntityType,
    TimelineFields,
    timelineViewMapping,
} from '../controllers/timeline';

export default function getRouter() {
    const queryHandler = QueryHandler.getHandlerFactory(this);
    const router = makeBaseRouter();

    router.route('/:entity').get(queryHandler.makeHandler(_getTimeline));

    return router;
}

async function _getTimeline(req: AuthenticatedRequest) {
    const query = req.query;
    const entity = req.params.entity as TimelineEntityType;
    const sort = toObject(query.sort as string) || { date: 1 }; // Only used if returning un-transformed transactions ?
    const filter = toObject(query.filter as string) || {};
    const fields = toObject(query.fields as TimelineFields) || [];
    const granularity = query.granularity as string; // TODO: implement this (with the category entity type)

    // TODO: Code path to retrieve a simple list of transactions is not optimal. The budget and categories are fetched
    //       regardless of if they are required.
    const periods =
        filter?.date_from && filter?.date_to
            ? splitPeriodByYear(dayjs(filter!.date_from), dayjs(filter!.date_to))
            : [{ yearStart: null, yearEnd: null }];
    const items = [];
    for (const { yearStart, yearEnd } of periods) {
        const budgetModel = await prepareModel({
            userId: req.userId,
            entity,
            filter: {
                ...filter,
                date_from: yearStart?.toDate(),
                date_to: yearEnd?.toDate(),
            },
            fields,
            search: query.q as string,
        });
        items.push(...timelineViewMapping[entity](budgetModel, { fields, granularity }));
    }

    return { items: customSort(items, sort) };
}

function customSort(items: TimelineEntity[], sort: Record<string, 1 | -1>) {
    let sortedItems = items;
    for (let [property, direction] of Object.entries(sort)) {
        sortedItems = _.orderBy(
            sortedItems,
            (item) => {
                switch (property) {
                    case 'date':
                        // @ts-ignore
                        const date = item?.date || item?.plannedDate;
                        return date ? new Date(date).getTime() : null;
                    case 'amount':
                        // @ts-ignore
                        return item?.amount || item?.plannedAmount || null;
                    default:
                        // @ts-ignore
                        return item[property];
                }
            },
            direction > 0 ? 'asc' : 'desc'
        );
    }
    return sortedItems;
}
