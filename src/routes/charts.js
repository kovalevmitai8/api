/**
 * Created by hugo on 2020-02-07
 */
const mongoose = require('mongoose');
const _ = require('lodash');
const dayjs = require('dayjs');

const BaseCtrl = require('./BaseController');
const NTree = require('../modules/nTree').default;
const { ClientError } = require('./utils/errors');

const Transaction = mongoose.model('transaction', require('../daos/transaction'));
const { Category } = require('../daos/category');
const { SystemCategory, CategoryType } = require('../models/enums');
const { Account } = require('../daos/account');
const { BudgetModelingBuilder } = require('../controllers/utils/BudgetModeling');
const queryUtil = require('./utils/queryUtil');
const { createPlannedTransactionMatcher } = require('../controllers/transaction');

const DEFAULT_PLOT_DATA_GRANULARITY = 'week';

class ChartsCtrl extends BaseCtrl {
    static getRouter() {
        super.getRouter();

        this.registerRoute('/bar/:from_date/:to_date/:group_period').get(
            this._handlerFactory.makeHandler(this._getBarChart)
        );
        this.registerRoute('/line/:from_date/to/:to_date').get(this.makeHandler(this._lineChart));

        return this._router;
    }

    /**
     * Get data for a stacked bar chart of spending / {period} of every spending and revenus
     * @param req
     * @private
     */
    // GET  /api/charts/bar/:from_date/:to_date/:group_period
    static async _getBarChart(req) {
        /**
         * @property from_date
         * @property to_date
         * @property group_period
         */
        let params = req.params;

        const acceptedPeriods = ['M', 't', 'y'];
        if (!acceptedPeriods.includes(params.group_period)) {
            throw new ClientError(
                `group_period value is not valid. Accepted values are: [${acceptedPeriods.join(', ')}]`
            );
        }

        let fromDate = new Date(params.from_date);
        let toDate = new Date(params.to_date);

        let categoryAggregates = await Transaction.aggregateAmountByCategoryAndPeriod(
            req.userId,
            fromDate,
            toDate,
            params.group_period
        );

        let groupedCategories = _.groupBy(categoryAggregates, (_agg) => {
            let groupDate = dayjs().set('year', _agg._id.year).startOf('year');
            if (params.group_period === 't') {
                groupDate = groupDate.set('month', _agg._id.t * 4).startOf('month');
            } else if (params.group_period === 'M') {
                groupDate = groupDate.set('month', _agg._id.M).startOf('month');
            }
            return groupDate.toDate().getTime();
        });

        let categories = await Category.find().forUser(req.userId).lean();
        let categoryTree = new NTree(
            categories,
            (c) => c.name,
            (c) => c.parent
        );
        let categoryRootMap = categoryTree.mapLeafsToRoots((_cat) => _cat.name);

        let series = [];
        for (let [groupDate, values] of Object.entries(groupedCategories)) {
            let rootCategorySums = new Map();
            for (let categoryAggregate of values) {
                let _cat = categories.find((_cat) => _cat.name === categoryAggregate._id.category);
                if (!_cat || _cat.systemType === SystemCategory.virement) {
                    continue;
                }

                let rootCategoryName = categoryRootMap.get(categoryAggregate._id.category);
                let sum = rootCategorySums.get(rootCategoryName) || 0;
                sum += categoryAggregate.amount;
                rootCategorySums.set(rootCategoryName, sum);
            }

            for (let [rootName, sum] of rootCategorySums.entries()) {
                let rootCategory = categories.find((_cat) => _cat.name === rootName);
                let s = series.find((_s) => _s.name === rootCategory.name);

                if (s) {
                    s.data.push([parseInt(groupDate), Math.round(sum * 100) / 100]);
                } else {
                    s = {
                        name: rootCategory.name,
                        color: rootCategory.hue,
                        data: [[parseInt(groupDate), Math.round(sum * 100) / 100]],
                    };
                    series.push(s);
                }
            }
        }

        return { items: series };
    }

    // GET  /api/charts/line/:from_date/to/:to_date
    static async _lineChart(req) {
        // TODO: This new route will still ignore planned transactions in the past if the current date is close to the
        //       start of the year. This is because the budget model is hard scoped to a single year.

        const from = dayjs(req.params.from_date);
        const to = dayjs(req.params.to_date);
        // possible values: "planned", "plannedAdjusted"
        const fields = queryUtil.toObject(req.query.fields) || [];
        const granularity = req.query.granularity || DEFAULT_PLOT_DATA_GRANULARITY;

        const accounts = await Account.getActiveAccounts(req.userId);
        const balanceOffset = _.sumBy(accounts, (_a) => _a.amount);
        const lastUpdateTimestamp = _.chain(accounts)
            .map((account) => new Date(account.updatedAt).getTime())
            .sortBy((timestamp) => timestamp)
            .last()
            .value();

        const defaultFrom = dayjs().startOf('year');
        const _from = from.isBefore(defaultFrom) ? defaultFrom : from;

        let model = await BudgetModelingBuilder.create({
            userId: req.userId,
            budgetYear: dayjs().year(),
            from: _from,
            to,
            filter: {
                recurring: true,
            },
        })
            .appendTransactions()
            .appendPlannedAmount()
            .build();
        const pendingPlannedTransactions = await this._findPendingPlannedTransactions(model, lastUpdateTimestamp);
        const pendingPlannedTransactionsSum = _.sumBy(pendingPlannedTransactions, (t) => t.amount);

        // The only way to know the balance at a specific date is to calculate it from the current balance.
        // If the year is the current one, just get teh current balance and offset the values by this amount.
        let historicData = from.isAfter(dayjs())
            ? []
            : await this._getBalanceHistoryUpTo(req.userId, from, balanceOffset);
        if (historicData.length > 0 && _.last(historicData)[0] < lastUpdateTimestamp) {
            historicData.push([lastUpdateTimestamp, balanceOffset]);
        }
        if (to.isBefore(dayjs())) {
            const endOfRange = to.clone().endOf('date').valueOf();
            historicData = historicData.filter((_tuple) => _tuple[0] <= endOfRange);
        }

        let plannedData = [];
        if (fields.includes('planned')) {
            plannedData = model.buildPlannedPlotData(granularity);
            const startOfYearBalance = this._findStartOfYearBalance(historicData);
            plannedData.forEach((tuple) => {
                tuple[1] += startOfYearBalance;
            });
        }

        let plannedAdjustedData = [];
        if (fields.includes('plannedAdjusted')) {
            model = await BudgetModelingBuilder.create({
                userId: req.userId,
                budgetYear: dayjs().year(),
                from: dayjs(lastUpdateTimestamp),
                to,
            })
                .appendTransactions()
                .appendPlannedAmount()
                .build();
            plannedAdjustedData = await model.buildPlannedAdjustedPlotData(granularity);
            plannedAdjustedData.forEach((tuple) => {
                tuple[1] += balanceOffset + pendingPlannedTransactionsSum;
            });
        }

        return {
            observed: this._prepareBalanceDataset(historicData),
            planned: this._prepareBalanceDataset(plannedData),
            plannedAdjusted: this._prepareBalanceDataset(plannedAdjustedData),
        };
    }

    static async _findPendingPlannedTransactions(model, lastUpdateTimestamp) {
        const pendingPlannedTransactions = [];
        model.iterate((node) => {
            const { transactions, plannedTransactions, budgetEntry } = node.value;

            const maximumSloppinessInDays = budgetEntry?.getMaximumSloppinessInDays();
            const cutoffTimestamp = dayjs().subtract(maximumSloppinessInDays, 'day').valueOf();

            const filteredPlannedTransactions = plannedTransactions?.filter(
                (t) => cutoffTimestamp <= t.date && t.date < lastUpdateTimestamp
            );
            const matcher = createPlannedTransactionMatcher(transactions, filteredPlannedTransactions, budgetEntry);
            const unfilteredPendingTransactions = matcher.plannedTransactions.filterIsPending();
            pendingPlannedTransactions.push(
                ...unfilteredPendingTransactions.filter((t) => t.date <= dayjs().valueOf())
            );
        });
        return pendingPlannedTransactions;
    }

    /**
     * @param {string} userId
     * @param {dayjs.Dayjs} start
     * @param {number} balanceOffset
     * @returns {Promise<{date: string, relativeBalance: number}[]>}
     * @private
     */
    static async _getBalanceHistoryUpTo(userId, start, balanceOffset) {
        const excludedCategories = await Category.find({
            type: CategoryType.base,
            systemType: SystemCategory.virement,
        })
            .forUser(userId)
            .select('name')
            .lean();
        const excludedCategoryNames = excludedCategories.map((c) => c.name);
        return await Transaction.getBalanceHistory(userId, start.toDate(), excludedCategoryNames, balanceOffset);
    }

    static _prepareBalanceDataset(dataset) {
        return dataset.map((_tuple) => {
            _tuple[1] = Math.round(_tuple[1] * 100) / 100;
            return _tuple;
        });
    }

    static _findStartOfYearBalance(dataset) {
        const endOfLastYear = dayjs().subtract(1, 'year').endOf('year');
        let endOfLastYearIndex = dataset.length - 1;
        let datePointer = dayjs(dataset[endOfLastYearIndex][0]);

        // Find the data point index for the end of last year
        while (!datePointer.isBefore(endOfLastYear, 'date')) {
            endOfLastYearIndex--;
            datePointer = dayjs(dataset[endOfLastYearIndex][0]);
        }
        return dataset[Math.max(endOfLastYearIndex, 0)][1];
    }
}
module.exports = ChartsCtrl;
