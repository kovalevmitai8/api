const path = require('path');

module.exports = Object.freeze({
    MAX_BACKUP_STORAGE_USE: 1024 * 1024 * 1024 * 15, // 15Gb limit
    BACKUP_BASE_DIR: path.join(__dirname, '..', '..', '_finances_API_backups'),
});
