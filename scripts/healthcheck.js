const http = require('http');
const isDev = process.env.NODE_ENV === 'DEV';
const options = {
    host: '0.0.0.0',
    port: isDev ? 9091 : 443,
    timeout: 2000,
    path: '/api/ping',
    method: 'GET'
};

const healthCheck = http.request(options, (res) => {
    console.log(`HEALTHCHECK STATUS: ${res.statusCode}`);
    if (res.statusCode == 200) {
        process.exit(0);
    }
    else {
        process.exit(1);
    }
});

healthCheck.on('error', function (err) {
    console.error('ERROR');
    process.exit(1);
});

healthCheck.end();