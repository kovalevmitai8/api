const testUtils = require('../utils/tests');
const { generateTransactions } = require('../utils/transactionGenerator');
const _ = require('lodash');

require('../setup');

describe('Transactions', function () {
    let authRes;
    before(async function () {
        authRes = await testUtils.authenticateRandomNewUser();
    });

    describe('No-data calls should not fail', function () {
        it('should succeed for transaction get', async function () {
            const res = await testUtils.apiGet(authRes.access_token, {
                url: `/api/transaction`,
            });
            expect(res.status).to.equal(200);
            expect(res.body.items).to.be.an('array');
        });

        it('should succeed for transaction search', async function () {
            const res = await testUtils.apiGet(authRes.access_token, {
                url: `/api/transaction?q=asdf`,
            });
            expect(res.status).to.equal(200);
            expect(res.body.items).to.be.an('array');
        });
    });

    it('should import successfully', async function () {
        const transactions = generateTransactions(7);
        const res = await testUtils.apiPut(authRes.access_token, {
            url: '/api/transaction',
            data: transactions,
        });
        expect(res.status).to.equal(200);
        expect(res.body.items).to.be.an('array');
        expect(res.body.items).to.have.lengthOf(transactions.length);
    });

    describe('Import override', function () {
        let authRes;
        beforeEach(async function () {
            authRes = await testUtils.authenticateRandomNewUser();
        });

        it('should not import the same transaction twice', async function () {
            const transactions = generateTransactions(7);
            const res = await testUtils.apiPut(authRes.access_token, {
                url: '/api/transaction',
                data: transactions,
            });
            expect(res.status).to.equal(200);

            const res2 = await testUtils.apiPut(authRes.access_token, {
                url: '/api/transaction',
                data: transactions,
            });
            expect(res2.status).to.equal(200);
            expect(res2.body.items).to.be.an('array');
            expect(res2.body.items).to.be.empty;
        });

        it('should correctly import new transactions only', async function () {
            const transactions = _.sortBy(generateTransactions(90), (t) => t.date.getTime());
            const omittedTransactions = transactions.splice(-3, 3);

            const res = await testUtils.apiPut(authRes.access_token, {
                url: '/api/transaction',
                data: transactions,
            });
            expect(res.status).to.equal(200);

            const res2 = await testUtils.apiPut(authRes.access_token, {
                url: '/api/transaction',
                data: [...transactions, ...omittedTransactions],
            });
            expect(res2.status).to.equal(200);
            expect(res2.body.items).to.be.an('array');
            expect(res2.body.items).to.have.lengthOf(omittedTransactions.length);
        });

        it('should ignore missing transactions', async function () {
            const transactions = generateTransactions(365); // yes, 365 days.
            const res = await testUtils.apiPut(authRes.access_token, {
                url: '/api/transaction',
                data: transactions,
            });
            expect(res.status).to.equal(200);

            transactions.pop();
            const res2 = await testUtils.apiPut(authRes.access_token, {
                url: '/api/transaction',
                data: transactions,
            });
            expect(res2.status).to.equal(200);
            expect(res2.body.items).to.be.an('array');
            expect(res2.body.items).to.be.empty;
        });
    });

    describe('Listing transactions', function () {
        let authRes, transactions;
        before(async function () {
            authRes = await testUtils.authenticateRandomNewUser();

            transactions = generateTransactions(90);
            const res = await testUtils.apiPut(authRes.access_token, {
                url: '/api/transaction',
                data: transactions,
            });
            expect(res.status).to.equal(200);
        });

        it('should fetch every transactions without filters specified', async function () {
            const res = await testUtils.apiGet(authRes.access_token, {
                url: `/api/transaction?size=1000`,
            });
            expect(res.status).to.equal(200);
            expect(res.body.items).to.be.an('array');
            expect(res.body.items).to.have.lengthOf(transactions.length);
        });
    });
});
