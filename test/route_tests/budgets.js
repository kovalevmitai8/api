const dayjs = require('dayjs');
const testUtils = require('../utils/tests');
const { generateTransactions } = require('../utils/transactionGenerator');

require('../setup');

describe('Budgets', async function () {
    let authRes;
    before(async function () {
        authRes = await testUtils.authenticateRandomNewUser();

        // import some transactions
        // categorise
    });

    describe('No-data calls should not fail', function () {
        it('should succeed for budget list', async function () {
            const res = await testUtils.apiGet(authRes.access_token, {
                url: `/api/budget`,
            });
            expect(res.status).to.equal(200);
            expect(res.body.items).to.be.an('array');
        });

        it('should succeed to fetch current year summary', async function () {
            const res = await testUtils.apiGet(authRes.access_token, {
                url: `/api/budget/${dayjs().year()}/category_summary`,
            });
            expect(res.status).to.equal(200);
        });

        it('should succeed to fetch observed plot data', async function () {
            const from = dayjs().startOf('year').format('yyyy-MM-dd');
            const to = dayjs().format('yyyy-MM-dd');
            const res = await testUtils.apiGet(authRes.access_token, {
                url: `/api/budget/line_data/observed/${from}/to/${to}`,
            });
            expect(res.status).to.equal(200);
        });

        it('should succeed to fetch planned plot data', async function () {
            const from = dayjs().startOf('year').format('YYYY-MM-DD');
            const to = dayjs().endOf('year').format('YYYY-MM-DD');
            const res = await testUtils.apiGet(authRes.access_token, {
                url: `/api/budget/line_data/planned/${from}/to/${to}`,
            });
            expect(res.status).to.equal(200);
        });

        it('should succeed to fetch planned adjusted plot data', async function () {
            const from = dayjs().format('YYYY-MM-DD');
            const to = dayjs().endOf('year').format('YYYY-MM-DD');
            const res = await testUtils.apiGet(authRes.access_token, {
                url: `/api/budget/line_data/planned_adjusted/${from}/to/${to}`,
            });
            expect(res.status).to.equal(200);
        });
    });

    // test the following routes:
    // TODO: add an override to create budgets in previous years (in CI only maybe ? or a boolean flag in the body ?)
    // - budget list / get / create
    // - budget entry list / create / update / delete
    // - basic sanity check of the plot data routes
    // - time-travel -> updating categories without breaking things
    // - /api/budget/:year/category_summary
    // - /api/budget/:year/category_summary/:category_id
});
